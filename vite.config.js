import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';
import { resolve } from 'path';
import imagemin from 'vite-plugin-imagemin';
import { visualizer } from 'rollup-plugin-visualizer';

const projectRootDir = resolve(__dirname);

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js'
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                }
            },
        }),
        imagemin({
            svgo: true,
            jpegtran: { progressive: true },
            optipng: { optimizationLevel: 5 },
            gifsicle: { optimizationLevel: 3 },
        }),
        visualizer({
            open: true,
            gzipSize: true,
            brotliSize: true,
        }),
    ],
    build: {
        minify: 'terser',
        sourcemap: false,
        cssCodeSplit: true,
        assetsDir: 'assets',
        chunkSizeWarningLimit: 500,
        rollupOptions: {
            output: {
                manualChunks(id) {
                    if (id.includes('node_modules')) {
                        return 'vendor';
                    }
                },
            },
        },
    },
    commonjsOptions: {
        esmExternals: true,
    },
    resolve: {
        alias: {
            $fonts: resolve(projectRootDir, "public/fonts"),
            $img: resolve(projectRootDir, "public/img"),
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
});
