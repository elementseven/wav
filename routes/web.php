<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;
use Nova;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('web')->group(function () {

	
	Route::get('/', [PageController::class, 'welcome'])->name('welcome');
	Route::get('/activities', [PageController::class, 'activities'])->name('activities');
	Route::get('/monitoring', [PageController::class, 'monitoring'])->name('monitoring');
	Route::post('/send-monitoring-form', [SendMail::class, 'monitoringForm'])->name('monitoring.send');

	Route::get('/activities/titanic-park/inflatable-park', function(){
		return redirect()->to('/activities/titanic-quarter/inflatable-park');
	})->name('inflatable-park-redirect');
	Route::get('/activities/titanic-park/indoor-skydiving', function(){
		return redirect()->to('/activities/titanic-quarter/indoor-skydiving');
	})->name('indoor-skydiving-redirect');
	Route::get('/activities/titanic-park/ninja-master-course', function(){
		return redirect()->to('/activities/titanic-quarter/ninja-master-course');
	})->name('ninja-master-course-redirect');

	Route::get('/activities/newtownbreda/inflatable-park', function(){
		return redirect()->to('/activities/inflatable-park');
	})->name('inflatable-park-redirect-nb');
	Route::get('/activities/newtownbreda/adventure-climbing', function(){
		return redirect()->to('/activities/adventure-centre');
	})->name('adventure-centre-redirect-nb');

	Route::get('/activities/inflatable-park', [PageController::class, 'inflatablePark'])->name('activities.inflatable-park');
	Route::get('/activities/adventure-centre', [PageController::class, 'adventureCentre'])->name('activities.adventure-centre');
	Route::get('/activities/toddler-bounce', [PageController::class, 'toddlerBounce'])->name('activities.toddler-bounce');

	Route::get('/gift-vouchers', [PageController::class, 'giftVouchers'])->name('gift-vouchers');
	Route::get('/billboards', [PageController::class, 'giftVouchers'])->name('gift-vouchers');
	Route::get('/contact', [PageController::class, 'contact'])->name('contact');
	Route::get('/birthdays', [PageController::class, 'birthdaysSplit'])->name('birthdaysSplit');
	Route::get('/accommodation', [PageController::class, 'accommodation'])->name('accommodation');
	Route::get('/charity-donations', [PageController::class, 'charityDonations'])->name('charity-donations');
	Route::get('/work-for-us', [PageController::class, 'workForUs'])->name('work-for-us');
	Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');
	Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');
	Route::get('/choose-your-activities', [PageController::class, 'chooseYourActivities'])->name('choose-your-activities');
	Route::get('/sensory-sessions', function(){
		return redirect()->to('/');
	})->name('sensory-sessions');

	Route::get('/christmas-gift-vouchers', function(){
		return redirect()->to('/gift-vouchers');
	})->name('gift-vouchers');

	// Parties & Groups Routes
	Route::get('/parties-and-groups', [PageController::class, 'partiesGroups'])->name('parties-and-groups');
	// Route::get('/parties-and-groups/birthday-parties', [PageController::class, 'birthdays'])->name('parties-and-groups.birthdays');
	// Route::get('/parties-and-groups/corporate-events', [PageController::class, 'corporate'])->name('parties-and-groups.corporate-events');
	Route::get('/parties-and-groups/schools-and-youth-groups', [PageController::class, 'schools'])->name('parties-and-groups.schools-and-youth-groups');
	// Route::get('/parties-and-groups/stag-and-hen-parties', [PageController::class, 'stagHen'])->name('parties-and-groups.stag-and-hen-parties');
	Route::get('/parties-and-groups/christmas', [PageController::class, 'partiesChristmas'])->name('parties-and-groups.christmas');
Route::get('/parties-and-groups/parties', [PageController::class, 'parties'])->name('parties-and-groups.parties');

	// FAQs Routes
	Route::get('/help', [FaqController::class, 'index'])->name('help');
	Route::get('/faqs/get/{faqcategory}', [FaqController::class, 'get'])->name('faqs.get');
	
	// Fixes
	Route::get('/offers- and-discounts/2021-10-05/toddler-bounce-newtonbreda', function () {
	    return redirect()->to('/offers-and-discounts/2021-10-05/toddler-bounce-newtownbreda');
	});
	Route::get('/offers-and-discounts/2022-01-03/birthday-bounce-3-for-the-price-of-2', function () {
	    return redirect()->to('/offers-and-discounts/2022-01-03/3-for-2-on-birthday-parties');
	});
	Route::get('/christmas', function(){
		return redirect()->to('/');
	});
	
	// Offers and Discounts Routes
	Route::get('/offers-and-discounts', [OfferController::class, 'index'])->name('offers-and-discounts');
	Route::get('/offers-and-discounts/get', [OfferController::class, 'get'])->name('offers-and-discounts.get');
	Route::get('/offers-and-discounts/{date}/{slug}', [OfferController::class, 'show'])->name('offers-and-discounts.show');

	// Activities Routes
	Route::get('/activities', [ActivityController::class, 'index'])->name('activities.index');
	Route::get('/activities/get', [ActivityController::class, 'get'])->name('activities.get');
	Route::get('/activities/{location}/{slug}', [ActivityController::class, 'show'])->name('activities.show');

	Route::get('/locations/{slug}', [LocationController::class, 'show'])->name('locations.show');
	
});

// Send enquiry form
Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');
Route::post('/send-party-enquiry', [SendMail::class, 'party'])->name('send-party-enquiry');
Route::post('/send-christmas-enquiry', [SendMail::class, 'christmasEnquiry'])->name('send-christmas-enquiry');
Route::post('/send-charity-enquiry', [SendMail::class, 'charity'])->name('send-charity-enquiry');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailing-list');
Route::post('/submit-cv', [ApplicationController::class, 'store'])->name('submit-cv');

// Logout
Route::get('/logout', function(){
	Auth::logout();
	return redirect()->to('/login');
});