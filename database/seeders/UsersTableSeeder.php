<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      User::create([
      	'full_name' => "Element Seven",
    		'first_name' => "Element",
    		'last_name' => "Seven",
    		'email' => "hello@elementseven.co",
    		'email_verified_at' => Carbon::now(),
    		'password' => bcrypt('Rubix2018!'),
    		'role_id' => 1,
    		'created_at' => Carbon::now(),
    		'updated_at' => Carbon::now(),
    	]);
    }
}
