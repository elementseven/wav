<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');
            $table->text('summary');
            $table->string('status');
            $table->string('ages')->nullable();
            $table->string('title_1');
            $table->text('description_1');
            $table->string('title_2');
            $table->text('description_2');
            $table->string('video')->nullable();
            $table->text('roller')->nullable();
            $table->string('photo')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('keywords')->nullable();
            $table->bigInteger('location_id')->unsigned()->index();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
