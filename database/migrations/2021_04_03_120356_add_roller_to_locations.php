<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRollerToLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->text('roller')->nullable()->after('photo');
            $table->string('title_1')->after('roller');
            $table->text('description_1')->after('title_1');
            $table->string('title_2')->after('description_1');
            $table->text('description_2')->after('title_2');
            $table->text('meta_description')->nullable()->after('description_2');
            $table->text('keywords')->nullable()->after('meta_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('roller');
            $table->dropColumn('title_1');
            $table->dropColumn('description_1');
            $table->dropColumn('title_2');
            $table->dropColumn('description_2');
            $table->dropColumn('meta_description');
            $table->dropColumn('keywords');
        });
    }
}
