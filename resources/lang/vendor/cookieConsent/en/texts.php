<?php

return [
    'message' => 'This site uses cookies to enhance your experience. Read our <a href="/privacy-policy">privacy policy</a> for more details.',
    'agree' => 'Allow cookies',
];
