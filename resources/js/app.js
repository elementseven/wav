import { createApp, defineAsyncComponent } from 'vue';
import './bootstrap';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/alert';
import 'bootstrap/js/dist/collapse';

import LazyLoad from 'vanilla-lazyload';
import 'waypoints/lib/noframework.waypoints.min.js';
import './plugins/modernizr-custom.js';
import './plugins/cookieConsent.js';
import { Carousel, Slide, Pagination, Navigation } from 'vue3-carousel';

import AOS from 'aos';
import 'vue3-carousel/dist/carousel.css';

import '@awesome.me/kit-9384155525';

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { all } from '@awesome.me/kit-9384155525/icons'
library.add(all);

// Creating the Vue 3 application
const app = createApp({});

app.component("font-awesome-icon", FontAwesomeIcon);


// Register components dynamically using defineAsyncComponent
const components = {
    'mailing-list': defineAsyncComponent(() => import('./components/MailingList.vue')),
    'loader': defineAsyncComponent(() => import('./components/Loader.vue')),
    'offer-modal': defineAsyncComponent(() => import('./components/OfferModal.vue')),

    'contact-page-form': defineAsyncComponent(() => import('./components/Contact/ContactPageForm.vue')),
    'monitoring-form': defineAsyncComponent(() => import('./components/Contact/MonitoringForm.vue')),

    'activities-slider': defineAsyncComponent(() => import('./components/ActivitiesSlider.vue')),
    'home-header': defineAsyncComponent(() => import('./components/Home/HomeHeader.vue')),
    'home-header-menu': defineAsyncComponent(() => import('./components/Home/HomeHeaderTopMenu.vue')),
    'home-boxes': defineAsyncComponent(() => import('./components/Home/HomeBoxes.vue')),
    'site-footer': defineAsyncComponent(() => import('./components/Footer.vue')),
    'main-menu': defineAsyncComponent(() => import('./components/Menus/MainMenu.vue')),
    'mobile-menu': defineAsyncComponent(() => import('./components/Menus/MobileMenu.vue')),
    'top-details': defineAsyncComponent(() => import('./components/TopDetails.vue')),

    'offers-index': defineAsyncComponent(() => import('./components/Offers/Index.vue')),
    'offers-show': defineAsyncComponent(() => import('./components/Offers/Show.vue')),

    'faqs': defineAsyncComponent(() => import('./components/FAQs/Index.vue')),

    'activities-index': defineAsyncComponent(() => import('./components/Activities/Index.vue')),
    'activities-show': defineAsyncComponent(() => import('./components/Activities/Show.vue')),
    'activity-header': defineAsyncComponent(() => import('./components/Activities/ActivityHeader.vue')),
    'activity-slideshow': defineAsyncComponent(() => import('./components/Activities/ActivitySlideshow.vue')),

    'birthdays-slider': defineAsyncComponent(() => import('./components/Parties/BirthdaysSlider.vue')),
    'schools-slider': defineAsyncComponent(() => import('./components/Parties/SchoolsSlider.vue')),

    'newtownbreda-header': defineAsyncComponent(() => import('./components/Locations/NewtownbredaHeader.vue')),
    'newtownbreda-map': defineAsyncComponent(() => import('./components/Locations/NewtownbredaMap.vue')),

    'charity-form': defineAsyncComponent(() => import('./components/Charities/CharityForm.vue')),
    'charity-slider': defineAsyncComponent(() => import('./components/Charities/CharitiesSlider.vue')),

    'apply-form': defineAsyncComponent(() => import('./components/Forms/Apply.vue')),
    'party-form': defineAsyncComponent(() => import('./components/Forms/Party.vue')),
    'christmas-form': defineAsyncComponent(() => import('./components/Forms/ChristmasForm.vue')),

    'birthday-selector': defineAsyncComponent(() => import('./components/BirthdaySelector.vue')),
    'birthdays-3-for-2-slider': defineAsyncComponent(() => import('./components/Parties/Birthdays3for2Slider.vue')),

    'party-selector': defineAsyncComponent(() => import('./components/PartySelector.vue')),
    'party-slider': defineAsyncComponent(() => import('./components/Parties/PartySlider.vue')),

    'inflatable-boxes': defineAsyncComponent(() => import('./components/Activities/InflatableBoxes.vue'))

};

// Register each component dynamically
Object.keys(components).forEach(key => {
    app.component(key, components[key]);
});

// Optionally use any Vue plugins if required
import VueLazyLoad from 'vue3-lazyload';
app.use(VueLazyLoad, {
    preLoad: 1.3,
    error: 'dist/error.png',
    loading: 'dist/loading.gif',
    attempt: 1
});

// Mount the Vue application to the DOM
app.mount('#app');
app.use(AOS);
document.addEventListener('DOMContentLoaded', function() {
    AOS.init({disable: 'mobile'});
    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
    });

});

// Initialize LazyLoad for non-Vue content
document.addEventListener('DOMContentLoaded', function () {
    new LazyLoad({
        elements_selector: ".lazy"
    });
});
