@php
$page = 'Memberships';
$pagetitle = 'Memberships | We Are Vertigo';
$metadescription = "Take advantage of all of our awesome activities at reduced prices with our memberhsip packages! Ranging from off-peak & single activity memberships to any time, all acrtivity memberships, theres something for everyone!";
$pagetype = 'white';
$pagename = 'memberships';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Coming soon!</p>
      <h1 class="mob-mt-0 page-title">Memberships</h1>
      <p>We are working on our membership packages and can't wait to bring them to you very soon! Follow us on social media to stay up to date and find out when they launch!</p>
      <p class="mb-0 text-large mt-3">
                  <a href="http://www.facebook.com/VertigoBelfast" target="_blank" class="text-white"><i class="fa fa-facebook"></i></a>
                  <a href="http://www.instagram.com/wearevertigobelfast/" target="_blank" class="text-white"><i class="fa fa-instagram ml-3"></i></a>
                  <a href="http://twitter.com/weare_vertigo" target="_blank" class="text-white"><i class="fa fa-twitter ml-3"></i></a>
                </p>
     {{--  <p>Take advantage of all of our awesome activities at reduced prices with our memberhsip packages! Ranging from off-peak & single activity memberships to any time, all acrtivity memberships, theres something for everyone!</p> --}}
{{--       <button id="activity-top-menu-btn" type="button" class="btn btn-primary btn-icon booknowbtn">Become a Member <i class="custom-icon chevron-double-down"></i></button>
 --}}    </div>
  </div>
</header>
@endsection
@section('content')
{{-- <div class="container-fluid pt-5 mt-5">
  <div class="row">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/memberships/bronze.webp" type="image/webp"/> 
                <source srcset="/img/memberships/bronze.jpg" type="image/jpeg"/> 
                <img src="/img/memberships/bronze.jpg" type="image/jpeg" alt="Bronze Membership by We are Vertigo Belfast" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Adventure Seeker</p>
                  <p class="mimic-h1 w-100">Bronze Membership</p>
                  <p class="activity-summary mb-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <p class="activity-summary mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venia.</p>
                  <button type="button" class="btn btn-black btn-icon d-inline-block booknowbtn">Become a Member <i class="custom-icon chevron-double-down-white"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview py-5 mob-mb-0 mob-pt-0 odd">
            <div class="activity-image">
              <picture>
                <source srcset="/img/memberships/silver.webp" type="image/webp"/> 
                <source srcset="/img/memberships/silver.jpg" type="image/jpeg"/> 
                <img src="/img/memberships/silver.jpg" type="image/jpeg" alt="Silver Membership by We are Vertigo Belfast" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-right">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Thrill Seeker</p>
                  <p class="mimic-h1 w-100">Silver Membership</p>
                  <p class="activity-summary mb-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <p class="activity-summary mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venia.</p>
                  <button type="button" class="btn btn-black btn-icon d-inline-block booknowbtn">Become a Member <i class="custom-icon chevron-double-down-white"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-5 mob-mt-0">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/memberships/gold.webp" type="image/webp"/> 
                <source srcset="/img/memberships/gold.jpg" type="image/jpeg"/> 
                <img src="/img/memberships/gold.jpg" type="image/jpeg" alt="Gold Membership by We are Vertigo Belfast" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Adventure Seeker</p>
                  <p class="mimic-h1 w-100">Gold Membership</p>
                  <p class="activity-summary mb-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <p class="activity-summary mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim venia.</p>
                  <button type="button" class="btn btn-black btn-icon d-inline-block booknowbtn">Become a Member <i class="custom-icon chevron-double-down-white"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid position-relative z-1">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container pt-4">
        
    </div>
  </div>
</div>
<div id="bookonline" class="container-fluid position-relative z-2 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 text-center mob-px-3 mob-pt-2">
            <h2 class="text-uppercase text-dark mb-4">Become a Vertigo VIP</h2>
            <iframe id="roller-widget" src="https://roller.app/wearevertigo/products/memberships"> </iframe><script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> --}}
{{-- <div class="container-fluid position-relative mt-5 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div> --}}
@endsection
@section('scripts')
<script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection
@section('modals')

@endsection