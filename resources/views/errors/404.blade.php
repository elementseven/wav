@php
$page = '404';
$pagetitle = '404 | We Are Vertigo';
$metadescription = 'Page not found';
$pagetype = 'white';
$pagename = '404';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row py-5 mob-pt-3">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">404 - looking for something else?</p>
      <h1 class="mob-mt-0 page-title">WOOPS!</h1>
      <p class="text-large mb-4">Unfortunately the page you were looking for wasn't found!</p>
      <a href="/">
      	<div class="btn btn-primary btn-icon">Home <i class="fa fa-home"></i></div>
      </a>
    </div>
  </div>
</header>
<top-details></top-details>
@endsection
@section('content')

@endsection