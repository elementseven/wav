@php
$page = '503';
$pagetitle = 'Maintenance | We Are Vertigo';
$metadescription = 'maintenance';
$pagetype = 'white';
$pagename = '503';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.maintenance', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row justify-content-center py-5 mob-pt-3">
    <div class="col-lg-9 mt-5 text-center">
      <p class="mimic-h2 text-primary text-uppercase">BACK SOON</p>
      <h1 class="mob-mt-0 page-title">Scheduled MAINTENTANCE</h1>
      <p class="text-large mb-5">If you need to contact us in the meantime send us an email or message us on social media.</p>
    </div>
  </div>
  <div class="row justify-content-center text-center">
    <div class="col-lg-3 col-7 pr-5 mob-mb-3 mob-pr-0">
      <img src="/img/logos/logo.svg" class="w-100" alt="we are vertigo logo"/>
    </div>
    <div class="col-lg-4">
      <p class="mb-1"><b>Email: <a href="mailto:info@wearevertigo.com" class="text-primary">info@wearevertigo.com</a></b></p>
{{--       <p class="mb-1"><b>Phone: <a href="tel:08700661522" class="text-primary">0870 0661 522*</a></b></p>
      <p class="text-smallest">*Calls to our 0870 number cost 2 pence per minute, plus your phone company’s access charge.</p> --}}
      <p>
        <a href="http://www.facebook.com/VertigoBelfast"><i class="fa fa-facebook"></i></a>
        <a href="http://www.instagram.com/wearevertigobelfast/"><i class="fa fa-instagram ml-3"></i></a>
        <a href="http://twitter.com/weare_vertigo" ><i class="fa fa-twitter ml-3"></i></a>
      </p>
    </div>

  </div>
</header>
{{-- <top-details></top-details>
 --}}@endsection
@section('content')

@endsection