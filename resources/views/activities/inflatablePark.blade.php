@php
$page = 'Activity';
$pagetitle = $activity->title . ' | We Are Vertigo';
$metadescription = $activity->meta_description;
$keywords = $activity->keywords;
$pagetype = 'light';
$pagename = 'activity';
$ogimage = 'https://wearevertigo.com' . $activity->getFirstMediaUrl('normal');
@endphp
@section('styles')
<style>
[data-f-id]{
  display:none !important;
}
</style>
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/newtownbredacheckout/en/products"></script>
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5">
  <div class="row pt-lg-5 pt-4">
    <div class="col-lg-3 d-flex align-items-center position-relative z-2 text-center text-lg-start">
      <div class="d-block w-100 pt-5">
        <p class="text-primary mimic-h3 text-title text-uppercase mb-0 ">Ages 4+</p>
        <h1 class="page-title-small d-block text-shadow mob-mb-0">Inflatable<br/>Park</h1>
        <button class="btn btn-primary btn-icon d-none d-lg-block" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </div>
    </div>
    <div class="col-lg-9 pt-lg-5">
      <div class="backdrop">
        <div class="backdrop-content">
          <div class="ratio ratio-16x9 mt-lg-5 mt-3">
          <iframe src="https://player.vimeo.com/video/656085798?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write" title="WAV - Entire Park"></iframe>
          </div>
        </div>
        <div class="backdrop-back" data-aos="fade-left"></div>
      </div>
      <p class="mt-5 text-center d-lg-none">
        <button class="btn btn-primary btn-icon" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative z-3">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left"/>
  <div class="py-5 mob-py-0">
    <div class="container pt-5 mob-pt-0 ipadp-pt-0">
      <inflatable-boxes></inflatable-boxes>
      <div class="row pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-8 pt-5 text-center text-lg-start mob-px-4">
          <p class="box-title-top text-primary text-uppercase pt-5">LETS GO!</p>
          <h2 class="title-large mob-text-h2-smaller mb-3">THE WORLD’S LARGEST <br class="d-none d-lg-block" />INFLATABLE PARK</h2>
          <p>Experience a fun day out for all the family at the World’s Largest Inflatable Park! With the main inflatable area for ages 4+, alongside our toddler area for little bouncers under 3 years, our Newtownbreda Inflatable Park is an inflatable playground where childhood fantasy becomes reality.</p>
          <p>Whether you’re bouncing off walls, battling it out with friends on the balance beam or braving the Mechanical Sweeper, our Inflatable Parks are the ultimate day out!</p>
          <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative z-3 py-5">
  <div class="row mob-py-5">
    <div class="col-lg-10">
      <activity-slideshow :slides="{{$activity->activityslides}}"></activity-slideshow>
    </div>
  </div>
</div>
<x-tots-sessions-box/>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="me-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    <!--//--><![CDATA[//><!--
      if(screen.width > 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('double')}}";
          img2.src = "{{$activity->getFirstMediaUrl('double-webp')}}";
        }
      }else if(screen.width > 768 && screen.width < 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('normal')}}";
          img2.src = "{{$activity->getFirstMediaUrl('normal-webp')}}";
        }
      }else{
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('featured')}}";
          img2.src = "{{$activity->getFirstMediaUrl('featured-webp')}}";
        }
      }
    //--><!]]>
  </script>
  <script src="https://player.vimeo.com/api/player.js"></script>
@endsection