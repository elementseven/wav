@php
$page = 'Activity';
$pagetitle = 'Toddler Bounce at Inflatable Park | We Are Vertigo';
$metadescription = '1 hour full rein on our huge inflatable park + 1 hour in our adventure centre equipped with low ropes course & 3 story soft play village! A massive hit with our little bouncers ages 1-5!';
$keywords = 'Toddler Bounce Session, We Are Vertigo, Inflatable Park, Newtownbreda, Belfast, Northern Ireland, toddler play, kids activities, toddler activities Belfast, inflatable fun, soft play, low ropes course, adventure centre, family activities, weekend activities, Saturday morning fun, Sunday morning activities, toddler bounce Belfast, We Are Vertigo toddler, bounce session for kids, inflatable park Belfast, things to do with toddlers, family fun, toddler bouncers, toddler play village';
$pagetype = 'light';
$pagename = 'activity';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@section('styles')
<style>
[data-f-id]{
  display:none !important;
}
</style>
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/newtownbredacheckout/en/products"></script>
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mt-lg-5">
  <div class="row pt-lg-5 pt-4">
    <div class="col-lg-3 d-flex align-items-center position-relative z-2 text-center text-lg-start">
      <div class="d-block w-100 pt-5">
        <p class="text-primary mimic-h3 text-title text-uppercase mb-0 ">Ages 1-5</p>
        <h1 class="page-title-small d-block text-shadow mob-mb-0">Toddler<br/>Bounce</h1>
        <button class="btn btn-primary btn-icon d-none d-lg-block" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </div>
    </div>
    <div class="col-lg-9 pt-lg-5">
      <div class="backdrop">
        <div class="backdrop-content">
          <div class="mt-lg-5 mt-3">
            <picture>
              <source srcset="/img/activities/tots-16by9.webp" type="image/webp"> 
              <source srcset="/img/activities/tots-16by9.jpg" type="image/jpeg"> 
              <img src="/img/activities/tots-16by9.jpg" type="image/jpeg" alt="Inflatable Park" class="backdrop-content lazy">
            </picture>
          </div>
        </div>
        <div class="backdrop-back" data-aos="fade-left"></div>
      </div>
      <p class="mt-5 text-center d-lg-none">
        <button class="btn btn-primary btn-icon" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative z-3 mob-px-4">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left"/>
  <div class="py-5 mob-py-0 mb-5">
    <div class="container pt-5 mob-pt-0 ipadp-pt-0">
      <div class="row pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-8 text-center text-lg-start">
          <p class="box-title-top text-primary text-uppercase">LETS GO!</p>
          <h2 class="title-large mob-text-h2-smaller text-uppercase mb-3">A massive hit with our <br class="d-none d-lg-block" />mini-bouncers</h2>
          <p>Looking for the perfect weekend adventure for your little ones? Join us every Saturday and Sunday morning at We Are Vertigo’s Inflatable Park in Newtownbreda, where toddlers aged 1-5 can bounce, slide, and explore in a space reserved just for them! With full access to our giant inflatable park, your mini adventurers will have a blast letting loose in a safe and exhilarating environment.</p>
          <p>This one-hour bounce session is the ultimate way to start the day with endless giggles, high-energy play, and a chance to burn off all that extra toddler excitement!</p>
          <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<x-inflatable-park-box/>
<!-- <div class="container-fluid position-relative">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="me-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div> -->
@endsection
@section('scripts')

  <script src="https://player.vimeo.com/api/player.js"></script>
@endsection