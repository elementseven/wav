@php
$page = 'Activity';
$pagetitle = $activity->title . ' | We Are Vertigo';
$metadescription = $activity->meta_description;
$keywords = $activity->keywords;
$pagetype = 'light';
$pagename = 'activity';
$ogimage = 'https://wearevertigo.com' . $activity->getFirstMediaUrl('normal');
@endphp
@section('styles')
<style>
[data-f-id]{
  display:none !important;
}
</style>
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/newtownbredacheckout/en/products"></script>
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5">
  <div class="row pt-lg-5 pt-4">
    <div class="col-lg-3 d-flex align-items-center position-relative z-2 text-center text-lg-start">
      <div class="d-block w-100 pt-5">
        <p class="text-primary mimic-h3 text-title text-uppercase mb-0 ">Ages 4+</p>
        <h1 class="page-title-small d-block text-shadow">Adventure<br/>Centre</h1>
        <button class="btn btn-primary btn-icon d-none d-lg-block" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </div>
    </div>
    <div class="col-lg-9 pt-lg-5">
      <div class="backdrop">
        <div class="backdrop-content">
          <div class="ratio ratio-16x9 mt-lg-5 mt-3">
          <iframe src="https://player.vimeo.com/video/656098343?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write" title="WAV - Entire Park"></iframe>
          </div>
        </div>
        <div class="backdrop-back" data-aos="fade-left"></div>
      </div>
      <p class="mt-5 text-center d-lg-none">
        <button class="btn btn-primary btn-icon" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative z-3 mob-px-4">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left"/>
  <div class="py-5 mob-py-0">
    <div class="container pt-5 mob-pt-0 ipadp-pt-0">
      <div class="row pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-8 text-center text-lg-start">
          <p class="box-title-top text-primary text-uppercase">LETS GO!</p>
          <h2 class="title-large mob-text-h2-smaller text-uppercase mb-3">Challenge the  <br class="d-none d-lg-block" />explorer in you</h2>
          <p>With play areas suitable for different ages and abilities, big kids and small can face their fears at the 30ft Climbing Wall , High & Low Ropes Course and 3 Tier Alpine Soft Play Village for 2 hours of non-stop fun! There’s even an on-site cafe to keep the grown-ups occupied.</p>
          <p>With a double seated ice slide, large flume, mini zip line, climbing wall, full height waffle tower, monkey bars and separate toddler area, our 3 Tier Soft Play Village is perfect for little adventurers. Let them face their fears and build confidence on our low ropes course. Parents can relax and enjoy a coffee while the kids have a great time.</p>
          <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative z-3 py-5">
  <div class="row mob-py-5">
    <div class="col-lg-10">
      <activity-slideshow :slides="{{$activity->activityslides}}"></activity-slideshow>
    </div>
  </div>
</div>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="me-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    <!--//--><![CDATA[//><!--
      if(screen.width > 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('double')}}";
          img2.src = "{{$activity->getFirstMediaUrl('double-webp')}}";
        }
      }else if(screen.width > 768 && screen.width < 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('normal')}}";
          img2.src = "{{$activity->getFirstMediaUrl('normal-webp')}}";
        }
      }else{
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('featured')}}";
          img2.src = "{{$activity->getFirstMediaUrl('featured-webp')}}";
        }
      }
    //--><!]]>
  </script>
  <script src="https://player.vimeo.com/api/player.js"></script>
@endsection