@php
$page = 'Activities';
$pagetitle = 'Activities - We are Vertigo';
$metadescription = "Discover what Vertigo Titanic Quarter and Vertigo Newtownbreda have to offer! With two huge Inflatable Parks, Ninja Master Course, Adventure Centre and Indoor Skydiving you’ll create memories that last a life-time with fun for everyone, aged 1-101!";
$pagetype = 'light';
$pagename = 'activities';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/allactivitiescheckout/en/products"></script>
@endsection
@section('header')
<header class="container position-relative pt-5 mob-px-4">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down"/>
  <div class="row pt-5 mob-pt-3 mt-lg-5 position-relative z-2">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
    	<p class="box-title-top text-primary text-uppercase">EXHILARATING</p>
      <h1 class="mob-mt-0 page-title">Activities</h1>
      <p class="mb-4">Discover what We Are Vertigo Newtownbreda has to offer! With one of the world's largest indoor Inflatable Parks + Adventure and Climbing Centre you'll create memories that will last a life time with fun for friends and family!</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="activities-index">
  <x-inflatable-park-box/>
  <x-tots-sessions-box/>
  <x-adventure-centre-box/>
</div>
<div class="container-fluid">
  <div class="row position-relative z-2">
    <div class="col-12 px-0">
      <div class="activity-preview pb-5 mob-mb-0 mob-pt-0 odd giftcards-activities">
        <div class="activity-image">
          <picture>
            <source srcset="/img/activities/gift-vouchers.webp" type="image/webp"/> 
            <source srcset="/img/activities/gift-vouchers.jpg" type="image/jpeg"/> 
            <img src="/img/activities/gift-vouchers.jpg" type="image/jpeg" alt="Gift Vouchers for We are Vertigo Belfast" class="lazy" />
          </picture>
        </div>
        <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-right">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <p class="box-title-top text-grey">FEELING GENEROUS?</p>
              <p class="mimic-h1 w-100">Gift an Experience<br/>to Remember</p>
              <p class="activity-summary mb-2">Choose a Gift Card fully-loaded with a cash amount to indulge in any of our activities!</p>
              <a href="/gift-vouchers">
                <button type="button" class="btn btn-black btn-icon d-inline-block applyonlinebtn">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('modals')
@endsection