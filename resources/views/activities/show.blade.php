@php
$page = 'Activity';
$pagetitle = $activity->title . ' | We Are Vertigo';
$metadescription = $activity->meta_description;
$keywords = $activity->keywords;
$pagetype = 'light';
$pagename = 'activity';
$ogimage = 'https://wearevertigo.com' . $activity->getFirstMediaUrl('normal');
@endphp
@section('styles')
<style>
[data-f-id]{
  display:none !important;
}
</style>
@if($activity->id == 4 || $activity->id == 5)
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/newtownbredacheckout/en/products"></script>
@elseif($activity->id == 1)
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/skydivecheckout/en/products"></script>
@else
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/titaniccheckout/en/products"></script>
@endif
@if($activity->slug == 'indoor-skydiving')
<link rel="preload" as="image" href="/img/activities/super-man.webp">
@endif
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('header')
@if($activity->slug == 'indoor-skydiving-decactivate')
<div class="full-height mob-fixed super-man bg z-1" style="margin-top: -34px; padding-top: 34px;">
  <div class="trans"></div>
  <skydiving-header></skydiving-header>
</div>
@else
<activity-header :activity="{{$activity}}"></activity-header>
@endif
@endsection
@section('content')
<div class="container-fluid position-relative z-3 mob-px-4">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left" data-aos="fade-down-left" data-aos-delay="200"/>
  <div class="row py-5 mob-py-0">
    <div class="container pt-5 mob-pt-0 ipadp-pt-0">
      <div class="row pt-5 mob-pt-0 ipadp-pt-0">
        <div class="col-lg-8 text-center text-lg-left">
          <p class="box-title-top text-primary text-uppercase">{{$activity->ages}}</p>
          <h1 class="mb-3">{{$activity->title_1}}</h1>
          {!!$activity->description_1!!}
          <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative z-3 py-5">
  <div class="row mob-py-5 justify-content-center">
    <div class="col-lg-10">
      <activity-slideshow :slides="{{$activity->activityslides}}"></activity-slideshow>
    </div>
  </div>
</div>
@if($activity->slug == 'indoor-skydiving')
<div class="container mt-5 pt-5">
  <div class="row justify-content-center">
    <div class="col-12 text-center mb-3 mt-5 mob-mt-0">
      <p class="mimic-h1">Package Deals!</p>
    </div>
    <div class="col-lg-4 mob-pl-4 mob-pr-5 mob-mb-5">
      <div class="activity-offer-card">
        <div class="position-relative text-center z-2">
          <picture>
            <source srcset="/img/offers/skydiving/silver-package.webp" type="image/webp"/> 
            <source srcset="/img/offers/skydiving/silver-package.jpg" type="image/jpg"/>
            <img src="/img/offers/skydiving/silver-package.jpg" alt="Indoor Skydiving - We are Vertigo - Belfast Northern Ireland" class="w-100 h-auto shadow" width="308" height="385"/>
          </picture>
          <img src="/img/icons/most-popular.svg" class="offer-ribbon h-auto" width="200" height="180"/>
          <div class="pl-3 ml-3 pt-3">
            <p class="text-one text-dark">Our most popular package for first time flyers allows a real sensation of flying. You'll leave with a few Skydiving tricks up your sleeve after this flight!</p>
            <button class="btn btn-red btn-icon mx-auto d-block ml-3" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4 mob-pl-4 mob-pr-5 offset-lg-1">
      <div class="activity-offer-card">
        <div class="position-relative text-center z-2">
          <picture>
            <source srcset="/img/offers/skydiving/high-5.webp" type="image/webp"/> 
            <source srcset="/img/offers/skydiving/high-5.jpg" type="image/jpg"/>
            <img src="/img/offers/skydiving/high-5.jpg" alt="Indoor Skydiving - We are Vertigo - Belfast Northern Ireland" class="w-100 h-auto shadow" width="308" height="385"/>
          </picture>
          <img src="/img/icons/great-value.svg" class="offer-ribbon h-auto" width="200" height="180"/>
          <div class="pl-3 ml-3 pt-3">
            <p class="text-one text-dark">Take to the skies with your 5 best friends or family members! Take turns in the wind tunnel and discover the true Superhero in your group!</p>
            <button class="btn btn-red btn-icon mx-auto d-block ml-3" type="button" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
{{-- Not titanic inflatable park --}}
@if($activity->id != 2)
<div class="pb-5 mob-px-4 mob-mt-3 position-relative z-2">
  <div class="container py-5 mob-py-3 mob-pt-0">
    <div class="row pt-5 justify-content-center">
      <div class="col-lg-10 text-center">
        <h1 class="mb-3">{{$activity->title_2}}</h1>
        {!!$activity->description_2!!}
        <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@if($activity->id == 1 || $activity->id == 2 || $activity->id == 3)
@section('modals')
<div class="modal fade show" id="notifyModal" tabindex="-1" role="dialog" aria-labelledby="notifyModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content text-center" style="border: 0 !important;">
      <div class="card bg-dark text-center py-5">
      <img src="/img/logos/logo.svg" alt="We are Vertigo Logo" width="200" height="60" class="text-center mx-auto h-auto d-inline-block mb-3">
      <p class="mimic-h2 text-primary mb-2">Sorry we're closed for essential maintenance!</p>
      <p class="mb-4">Don't miss out on the fun, check out our Newtownbreda Centre!</p>
      <a href="/locations/newtownbreda">
        <button class="btn btn-primary mb-3" type="button">BOOK NEWTOWNBREDA</button>
      </a>
{{--       <a href="/activities/titanic-quarter/indoor-skydiving">
        <button class="btn btn-primary mb-0" type="button">BOOK INDOOR SKYDIVING</button>
      </a> --}}
      {{-- <p class="close-modal text-small mb-0"><a class="text-white w-100" data-dismiss="modal" aria-label="Close"><u>Close & continue to page</u></a></p> --}}
    </div>
  </div>
</div>
@endsection
@endif
@section('prescripts')

@endsection

@section('scripts')
@if($activity->id == 1 || $activity->id == 2 || $activity->id == 3)
<script>  
  $('#notifyModal').modal('show');
</script>
<script>
  $('.close-modal').click(function(){
    $('#notifyModal').modal('hide');
  });
</script>
@endif
<script type="text/javascript">
    <!--//--><![CDATA[//><!--
      if(screen.width > 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('double')}}";
          img2.src = "{{$activity->getFirstMediaUrl('double-webp')}}";
        }
      }else if(screen.width > 768 && screen.width < 1500){
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('normal')}}";
          img2.src = "{{$activity->getFirstMediaUrl('normal-webp')}}";
        }
      }else{
        if (document.images) {
          img1 = new Image();
          img2 = new Image();
          img1.src = "{{$activity->getFirstMediaUrl('featured')}}";
          img2.src = "{{$activity->getFirstMediaUrl('featured-webp')}}";
        }
      }
    //--><!]]>
  </script>
@endsection