@php
$page = 'Parties & Groups';
$pagetitle = 'Parties & Groups | We Are Vertigo';
$metadescription = "Hens, stags, youth groups, school groups, superheroes, girl-bands... we’ve seen it all! Whether you are planning an action packed birthday party or a corporate day out, our team is on hand to tailor the perfect package for you! From team building challenges to unforgettable birthdays, we make any event or celebration hassle free & unforgettable!";
$pagetype = 'white';
$pagename = 'parties-and-groups';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5  z-2">
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Perfect for</p>
      <h1 class="mob-mt-0 page-title">Parties <span class="text-primary">&</span> Groups</h1>
      <p>Youth groups, school groups, superheroes, girl-bands... we’ve seen it all! Whether you are planning an action-packed birthday party or fun day out with friends, our team is on hand to tailor the perfect package for you! From team building challenges to unforgettable birthdays, we make any event or celebration hassle free & unforgettable!</p>
    </div>
  </div>
</header>
@endsection
@section('styles')
<style lang="scss">
  .get3for2{
    position: absolute;
    top: 1rem;
    right: 1rem;
    width: 110px;
    height: 110px;
  }
  @media only screen 
  and (min-device-width : 768px) 
  and (max-device-width : 1024px)  {
    .get3for2 {
      top: -2rem;
      right: -1rem;
      width: 90px;
      height: 90px;
    }
  }
  @media only screen 
  and (min-device-width : 768px) 
  and (max-device-width : 1024px) 
  and (orientation : landscape) {
    .get3for2 {
      top: -2rem;
      right: 1rem;
      width: 120px;
      height: 120px;
    }
  }
  @media only screen and (max-width : 767px){
    .get3for2 {
      top: -2rem;
      right: -0.5rem;
      width: 80px;
      height: 80px;
    }
  }
</style>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down"/>
  <div class="row">
    <div class="container pt-4">
        
    </div>
  </div>
</div>
<div class="container-fluid pt-3">
  <div class="row pb-5 mob-py-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/birthdays.webp" type="image/webp"/> 
                <source srcset="/img/parties/birthdays.jpg" type="image/jpeg"/> 
                <img src="/img/parties/birthdays.jpg" type="image/jpeg" alt="We are Vertigo Birthday Parties" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top">Ideal for</p>
                  <p class="mimic-h1 w-100">Birthday Parties</p>
                  <p class="mb-4 activity-summary">Whether you're turning 1 or 101...We Are Vertigo is THE place to celebrate your birthday! Chose from one of our awesome activities or double up on the fun with our combo packages! Continue the celebrations in one of our party rooms with a pizza party! Booking is a pizza cake! The birthday child even gets a FREE return pass!<br/><span class="small">(T's&C's apply)</span></p>
                  <p class="mb-0"><a href="/birthdays#bookonline" >
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                  <p class="button-after button-after-dark mt-1 pl-2 mb-0"><a href="/birthdays" ><i class="return-arrow"></i> More information</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 odd">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/parties.webp" type="image/webp"/> 
                <source srcset="/img/parties/parties.jpg" type="image/jpeg"/> 
                <img src="/img/parties/parties.jpg" type="image/jpeg" alt="We are Vertigo parties" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-right">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">You should</p>
                  <p class="mimic-h1">Celebrate with us!</p>
                  <p class="mb-4 activity-summary">Whether you've finished school, passed your piano exam or ate all your veggies there's always a reason to celebrate! We have party packages for all ages & occasions from bouncing to swinging, sliding to racing! Check out our packages now!</p>
                  <p class="mb-0"><a href="/parties-and-groups/parties" >
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Find out more <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/school-2.webp" type="image/webp"/> 
                <source srcset="/img/parties/school-2.jpg" type="image/jpeg"/> 
                <img src="/img/parties/school-2.jpg" type="image/jpeg" alt="We are Vertigo Schools and Youth Groups" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Perfect for</p>
                  <p class="mimic-h1">Schools & Youth Groups</p>
                  <p class="mb-4 activity-summary">A day trip to one of our brilliant activity centres will see your School or Youth Group take an educational journey that will challenge them physically and mentally, with fun team building activities, friendly competition and a sensational amount of fun. Our team is on hand to create a hassle-free day out with thrilling activities and delicious food packages.</p>
                  <p class="mb-0"><a href="/parties-and-groups/schools-and-youth-groups" >
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Find out more <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
{{--         <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/christmas.webp" type="image/webp"/> 
                <source srcset="/img/parties/christmas.jpg" type="image/jpeg"/> 
                <img src="/img/parties/christmas.jpg" type="image/jpeg" alt="We are Vertigo Christmas Events" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" >
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Exhilarating</p>
                  <p class="mimic-h1">Christmas<br/>Parties</p>
                  <p class="mb-4 activity-summary">Jump into the Christmas spirit with us! Whether it is a corporate, stag, hen or team bonding day out, We Are Vertigo is the place to go to this Christmas! We've got loads of packages to ensure you're put in the festive mood!</p>
                  <p class="mb-0"><a href="/parties-and-groups/christmas" >
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Find out more <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
        {{-- <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 odd">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/corporate/corporate6.webp" type="image/webp"/> 
                <source srcset="/img/parties/corporate/corporate6.jpg" type="image/jpeg"/> 
                <img src="/img/parties/corporate/corporate6.jpg" type="image/jpeg" alt="We are Vertigo Corporate Events" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" >
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Team Building</p>
                  <p class="mimic-h1">Corporate Events</p>
                  <p class="mb-4 activity-summary">We've aced Corporate Team Building - everyone is guaranteed to be besties after one of our sessions! Whether you're a small tight-knit crew or an army of a workforce, we can host a fantastic package for you at either our Titanic or Newtownbreda sites. We've also partnered up with <a href="https://haymarketbelfast.com" target="_blank" class="text-dark"><u>Haymarket</u></a> a bar/restaurant in Belfast city centre packed with an amazing atmosphere and even better cocktails to deliver you hospitality packages to finish off your evening on a high!</p>
                  <p class="mb-0"><a href="/parties-and-groups/corporate-events" >
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Find out more <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
       {{--  <div class="col-12 px-0">
          <div class="activity-preview py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/parties/stag-and-hen.webp" type="image/webp"/> 
                <source srcset="/img/parties/stag-and-hen.jpg" type="image/jpeg"/> 
                <img src="/img/parties/stag-and-hen.jpg" type="image/jpeg" alt="We are Vertigo Stag and Hen parties" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Extraordinary</p>
                  <p class="mimic-h1">Stag & Hen Parties</p>
                  <p class="mb-4 activity-summary">Let us provide that last blow out before the big day. We have a huge array of activities spread out over two fun-filled sites, from Indoor Skydiving to the World's Largest Inflatable Action Course. We've partnered up with <a href="https://haymarketbelfast.com" target="_blank" class="text-dark"><u>Haymarket</u></a> why not combine your package and enjoy delicious street food and drinks in their heated courtyard or indoor space? Let us help you reinvent the normal!</p>
                  <p class="mb-0"><a href="/parties-and-groups/stag-and-hen-parties">
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Find out more <i class="custom-icon chevron-double-right-white"></i></button>
                  </a></p>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection