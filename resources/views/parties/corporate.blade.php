@php
$page = 'Corporate Event';
$pagetitle = 'Corporate Events | We Are Vertigo';
$metadescription = "We've aced Corporate Team Building! Everyone is guaranteed to be BFF’s after one of our sessions! Whether you're a small tight-knit crew or an army we can host a fantastic package for you at either of our Titanic or Newtownbreda sites.";
$pagetype = 'white';
$pagename = 'corporate';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Team Building</p>
      <h1 class="mob-mt-0 page-title">Corporate<br/>Events</h1>
      <p>Escape the office with thrilling activities for all your corporate needs and let us create a day of team building, memory making and healthy competition unlike you have ever seen before!</p>
      <p class="mb-4">Whether your business has a small group of employees or you’re organising for a large corporate company, we can host a fantastic package for you at our Newtownbreda activity centres that will go down in office history.</p>
      <ul class="mb-4 text-left">
        <li><p>Explore our enormous Inflatable Park</p></li>
        <li><p>Face your fears with High Ropes and Climbing Wall</p></li>
        {{-- <li><p>Take to the skies at our Indoor Skydiving Centre</p></li> --}}
      </ul>
      <p class=""><b>Looking for that corporate gift?</b></p>
      <p>Our vouchers are the perfect gift and can we used for some family fun or a team bonding day out with colleagues!</p> 
      <p>Discount available for vouchers bought in bulk, enquire now!</p>
      <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0 pl-0 mob-px-3">
  <div class="row">
    <div class="container pt-5 pl-0 mob-px-3">
      <corporate-slider></corporate-slider>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2 pt-5">
  <div class="row py-5 mob-pt-0">
    <div class="container py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-left">
          <h1 class="mb-3">We're big on <span class="text-primary">hospitality</span></h1>
          <p>Hospitality packages are available, from coffee breaks in our meeting rooms to tasty street food and crackin cocktails in our partner <a href="https://haymarketbelfast.com" target="_blank" class="text-primary"><u>Haymarket</u></a> located in Belfast city centre.</p>
          <p class="mb-4">Please complete the form below to contact our team, who will reach out and work to create an itinerary that will be suitable to your group and ensure a fun and unique team building trip!</p>
          <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="requestbooking" class="container-fluid position-relative z-2 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 pb-4 mob-px-3 mob-pt-2">
            <p class="mimic-h2 mb-2 text-dark text-center">Request a booking</p>
            <p class="text-dark text-center">Tell a bit about you and your event using this short form.</p>
            <party-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></party-form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative pt-5 mt-5 mob-mt-0">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#requestbooking").offset().top -100
      }, 500);
    });
  });
</script>
@endsection