@php
$page = 'Stag & Hen Party';
$pagetitle = 'Stag & Hen Parties | We Are Vertigo';
$metadescription = "Reinvent the pre wedding parties at Northern Ireland’s Best Indoor Entertainment Venue, perfect for hen and stag parties alike! Take the hassle out of organising and let us create bespoke group packages at our Newtownbreda or Titanic Quarter locations.";
$pagetype = 'white';
$pagename = 'stagg-hen';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Extraordinary</p>
      <h1 class="mob-mt-0 page-title">Stag <span class="text-primary">&</span> Hen<br/>Parties</h1>
      <p>Reinvent the pre wedding parties at Northern Ireland’s Best Indoor Entertainment Venue, perfect for hen and stag parties alike! Take the hassle out of organising and let us create bespoke group packages at our Newtownbreda or Titanic Quarter locations.</p>
      <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
    </div>
  </div>
</header>
@endsection
@section('content')
{{-- <div class="container-fluid position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0 pl-0 mob-px-3">
  <div class="row">
    <div class="container pt-5 pl-0 mob-px-3">
      <stag-hen-slider></stag-hen-slider>
    </div>
  </div>
</div> --}}
<div class="container-fluid position-relative z-2 pt-5">
  <div class="row py-5 mob-pt-0">
    <div class="container py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-left">
          <h1 class="mb-3">MAKE IT UNFORGETTABLE!</h1>
          <p>Duel on the battle beam at our massive inflatable parks, test your agility on the Ninja Master Course, unmask your inner Superhero with indoor skydiving or even combine them all.</p>
          <p>The fun doesn't have to stop at Vertigo. Why not visit our sister venue Haymarket Belfast, the cities go to bar & street food hangout. With an extensive cocktail list, awesome street food and acoustic live music from talented local artists! All that fun definitely works up an appetite. Why not combine your package and enjoy a tasty meal, crackin' cocktails, a draught beer or two all surrounded by some awesome live music to finish off an epic day!</p>
          <p class="mb-4">Please complete the short form below and a member of our events team will reach out with a tailored package and suggested itinerary.</p>
          <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="requestbooking" class="container-fluid position-relative z-2 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 pb-4 mob-px-3 mob-pt-2">
            <p class="mimic-h2 mb-2 text-dark text-center">Request a booking</p>
            <p class="text-dark text-center">Tell a bit about you and your party using this short form.</p>
            <party-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></party-form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative pt-5 mt-5 mob-mt-0">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#requestbooking").offset().top -100
      }, 500);
    });
  });
</script>
@endsection