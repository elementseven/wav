@php
$page = 'Christmas Event';
$pagetitle = 'Christmas | We Are Vertigo';
$metadescription = "Jump into the Christmas spirit with us! Whether it is a corporate, stag, hen or team bonding day out, We Are Vertigo is the place to go to this Christmas! We've got loads of packages to ensure you're put in the festive mood!";
$pagetype = 'white';
$pagename = 'chirstmas snow-bg bg-repeat';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/offers/christmas/snowflake.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="100"/>
  <div class="row pt-5 mob-pt-3 mt-lg-5">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Festive Deals</p>
      <h1 class="mob-mt-0 page-title">Christmas Parties</h1>
      <p>Jump into the Christmas spirit with us! Whether it is a corporate, stag, hen or team bonding day out, We Are Vertigo is the place to go to this Christmas! We've got loads of packages to ensure you're put in the festive mood! Check them out now!</p>
      <a href="/docs/christmas-parties-2022.pdf?v2.0" target="_blank">
        <button type="button" class="btn btn-primary btn-icon booknowbtn">View Packages<i class="fa fa-file-pdf-o"></i></button>
      </a>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0 pl-0 mob-px-3">
  <div class="">
    <div class="container pt-5 mob-px-3">
      <div class="backdrop">
        <div class="backdrop-content">
          <picture>
            <source srcset="/img/parties/christmas/christmas.webp" type="image/webp"/> 
            <source srcset="/img/parties/christmas/christmas.jpg" type="image/jpeg"/> 
            <img src="/img/parties/christmas/christmas.jpg" type="image/jpeg" alt="We are Vertigo Christmas" class="lazy w-100" />
          </picture>
        </div>
        <div class="backdrop-back" data-aos="fade-down-right"></div>
      </div>
    </div>
  </div>
</div>
<div class="position-relative z-2">
  <div class="pb-5">
    <div class="container py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-start">
          <h1 class="mb-3">Something For <span class="text-primary">Everyone</span></h1>
          <p>We Are Vertigo is Northern Ireland's leading indoor activities destination for all ages. We believe you’re never too old to have fun, so we encourage adventure and exploring with everything we do. We pride ourselves in being Northern Ireland's go-to Christmas party venue, with a difference!</p>
          <p>The world's largest inflatable park can be found in our <a href="/locations/newtownbreda">Newtownbreda Centre</a> along with an incredible alpine themed adventure climbing & soft play village.</p>
          <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="pt-3">
  <div class="pb-5 mob-py-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 odd">
            <div class="activity-image">
              <picture>
                <source srcset="/img/bg/home-adventure.webp" type="image/webp"/> 
                <source srcset="/img/bg/home-adventure.jpg" type="image/jpeg"/> 
                <img src="/img/bg/home-adventure.jpg" type="image/jpeg" alt="We are Vertigo Christmas Package 2" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-right">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="box-title-top text-grey">Newtownbreda</p>
                  <p class="mimic-h1">Christmas Package</p>
                  <p>The ideal Christmas Adventure! This 3-hour package is perfect for the explorers of the family! Spend the day in our Newtownbreda site...did you know our Inflatable Park is the biggest indoor Inflatable Park in the world!? With challenges like the adrenalator, hungry hippos & loads more to put everyone to the test!</p>
                  <p class="mb-4 activity-summary">If that's not enough our Adventure Centre has a 30ft climbing wall, high & low ropes courses & an alpine soft play village!</p>
                  <p class="mb-0"><button type="button" class="btn btn-black btn-icon d-inline-block booknowbtn">Enquire Now <i class="custom-icon chevron-double-down-white"></i></button></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="requestbooking" class="position-relative z-2 mt-5">
  <div class="">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 pb-4 mob-px-3 mob-pt-4">
            <p class="mimic-h2 mb-2 text-dark text-center">Request a booking</p>
            <p class="text-dark text-center">Check out our <a href="/docs/christmas-parties-2022.pdf?v2.0" target="_blank" class="text-dark"><u>Christmas Packages</u></a> and tell us what you want to book</p>
            <christmas-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></christmas-form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="position-relative pt-5 mt-5 mob-mt-0">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#requestbooking").offset().top -100
      }, 500);
    });
  });
</script>
@endsection