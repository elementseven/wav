@php
$page = 'Birthday Parties';
$pagetitle = 'Birthday Parties | We Are Vertigo';
$metadescription = "Celebrate with an action packed day of breathtaking Indoor Skydiving, exploring the Ninja Master Course, adventurous Climbing & Ropes or bouncing in one of our massive Inflatable Parks. All packages are topped off with a private party room, delicious food, and bottomless jugs of juice. Parents can relax and enjoy a coffee in our viewing area while our dedicated party host looks after the rest. The perfect way to celebrate with friends and family! Book for 12 or more guests and get FREE Indoor Skydive for the Birthday child!";
$pagetype = 'white';
$pagename = 'birthdays bg-repeat balloons-bg';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home"></script>
<style type="text/css">
.no-webp .balloons-bg{
  background-image: url('/img/bg/birthdays/balloons-bg.png');
}
.webp .balloons-bg{
  background-image: url('/img/bg/birthdays/balloons-bg.webp');
}
.balloons-bg{
  background-size: 1000px;
  background-repeat: repeat;
  background-position: center;
}
</style>
@endsection
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Unforgettable</p>
      <h1 class="mob-mt-0 page-title">Birthday Parties</h1>
{{--       <p class="text-uppercase"><b>£50 Discount on all Birthday Parties! Use code</b> - <b class="text-primary">PARTY50</b></p>
 --}}      <p>Missed out on a Birthday last year..? Why not make this one extra special! Jump for joy at our amazing Birthday deal, whether you want to bounce, run, swing or climb we've got you covered! You'll receive a private party room, delicious food and bottomless jugs of juice! Parents can relax and enjoy a coffee in our viewing area whilst our dedicated party host looks after the rest! The perfect way to celebrate with friends and family! Book for 10 or more guests and get a FREE indoor skydive for the Birthday child!</p>
      <a href="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home">
        <button type="button" class="btn btn-primary btn-icon">Book Now <i class="custom-icon chevron-double-right"></i></button>
      </a>
      {{-- <button type="button" class="btn btn-primary btn-icon" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button> --}}
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0 pl-0 mob-px-3">
  <div class="row">
    <div class="container pt-5 pl-0 mob-px-3">
      <birthdays-slider></birthdays-slider>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2 pt-5">
  <div class="row pt-5 mob-pt-0">
    <div class="container py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-left">
          <h1 class="mb-3"><span class="text-primary">Hassle Free</span><br/>Birthday Packages</h1>
          <p>Our party packages really do provide truly unrivalled fun! Create a unique package by choosing one of our adrenaline fuelled activities and let us look after the rest (we’ll even do the cleaning up!). Enjoy your:</p>
          <ul class="mb-3 text-left">
            <li><p>Dedicated Party Host and Express VIP Check In</p></li>
            <li><p>Private Party Room</p></li>
            <li><p>Stone Baked Pizza & Ice Cream</p></li>
            <li><p>Bottomless Juice</p></li>
            <li><p>Reserved seating area for spectators</p></li>
            <li><p>FREE Indoor Skydive Voucher for the Birthday child (for parties of 10+ guests)</p></li>
          </ul>
          <p class="mb-4">You are more than welcome to bring your own birthday cake, decorations & party bags to make your childs day extra special!</p>
          <a href="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home">
            <button type="button" class="btn btn-primary btn-icon">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
          {{-- <button type="button" class="btn btn-primary btn-icon" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button> --}}
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-1 ">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-left mob-pb-5">
          <p class="mb-3 activity-summary">Whether you’re turning 1 or 101... We Are Vertigo is THE place to celebrate your birthday! Soar high as an Indoor Skydiver or explore our Adventure Centre, test your agility on the Ninja Master Course or bounce till you drop at our massive Inflatable Parks! Don’t miss out - parties of 10 or more get FREE Indoor Skydive for the Birthday child!</p>
          <p class="mb-4 activity-summary">This offer only applies to parties over 9 people. Booking for less than 9 people? Don't worry you can still book our <a href="/activities">activities</a>!</p>
          <a href="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home">
            <button type="button" class="btn btn-primary btn-icon">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
          {{-- <button type="button" class="btn btn-primary btn-icon"onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button> --}}
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center mt-5 pt-5 mob-pb-5 mob-pt-0">
          <p class="mimic-h2 mb-2">What your looking for fully booked?</p>
          <a href="/activities">
            <button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Activities <i class="custom-icon chevron-double-right"></i></button>
            </a>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- <div class="container-fluid position-relative pt-5 mt-5 mob-mt-0">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div> --}}
@endsection
@section('scripts')

@endsection