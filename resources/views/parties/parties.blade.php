@php
$page = 'Parties';
$pagetitle = 'Parties | We Are Vertigo';
$metadescription = "We Are Vertigo is THE place to celebrate! Soar high as an Indoor Skydiver or explore our Adventure Centre, test your agility on the Ninja Master Course or bounce till you drop at our massive Inflatable Parks!";
$pagetype = 'white';
$pagename = 'offers';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preload" as="image" href="/img/parties/birthdays/3for2/1.webp">
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home"></script>
<style>
#roller-widget{
  display: block;
  position: static;
  visibility: visible;
  top: 0px;
  width: 100%;
  border: none;
  overflow: hidden;
  min-height: 350px;
}
</style>
@endsection
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" />
  <div class="row pt-5 mt-5 mob-pt-3 position-relative z-2">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">You should</p>
      <h1 class="mob-mt-0 page-title">Celebrate <br/>with us</h1>
      <p>Whether you've finished school, passed your piano exam or ate all your veggies there's always a reason to celebrate! We have party packages for all ages & occasions from bouncing to swinging, sliding to racing! Check out our packages now!</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container">
  <div class="row py-5 mt-5 text-lg-start text-center">
    <div id="newtownbreda-birthays" class="col-12">
      <p class="mimic-h2"><span class="text-primary">Vertigo</span> <br class="d-lg-none" /> PARTY!</p>
      <p>Our Newtownbreda site is home to the largest indoor inflatable park in the world, what better place to celebrate!? We also have a huge Adventure Centre with climbing wall, ropes courses and a huge soft play village! Choose from either an Inflatable Park Party, Adventure Party or double up on the fun with a combo party (both activities) . Finish the day with a pizza party in one of our party rooms!</p>
      <p class="text-large"><b>Here is how your party will go...</b></p>
      <ul class="text-start">
        <li><p>Please arrive at the front desk 30 mins before your party is scheduled!</p></li>
        <li><p>Here you will meet your party host who will talk you through your special day!</p></li>
        <li><p>If you are an inflatabe park party you will recieve your grippy socks!</p></li>
        <li><p>Your group will be shown to your starting destination!</p></li>
        <li><p>Enjoy your big day with all your friends! You'll get a 1 hour session or if it is a combo it'll be 2 hours!</p></li>
        <li><p>Finish your day on a high! You'll then be shown to your party room for pizza, icecream & unlimited juice! (*Please note we have a strict no picnic policy)</p></li>
      </ul>
      <button class="btn btn-primary btn-icon" onclick="RollerCheckout.show()" type="button">BOOK NOW <i class="custom-icon chevron-double-right"></i></button>
    </div>
  </div>
</div>
<div class="container position-relative z-1 pt-5 mt-5 mob-mt-0 pl-0 mob-px-3">
  <div class="row justify-content-center">
    <div class="col-lg-10 pb-5 pl-0 mob-px-3">
      <party-slider></party-slider>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-1 mb-5">
  <div class="row">
     <div class="container pt-5">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection