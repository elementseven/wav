@php
$page = 'School or Youth Group Event';
$pagetitle = 'Schools & Youth Groups | We Are Vertigo';
$metadescription = "Are you a school teacher or youth group leader looking to book your next trip? Take the stress out of organising and let our dedicated team tailor a bespoke itinerary with awesome activities, team building games, breakout rooms and hospitality packages (and for larger groups, we even offer exclusive discounts!).";
$pagetype = 'white';
$pagename = 'schools';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 mob-py-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Perfect for</p>
      <h1 class="mob-mt-0 page-title">Schools <span class="text-primary">&</span><br/>Youth Groups</h1>
      <p>Are you a school teacher or youth group leader looking to book your next trip? We have a range of packages available at our Newtownbreda site, perfect for a fun and safe day out for everyone! Take the stress out of organising and let our dedicated team tailor a bespoke itinerary with awesome activities, team building games, breakout rooms and hospitality packages (and for larger groups, we even offer exclusive discounts!). We would recommend our site for ages 4-16.</p>
      {{-- <p>Are you a school teacher or youth group leader looking to book your next trip? We have a range of activity packages available at both our Titanic Quarter and Newtownbreda sites, each one perfect for a fun and safe day out! Take the stress out of organising and let our dedicated team tailor a bespoke itinerary with awesome activities, team building games, breakout rooms and hospitality packages (and for larger groups, we even offer exclusive discounts!).</p> --}}
      <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0 pl-0 mob-px-3">
  <div class="row">
    <div class="col-lg-10 pt-5 pl-0 mob-px-3">
      <schools-slider></schools-slider>
    </div>
  </div>
</div>
<div class="container position-relative z-2">
  <div class="row pb-5 mob-pt-0">
    <div class="col-12 py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-start">
          <h1 class="mb-3">Here is <span class="text-primary">a taste</span><br clear="d-none d-lg-inline"/>of what we can offer: </h1>
          <p>Explore our Adventure Centre and encourage your students to overcome their fears with our high ropes course, channel competitive spirits with our 30ft Climbing Wall or let them burn off some post exam stress in our 3 Tier Soft Play Village (under 12 years).</p>
          <p>In need of a day out for energetic teen youth groups? Our Inflatable Park packages are perfect with heaps of space to let off steam, while taking on inflatable challenges such as Mechanical sweeper, High Slides and Wipe-Out Assault Course!</p>
          {{-- <p>Reward high flyers in your school with our Indoor Skydiving Packages! Let them spread their wings and fly at 120mph with the help of our experienced instructors!</p> --}}
          <p class="mb-4">Whether you are a Youth Group, School, Community Group or Summer Scheme; we'll have you all pumping with adrenaline!</p>
          <button type="button" class="btn btn-primary btn-icon booknowbtn">Enquire Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
      </div>
      <div class="row mt-5 pt-5">
        <div class="col-lg-10">
          <h2 class="text-uppercase text-center text-lg-start"> What you need to know</h2>
          <ul>
            <li><p>Minimum persons required: 40</p></li>
            <li><p>Children must be over 4 years old for Inflata Activities and Adventure children must be over 1 years of age. </p></li>
            <li><p>Height restrictions do apply in all our activities, please check our Help page for more info. </p></li>
            <li><p>Availability: Groups less than 30 must book during normal opening hours, Friday-Sunday term time only. (Groups over 40 we can accommodate Monday-Friday)</p></li>
            <li><p>At least one teacher/leader will be required to supervise the group whilst on the premises.</p></li>
            <li><p>If you opt in from our food package, we will provide you with a room to enjoy your tasty pizza & unlimited juice!</p></li>
          </ul>
          <p class="text-center text-lg-start"><b>Please contact us and ask a member of our team for more details.</b></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="requestbooking" class="container position-relative z-2 mt-5">
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 pb-4 mob-px-3 mob-pt-2">
            <p class="mimic-h2 mb-2 text-dark text-center">Request a booking</p>
            <p class="text-dark text-center">Tell a bit about you and your event using this short form.</p>
            <party-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></party-form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container position-relative pt-5 mt-5 mob-mt-0">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="col-12 text-center mt-5">
      <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  document.addEventListener("DOMContentLoaded", function() {
    document.querySelectorAll(".booknowbtn").forEach(function(button) {
      button.addEventListener("click", function() {
        const targetElement = document.getElementById("requestbooking");
        if (targetElement) {
          const offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset - 100;

          window.scrollTo({
            top: offsetTop,
            behavior: "smooth"
          });
        }
      });
    });
  });
</script>
@endsection