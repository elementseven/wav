@php
$page = 'Homepage';
$pagetitle = "We Are Vertigo | NI's leading Inflatable Park & Activity Centre.";
$metadescription = "We Are Vertigo is Northern Ireland’s leading Indoor activities destination for all ages. The World's Largest Inflatable Park can be found in our Newtownbreda Centre along with an incredible Alpine Themed Adventure Climbing & Soft Play village.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
.home-full-height{
  min-height: 750px;
}
.home-bauble{
  position: absolute;
  bottom: 100px;
  right: 10%;
  width: 200px;
  height: auto;
  z-index: 1;
}
@media only screen and (max-width : 767px){
  .menu .menu_logo{
    top: 15px;
    position: relative;
  }
  .home-bauble{
    bottom: 130px;
    right: 15px;
    width: 28vw;
  }
}
</style>
<link rel="preload" as="image" href="/img/home-slides/half-term.webp?v=2025-01-05">
@endsection
@section('header')
<div id="activities-banner" class="container-fluid bg-primary position-relative z-2">
  <div class="row">
    <div class="col-12 text-center">
      <a href="/choose-your-activities">
        <p class="mb-0 text-dark text-one text-uppercase py-1 position-relative d-inline-block"><b>Let us help you choose your activities</b> <i class="custom-icon chevron-double-right ms-2 text-one"></i></p>
      </a>
    </div>
  </div>
</div>
<div  class="home-full-height mob-fixed bg z-1 half-term" style="margin-top: -34px; padding-top: 34px;">
  <div class="trans"></div>
  <home-header></home-header>
</div>
<div class="container d-md-none mob-home-menu-container">
  <div class="row justify-content-center">
    <div class="col-10">
      <div class="card bg-primary text-dark mt-2 py-3 position-relative text-center shadow z-2">
        <a href="/activities" class="zoom-link">
          <p class="mimic-h2 zoom-item mb-0 text-dark text-center">ACTIVITIES</p>
          <img src="/img/icons/chevron-double-right.svg" alt="We are Vertigo chevron down icon" class="chevron-down" width="14" height="17" />
        </a>
      </div>
      <div class="card bg-primary text-dark mt-2 py-3 position-relative text-center shadow z-2">
        <a href="/parties-and-groups" class="zoom-link">
          <p class="mimic-h2 zoom-item mb-0 text-dark text-center">BOOK YOUR PARTY</p>
          <img src="/img/icons/chevron-double-right.svg" alt="We are Vertigo chevron down icon" class="chevron-down" width="14" height="17" />
        </a>
      </div>
    </div>
  </div>
</div>
@endsection
@section('content')
<div class="overflow-hidden position-relative z-2">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double" alt="We are Vertigo chevrons right" />
</div>
<div id="scrollToActivities" class="container-fluid position-relative pt-lg-5">
  <div class="row pt-5 mt-lg-5">
    <div class="container container-wide pt-lg-5 mob-py-0 pl-0 mob-px-3 pt-lg-5 mt-lg-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 pl-0 pt-lg-5 pt-4">
          <p class="box-title-top text-primary text-center text-uppercase">EXHILARATING</p>
          <p class="mimic-h1 text-center mb-4 page-title">Activities</p>
        </div>
      </div>
    </div>
  </div>
</div>
<x-inflatable-park-box/>
<x-tots-sessions-box/>
<x-adventure-centre-box/>
<div id="home-section-1" class="position-relative">
  <div class="pb-5 mob-py-0">
    <div class="container py-5">
      <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center text-lg-start">
          <h1 class="mb-3">Northern Ireland's<br/>premier entertainment complex</h1>
        </div>
        <div class="col-lg-8 text-center text-lg-start">
          <p>We Are Vertigo is Northern Ireland’s leading Indoor activities destination for all ages. We believe you’re never too old to have fun, so we encourage adventure and exploring with everything we do.</p>
          <p class="mb-4">The World's Largest Inflatable Park can be found in our Newtownbreda Centre along with an incredible Alpine Themed Adventure Climbing & Soft Play village.</p>
          <a href="/activities">
            <button type="button" class="btn btn-primary btn-icon">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
        </div>
        <div class="col-lg-2 offset-lg-1 text-center pt-5 pt-lg-0">
          <div id="TA_certificateOfExcellence473" class="TA_certificateOfExcellence"><ul id="Y1ZMtNMQ" class="TA_links l8chKqmC25Cj"><li id="A5b9KrwG" class="RHOdMkDcMM"><a target="_blank" aria-label="Read reviews on TripAdvisor" href="https://www.tripadvisor.co.uk/Attraction_Review-g186470-d5810524-Reviews-We_Are_Vertigo-Belfast_Northern_Ireland.html"><img data-src="https://static.tacdn.com/img2/travelers_choice/widgets/tchotel_2023_LL.png" alt="TripAdvisor" class="widCOEImg lazy" id="CDSWIDCOELOGO"/></a></li></ul></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="me-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script async src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=473&amp;locationId=5810524&amp;lang=en_UK&amp;year=2023&amp;display_version=2" data-loadtrk onload="this.loadtrk=true"></script>
@endsection
@section('modals')

@endsection