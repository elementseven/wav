@php
$page = 'Activity';
$pagetitle = 'Titanic Quarter | We Are Vertigo';
$metadescription = $location->meta_description;
$pagetype = 'light';
$pagename = 'titanic-park';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $location->keywords, 'ogimage' => $ogimage])
@section('styles')
{{-- <script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/titaniccheckout/en/products"></script> --}}
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/allactivitiescheckout/en/products"></script>
@endsection
@section('header')
<div  class="full-height mob-fixed home-highstreet-voucher bg z-1" style="margin-top: -34px; padding-top: 34px;">
  <titanic-header></titanic-header>
</div>
@endsection
@section('content')
<div class="container-fluid position-relative z-2 mob-px-4">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left" data-aos="fade-down-left" data-aos-delay="200"/>
  <div class="row py-5 mob-py-0">
    <div class="container pt-5">
      <div class="row pt-5 mob-pt-0">
        <div class="col-lg-8 text-center text-lg-left">
          <h1 class="mb-3">{{$location->title_1}}</h1>
          {!!$location->description_1!!}
          <a href="https://ecom.roller.app/wearevertigo/allactivitiescheckout/en/products">
            <button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
          {{-- <button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button> --}}
        </div>
      </div>
    </div>
  </div>
</div>
<div id="scrollToActivities" class="container-fluid position-relative z-2">
  <img src="/img/graphics/chevrons-left.svg" class="home-chevrons-left-1" alt="We are Vertigo chevrons left" data-aos="fade-up-right"/>
  <div class="row">
    <div class="container container-wide py-5 mob-py-0 pl-0 mob-px-3">
      <div class="row py-5">
        <div class="col-12 pl-0" style="min-height: 548px;">
          <activities-slider :location="'titanic-quarter'" :items="3"></activities-slider>
          
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2 mob-mt-5">
  <div class="row">
     <div class="container pt-5">
      <div class="row">
        <div class="col-12 text-center mt-5">
         <p class="mimic-h2 mb-5"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Check out all our activities</span> <a href="{{route('activities.index')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Check them out <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<contact-page-map></contact-page-map>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection