@php
$page = 'Activity';
$pagetitle = 'Newtownbreda | We Are Vertigo';
$metadescription = $location->meta_description;
$pagetype = 'light';
$pagename = 'newtownbreda';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $location->keywords, 'ogimage' => $ogimage])
@section('styles')
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/newtownbredacheckout/en/products"></script>
@endsection
@section('header')
<div  class="full-height mob-fixed home-highstreet-voucher bg z-1" style="margin-top: -34px; padding-top: 34px;">
  <newtownbreda-header></newtownbreda-header>
</div>
@endsection
@section('content')
<div class="position-relative z-2 mob-px-4 mb-5">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double d-none d-lg-block" alt="We are Vertigo chevrons double left" data-aos="fade-down-left" data-aos-delay="200"/>
  <div class="py-5 mob-py-0">
    <div class="container pt-5">
      <div class="row pt-5 mob-pt-0">
        <div class="col-lg-8 text-center text-lg-start">
          <h1 class="mb-3">{{$location->title_1}}</h1>
          {!!$location->description_1!!}
          <a href="https://ecom.roller.app/wearevertigo/allactivitiescheckout/en/products">
            <button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
          {{-- <button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button> --}}
        </div>
      </div>
    </div>
  </div>
</div>
<x-inflatable-park-box/>
<x-tots-sessions-box/>
<x-adventure-centre-box/>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="me-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection
@section('modals')
@endsection