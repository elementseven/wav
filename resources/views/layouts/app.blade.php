<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="canonical" href="https://wearevertigo.com/">
  <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#111111">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#111111">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>
  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="We are Vertigo">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:image:secure" content="{{$ogimage}}">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="1200" /> 
  <meta property="og:image:height" content="627" />
  <meta property="og:updated_time" content="2021-04-14T21:23:54+0000" />
  <meta property="og:description" content="{{$metadescription}}">
  <meta property="fb:app_id" content="447714939820641" />
  <meta name="facebook-domain-verification" content="5rk8y059pg9es6d2ww0li2szmusr4d" />
  <meta name="facebook-domain-verification" content="16c7wxef5cg6zr1j34csbf28azuehy" />
  <meta name="description" content="{{$metadescription}}">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Semibold.woff"  type="font/woff" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Semibold.woff2" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Semibold.ttf" type="font/ttf" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Bold.woff" type="font/woff" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Bold.woff2" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Bold.ttf" type="font/ttf" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Black.woff" type="font/woff" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Black.woff2" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Tungsten/Tungsten-Black.ttf" type="font/ttf" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/GothamRnd-Book.woff" type="font/woff" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/GothamRnd-Book.woff2" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/GothamRnd-Book.ttf" type="font/ttf" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/Gotham-Bold.woff" type="font/woff" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/Gotham-Bold.woff2" type="font/woff2" crossorigin="anonymous">
  <link rel="preload" as="font" href="/fonts/Gotham/Gotham-Bold.ttf" type="font/ttf" crossorigin="anonymous">
  {{-- <link rel="preload" as="font" href="/fonts/fontawesome-webfont.woff2?af7ae505a9eed503f8b8e6982036873e" type="font/woff2" crossorigin="anonymous"> --}}
  @vite(['resources/sass/app.scss'])
  <link rel="preconnect" href="https://www.googletagmanager.com"/>
  <link rel="preconnect" href="https://connect.facebook.net">
  <link rel="preconnect" href="https://analytics.tiktok.com">
  <link rel="preconnect" href="https://www.google-analytics.com">
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "08700661522",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
@yield('head_section')
</head>  
<body id="wav-body" class="front page-{{$pagename}}">

<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    <div id="fb-root"></div>
    <main-menu></main-menu>
    <mobile-menu></mobile-menu>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    <x-footer/>
    @yield('modals')
    <loader></loader>
    <a href="/faqs" title="Frequently asked questions">
      <div class="faq-btn" data-aos="fade-up"><font-awesome-icon :icon="['fas', 'info']"/></div>
    </a>
    <!-- <offer-modal></offer-modal> -->
  </div>
  <div id="menu_body_hide"></div>
  <!-- Modal -->
  
  <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="card border-0 box-shadow">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body p-5">
            <img src="/img/logos/logo.svg" class="menu_logo mb-3" alt="We are Vertigo Logo" width="120" />
            <p class="modal-text"></p>
            <a class="modal-btn-link" href="#">
              <button type="button" class="btn btn-primary modal-btn mb-2"></button>
            </a>
            <div class="btn-text cursor-pointer text-center d-block w-100" data-dismiss="modal">[X] Close</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
@vite('resources/js/app.js')
@yield('scripts')
<script>
  window.addEventListener("load", function(){
   window.cookieconsent.initialise({
     "palette": {
       "popup": {
         "background": "#1D252D",
         "text": "#ffffff"
       },
       "button": {
         "background": "#F6CF3C",
         "text": "#1D252D",
         "border-radius": "30px"
       }
     }
   })});
 </script>
@if(env('APP_ENV') != 'local')
<script async>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WQHW9T');</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WQHW9T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
@endif
</body>
</html>