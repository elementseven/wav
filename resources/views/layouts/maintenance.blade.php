<!DOCTYPE html>
<html id="wav-html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/site.webmanifest">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#111111">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#111111">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <title>{{$pagetitle}}</title>

  <meta property="og:type" content="website">
  <meta property="og:title" content="{{$pagetitle}}">
  <meta property="og:url" content="{{Request::url()}}">
  <meta property="og:site_name" content="We are Vertigo">
  <meta property="og:locale" content="en_GB">
  <meta property="og:image" content="{{$ogimage}}">
  <meta property="og:description" content="{{$metadescription}}">
  <meta name="description" content="{{$metadescription}}">
  <link href="{{ asset('css/app.css') }}?v0.00000008" rel="stylesheet"/>
  @yield('styles')
  <script type="application/ld+json">
   {
    "@context" : "https://schema.org",
    "@type" : "Organization",       
    "telephone": "+442895031307",
    "contactType": "Customer service"
  }
</script>
<script>window.dataLayer = window.dataLayer || [];</script>
</head>  
<body id="wav-body" class="front">
@yield('fbroot')
<div id="main-wrapper">
  <div id="app" class="front {{$pagetype}} overflow-hidden mw-100" data-scroll-container>
    @yield('header')
    <main id="content" style="z-index: 2;" class="">
      <div id="menu-trigger"></div>
      @yield('content')
    </main>
    {{-- <site-footer></site-footer> --}}
    @yield('modals')
    <loader></loader>
  </div>
  <div id="menu_body_hide"></div>
  <!-- Modal -->
  <div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="card border-0 box-shadow">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body p-5">
            <img src="/img/logos/logo.svg" class="menu_logo mb-3" alt="We are Vertigo Logo" width="120" />
            <p class="modal-text"></p>
            <a class="modal-btn-link" href="#">
              <button type="button" class="btn btn-primary modal-btn mb-2"></button>
            </a>
            <div class="btn-text cursor-pointer text-center d-block w-100" data-dismiss="modal">[X] Close</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('prescripts')
<script src="{{ asset('js/app.js') }}?v0.00000008"></script>
<script type="text/javascript">!function(e,t,n){ function a() { var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e) }if(e.Beacon=n=function(t,n,a){ e.Beacon.readyQueue.push({ method:t,options:n,data:a}) },n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1) }(window,document,window.Beacon||function(){ });</script>
<script type="text/javascript">window.Beacon('init', '46308975-f2af-4c8b-947b-c111eeff3b69')</script>
<style>.hsds-beacon .iRZbXW{bottom:0.5rem!important;right:0.5rem !important;</style>
@yield('scripts')
</body>
</html>