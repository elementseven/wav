@php
$page = 'News';
$pagetitle = $post->title . ' | We Are Vertigo';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://wearevertigo.com' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container position-relative pt-5 mob-pt-0">
  <div class="row pt-5 mob-mb-5 text-center text-lg-left">
    <div class="col-xl-12 pt-5 mob-px-4 mb-5 mob-mb-0">
      <p class="pt-5 mob-pt-0 mb-1 text-title box-title-top text-uppercase"><a href="{{route('news.index')}}"><i class="fa fa-angle-double-left"></i>&nbsp; Browse news</a></p>
      <h1 class="blog-title mb-3">{{$post->title}}</h1>
      <p class="text-larger mt-3 mb-4">{{$post->excerpt}}</p> 
      <p class="mb-1 mimic-h3"><b>Share this article:</b></p> 
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
      
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 mob-py-0 text-center text-lg-left">
        <div class="col-12 mob-mt-0 mb-5">
          <div class="backdrop overflow-hidden pb-3">
            <picture>
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('news','double-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('news','double')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('news','normal-webp')}}" type="image/webp"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('news','normal')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('news','featured-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('news','featured')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
              <img src="{{$post->getFirstMediaUrl('news','featured')}} 1w,  {{$post->getFirstMediaUrl('news','normal')}} 768w, {{$post->getFirstMediaUrl('news','double')}} 1200w" type="{{$post->getFirstMedia('news')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
            </picture>
            <div class="backdrop-back" data-aos="fade-down-right"></div>
          </div>
        </div>
        <div class="col-xl-10 mob-mt-0 blog-body">
          {!!$post->content!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1 mimic-h3"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<div class="container pt-5 mob-px-4">
  <div class="row">
    <div class="col-12 mb-2 text-center text-lg-left">
      <p class="mimic-h2 mb-0">Latest News</p>
      <hr class="line-primary my-5 mob-my-3" />
    </div>
  </div>
  @if(count($others))
  <div class="row pb-5 mob-py-0">
    @foreach($others as $key => $post)
    @if($key == 0)
    <div class="col-12 mb-5">
    @else
    <div class="col-12 mb-5 mt-5 mob-mt-0">
    @endif
      <div class="row">
        <div class="col-lg-4">
          <a href="/news/{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('Y-m-d')}}/{{$post->slug}}" >
            <div class="backdrop overflow-hidden pb-3">
              <picture>
                <source srcset="{{$post->getFirstMediaUrl('news','featured-webp')}}" type="image/webp"/> 
                <source srcset="{{$post->getFirstMediaUrl('news','featured')}}" type="{{$post->getFirstMedia('news')->mime_type}}"/> 
                <img src="{{$post->getFirstMediaUrl('news','featured')}}" type="{{$post->getFirstMedia('news')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
              </picture>
              <div class="backdrop-back" data-aos="fade-down-right"></div>
            </div>
          </a>
        </div>
        <div class="col-lg-8 pl-3 pt-4">
          <p class="news-title-top text-light">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('jS M Y')}}</p>
          <h3 class="mb-1 text-uppercase post-title">{{$post->title}}</h3>
          <p class="mb-2">{{$post->excerpt}} <a href="/news/{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('Y-m-d')}}/{{$post->slug}}" >[...]</a></p>
          <p class="text-title read-more-news text-uppercase mb-0"><a href="/news/{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('Y-m-d')}}/{{$post->slug}}" >Read Article <i class="fa fa-angle-double-right ml-2"></i></a></p>
        </div>
      </div>
    </div>
    @endforeach
  </div>
  @endif
  <div class="row">
    <div class="col-12 text-center mt-5">
      <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">More Veritgo news</span> <a href="{{route('news.index')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">See all news <i class="custom-icon chevron-double-right"></i></button></a></p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection