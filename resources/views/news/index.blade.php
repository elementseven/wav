@php
$page = 'News';
$pagetitle = 'Latest News - We are Vertigo';
$metadescription = "Stay up to date with all the latest news & announcments at Northern Ireland's leading entertainment complex.";
$pagetype = 'light';
$pagename = 'news';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
	<img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 mt-5 text-center text-lg-left">
    	<p class="box-title-top text-primary text-uppercase"><i class="fa fa-newspaper-o mr-2"></i>Stay up to date</p>
      <h1 class="mob-mt-0 page-title">Latest News</h1>
      <p class="mb-4">Stay up to date with all the latest news & announcments at Northern Ireland's leading entertainment complex.</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
@endsection