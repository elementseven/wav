@php
$page = 'Work For Us';
$pagetitle = 'Work For Us | We Are Vertigo';
$metadescription = "We are looking for passionate individuals, with a desire to learn and work in a fast-paced environment, to join the Vertigo Family!";
$pagetype = 'white';
$pagename = 'work-for-us';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="position-relative py-5 z-2 bg workforus_bg">
  <div class="trans-dark"></div>
  <div class="">
    <div class="container position-relative z-2">
      <div class="row py-5 mt-lg-5 mob-pt-3">
        <div class="col-lg-9 mt-5 text-center text-lg-start">
          <p class="box-title-top text-primary text-uppercase">Careers</p>
          <h1 class="mob-mt-0 page-title">Work For Us</h1>
          <p>Are you interested in joining our enthusiastic and passionate team! Do you like working in a fast-paced environment? Are you a people person? Do you have a passion for customer service? Join us Now!</p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container pt-5 mt-5">
  <div class="row">
    <div class="col-12">
      <h2 class="text-uppercase title-large">Vacancies</h2>
      <div class="border-bottom mt-4 border-primary"><div></div></div>

 
      <div class="row my-5">
        <div class="col-lg-9">
          <h3 class="mob-text-large d-none d-lg-block">Group Bookings and Customer Communications advisor</h3>
        </div>
        <div class="col-lg-3 text-lg-right d-none d-lg-block">
          <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn">Apply Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
        <div class="col-12 mt-3 mob-mt-0">
          <div class="row">
            <div class="col-lg-5">
              <picture>
                <source src="/img/workforus/img2.webp" type="image/webp"/>
                <source src="/img/workforus/img2.jpg" type="image/jpeg"/>
                <img src="/img/workforus/img2.jpg" class="w-100 h-auto" alt="Assistant Site Manager - We are vertigo"/>
              </picture>
            </div>
            <div class="col-lg-7">
              <h3 class="mob-text-large mt-4 d-lg-none">Group Bookings and Customer Communications advisor</h3>
              <p class="text-one-one">A fantastic new opportunity has arisen to work for an award-winning leisure and hospitality business in Northern Ireland, with amazing career prospects and a genuine, supportive working environment. Interested? Of course, you are! If you have experience in customer service and are looking for that next step up in your career, keep reading...</p>
              <p class="text-one-one">We are currently recruiting now for brilliant Customer Service and Group Bookings Advisors. As a customer advisor you will be the heart of our business and at the front end of delivering a first-class service advising on range of our activities!</p>
              <p class="text-left mb-0"><a data-toggle="collapse" href="#vaccancy2" role="button" aria-expanded="false" aria-controls="vaccancy2">
                <b class="collapsed"><b class="text-white">Think you're right for the role?</b><br/><u>Learn More</u> <i class="fa fa-angle-double-down"></i></b>
              </a></p>
            </div>
          </div>
          <div class="collapse" id="vaccancy2">
            
            <p class="mt-5"><b>What are we looking for?</b></p>
            <p>Providing customers with information about our services, ranging from individual passes and bookings, gift vouchers, corporate enquiries, birthday parties or any other questions our customers might have! Whether your background is retail, hospitality, event organisation or team member roles to name a few, the skills that you have acquired will be utilised to the full. If you are able to pick up on the small details that help you understand what people want then, you are the person we want.</p>
            <p>We are looking for individuals with a passion for delivering great customer service and an interest in the leisure industry. We are responsive, empathetic, solution driven and, most of all, customer focused. You’ll take great care to make sure every customer has a positive experience.</p>

            <p class="mt-5"><b>What we need:</b></p>
            <ul>
              <li>At least 1 year customer service experience</li>
              <li>Good level of spoken and written English</li>
              <li>At least 6 months customer service experience</li>
              <li>Drive and determination to succeed</li>
            </ul>
            <p class="mt-5"><b>How do we look after you?</b></p>
            <ul>
              <li>Competitive salary.</li>
              <li>The opportunity to progress within a growing company</li>
              <li>Great employee discounts on all our activities-upon completion of probationary period</li>
              <li>Unique staff incentives </li>
              <li>Annual company activity days and recognition awards</li>
              <li>Free car parking</li>
            </ul>
            <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn mb-3  mt-3">Apply Now <i class="custom-icon chevron-double-down"></i></button>
          </div>
        </div>
      </div>
      <div class="border-bottom border-primary"><div></div></div>

      <div class="row my-5">
        <div class="col-lg-9">
          <h3 class="mob-text-large d-none d-lg-block">Reception/ Front Desk</h3>
        </div>
        <div class="col-lg-3 text-lg-right d-none d-lg-block">
          <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn">Apply Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
        <div class="col-12 mt-3 mob-mt-0">
          <div class="row">
            <div class="col-lg-5">
              <picture>
                <source src="/img/workforus/img3.webp" type="image/webp"/>
                <source src="/img/workforus/img3.jpg" type="image/jpeg"/>
                <img src="/img/workforus/img3.jpg" class="w-100 h-auto" alt="Assistant Site Manager - We are vertigo"/>
              </picture>
            </div>
            <div class="col-lg-7">
              <h3 class="mob-text-large mt-4 d-lg-none">Duties & Responsibilities</h3>
              <p class="text-one-one">We are currently looking for both Full and Part time Receptionists Staff to join our team in Vertigo, in both our Newtownbreda & Titanic Quarter Sites! Being the face of the company, we are looking for a sociable and passionate person with a can-do outlook on life! </p>
              <p class="text-left mb-0"><a data-toggle="collapse" href="#vaccancy3" role="button" aria-expanded="false" aria-controls="vaccancy3">
                <b class="collapsed"><b class="text-white">Think you're right for the role?</b><br/><u>Learn More</u> <i class="fa fa-angle-double-down"></i></b>
                </a>
              </p>
              <div class="collapse" id="vaccancy3">
                
                <p><b>Duties & Responsibilities</b></p>
                <ul>
                  <li>Meeting and greeting customers.</li>
                  <li>Handling check-in of customers to ensure smooth arrival process.</li>
                  <li>Dealing with customer enquiries.</li>
                  <li>Promoting company products to existing and new customers.</li>
                  <li>Booking in of customers on computerised diary system.</li>
                  <li>General administrative duties.</li>
                  <li>Replying to customer enquiries via several communication channels.</li>
                  <li>To develop and maintain a comprehensive knowledge and thorough understanding of all services, rates and relevant administration and operational processes.</li>
                </ul>
                
                <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn mt-3 mb-3">Apply Now <i class="custom-icon chevron-double-down"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="border-bottom border-primary"><div></div></div>

      <div class="row my-5">
        <div class="col-lg-9">
          <h3 class="mob-text-large d-none d-lg-block">Crew Members</h3>
        </div>
        <div class="col-lg-3 text-lg-right d-none d-lg-block">
          <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn">Apply Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
        <div class="col-12 mt-3 mob-mt-0">
          <div class="row">
            <div class="col-lg-5">
              <picture>
                <source src="/img/workforus/img4.webp" type="image/webp"/>
                <source src="/img/workforus/img4.jpg" type="image/jpeg"/>
                <img src="/img/workforus/img4.jpg" class="w-100 h-auto" alt="Crew Members - We are vertigo"/>
              </picture>
            </div>
            <div class="col-lg-7">
              <h3 class="mob-text-large mt-4 d-lg-none">Crew Members</h3>
              <p class="text-one-one">Join our growing team and gain new skills in Northern Ireland's Premier Family Entertainment Centres! We have a range of roles available for Activity Marshals between our both sites, Newtownbreda and Titanic Quarter site.</p>
              <p class="text-left mb-0"><a data-toggle="collapse" href="#vaccancy4" role="button" aria-expanded="false" aria-controls="vaccancy4">
                <b class="collapsed"><b class="text-white">Think you're right for the role?</b><br/><u>Learn More</u> <i class="fa fa-angle-double-down"></i></b>
              </a></p>
              <div class="collapse" id="vaccancy4">
                
                <p><b>What are we looking for?</b></p>
                <ul>
                  <li>Passionate & enthusiastic individuals.</li>
                  <li>Keen to learn, develop and grow in a fast-paced environment.</li>
                  <li>Great interpersonal skills and strong communicator.</li>
                  <li>Happy to help and provide outstanding customers.</li>
                </ul>
                
                <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn mt-3 mb-3">Apply Now <i class="custom-icon chevron-double-down"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="border-bottom border-primary"><div></div></div>

      <div class="row my-5">
        <div class="col-lg-9">
          <h3 class="mob-text-large d-none d-lg-block">Catering</h3>
        </div>
        <div class="col-lg-3 text-lg-right d-none d-lg-block">
          <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn">Apply Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
        <div class="col-12 mt-3 mob-mt-0">
          <div class="row">
            <div class="col-lg-5">
              <picture>
                <source src="/img/workforus/img5.webp" type="image/webp"/>
                <source src="/img/workforus/img5.jpg" type="image/jpeg"/>
                <img src="/img/workforus/img5.jpg" class="w-100 h-auto" alt="Catering - We are vertigo"/>
              </picture>
            </div>
            <div class="col-lg-7">
              <h3 class="mob-text-large mt-4 d-lg-none">Catering</h3>
              <p class="text-one-one">We are looking for a fun and friendly customer focused individuals to join our catering team at We Are Vertigo, in both our Newtownbreda & Titanic Quarter Sites!</p>
              <p class="text-left mb-0"><a data-toggle="collapse" href="#vaccancy5" role="button" aria-expanded="false" aria-controls="vaccancy5">
                <b class="collapsed"><b class="text-white">Think you're right for the role?</b><br/><u>Learn More</u> <i class="fa fa-angle-double-down"></i></b>
              </a></p>
              <div class="collapse" id="vaccancy5">
                
                <p class=""><b>Duties and Responsibilities:</b></p>
                <ul>
                  <li>Accurate cash handling and till operation.</li>
                  <li>Ensuring an exceptional customer experience.</li>
                  <li>Preparation and presentation of food, with a focus on customer satisfaction.</li>
                  <li>Maintaining a high level of kitchen cleanliness and safe food handling.</li>
                  <li>Ensuring all relevant paperwork and temperature records are accurately recorded.</li>
                  <li>Assisting in opening and closing thecae and general cafe duties as required.</li>
                </ul>
                
                <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn mt-3 mb-3">Apply Now <i class="custom-icon chevron-double-down"></i></button>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <div class="border-bottom border-primary"><div></div></div>

      <div class="row my-5">
        <div class="col-lg-9">
          <h3 class="mob-text-large d-none d-lg-block">Party Host</h3>
        </div>
        <div class="col-lg-3 text-lg-right d-none d-lg-block">
          <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn">Apply Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
        <div class="col-12 mt-3 mob-mt-0">
          <div class="row">
            <div class="col-lg-5">
              <picture>
                <source src="/img/workforus/img6.webp" type="image/webp"/>
                <source src="/img/workforus/img6.jpg" type="image/jpeg"/>
                <img src="/img/workforus/img6.jpg" class="w-100 h-auto" alt="Party Host - We are vertigo"/>
              </picture>
            </div>
            <div class="col-lg-7">
              <h3 class="mob-text-large mt-4 d-lg-none">Party Host</h3>
              <p class="text-one-one">We are looking for a fun and friendly and naturally sociable individuals to join our Party Host team at We Are Vertigo, in both our Newtownbreda & Titanic Quarter Sites! Do you enjoy helping others, organising events and bringing happiness to others? Then apply now!</p>
              <p class="text-left mb-0"><a data-toggle="collapse" href="#vaccancy6" role="button" aria-expanded="false" aria-controls="vaccancy6">
                <b class="collapsed"><b class="text-white">Think you're right for the role?</b><br/><u>Learn More</u> <i class="fa fa-angle-double-down"></i></b>
              </a></p>
              <div class="collapse" id="vaccancy6">
                
                <p><b>Key responsibilities:</b></p>
                <ul>
                  <li>Organising and supervising party goers</li>
                  <li>Ensuring exceptional customer experience is provided</li>
                  <li>Greeting customers in a pleasant manor.</li>
                  <li>Ensuring the safety of guests while on site.</li>
                  <li>Cleanliness and set up in all party rooms, areas.</li>
                </ul>
                
                <button type="button" class="btn btn-primary btn-small btn-icon d-inline-block applyonlinebtn mt-3 mb-3">Apply Now <i class="custom-icon chevron-double-down"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="border-bottom border-primary"><div></div></div>

    </div>
  </div>
</div>
<div id="applyonline" class="container mt-5 py-5">
  <div class="row">
    <div class="col-12">
      <p class="mimic-h2">Apply</p>
      <p>Tell us who you are and send us your CV using this short form.</p>
      <apply-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'"></apply-form>
    </div>
  </div>
</div>
<div class="container-fluid position-relative pt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  document.addEventListener("DOMContentLoaded", function() {
  document.querySelectorAll(".applyonlinebtn").forEach(function(button) {
    button.addEventListener("click", function() {
      const targetElement = document.getElementById("applyonline");
      if (targetElement) {
        const offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset - 100;

        window.scrollTo({
          top: offsetTop,
          behavior: "smooth"
        });
      }
    });
  });
});

</script>
@endsection