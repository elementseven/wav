@php
$page = 'Help';
$pagetitle = 'Help & Frequently asked questions | We Are Vertigo';
$metadescription = "Take a look at some of our most Frequently Asked Questions, if there's something we haven't answered be sure to get in touch.";
$pagetype = 'white';
$pagename = 'help';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
	#app{
		overflow: visible !important;
	}
	.make-me-sticky {
	  position: -webkit-sticky;
		position: sticky;
    top: 110px;
	}
	.line-white{
		border-bottom: 1px solid #fff;
	}
</style>
@endsection
@section('header')
<header class="pt-5 z-2 overflow-hidden"> 
  <div class="">
  	<div class="container position-relative">
  		<img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
		  <div class="row pt-5 mt-lg-5 mob-pt-3 position-relative z-2">
		    <div class="col-lg-10 mt-5 text-center text-lg-start">
		      <p class="box-title-top text-primary text-uppercase">Some of our most</p>
		      <h1 class="mob-mt-0 page-title">Frequently Asked Questions</h1>
		      <p class="mb-4">Take a look at some of our most Frequently Asked Questions, if there's something we haven't answered be sure to <a href="/contact"><b>get in touch</b></a>.</p>
		    </div>
		  </div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="position-relative mt-5 mob-mt-0">
  <div class="">
  	<div class="container">
  		<div class="row">
  			<div class="col-lg-3 pb-5">
  				<div class="backdrop make-me-sticky">
	  				<div class="card backdrop-content bg-dark text-white px-4 pt-4 pb-2 mob-px-3">
			  			<p class="mimic-h3 mob-mb-0 text-center text-lg-start">Categories</p>
			  			<p class="text-small mb-0 text-center text-lg-start text-primary">Click on a category</p>
			        <div class="row half_row mob-mb-2">
			        	@foreach($categories as $key => $cat)
			          <div class="col-6 col-md-12 half_col">
			            <p class="mb-0 text-one mob-text-smaller"><b><a href="#{{$cat->slug}}" class="faqs-link text-white py-3 mob-py-2 d-block" data-target="{{$cat->slug}}">{{$cat->name}} </a></b></p>
			            @if($key < count($categories) -1)
			            <hr class="my-0 line-white @if($key == 6) d-none d-lg-block @endif" />
			            @endif
			          </div>
			         	@endforeach
			        </div>
			  		</div>
			  		<div class="backdrop-back"></div>
			  	</div>
		  	</div>
		  	<div class="col-lg-9 pl-5 mob-px-3 mob-pt-5 ps-lg-5">
			  @foreach($categories as $cat)
			    @if($cat->slug != 'ninja-master-course')
			      <div id="{{ $cat->slug }}" class="row pb-5">
			        <div class="col-12 faq-question">
			          <p class="mimic-h2 mb-1">{{ $cat->name }}</p>
			          @if($cat->name == 'Indoor Skydiving')
			            <p><i>Please note our weight limit for our indoor skydiving is 19 stone.</i></p>
			          @endif
			          <p class="text-small mb-4 text-primary">* Click on a question to see the answer</p>
			          @foreach($cat->faqs as $faq)
			            <p class="text-large mb-0">
			              <b>
			                <a id="question-{{ $faq->id }}" class="text-white d-block pb-3" data-bs-toggle="collapse" href="#faqid-{{ $faq->id }}" role="button" aria-expanded="false" aria-controls="faqid-{{ $faq->id }}">
			                  {{ $faq->question }}
			                </a>
			              </b>
			            </p>
			            <div class="collapse pb-4" id="faqid-{{ $faq->id }}">
			              {!! $faq->answer !!}
			            </div>
			            <hr class="line-primary mt-0"/>
			          @endforeach
			        </div>
			        @if($cat->name == 'Adventure Climbing')
			          <div class="col-12">
			            <p>Please NO crocs on the climbing wall & high ropes.</p>
			            <p class="mb-5">Please wear secure closed toe shoes, ideally trainers if you wish to partake on our climbing wall and high ropes courses.</p>
			          </div>
			        @endif
			      </div>
			    @endif
			  @endforeach

	
			</div>

		  </div>
		</div>
  </div>
</div>
<div class="container-fluid position-relative">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5 mob-mt-0 mob-mb-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="opening-hours-faqs"></div>
@endsection
@section('scripts')
<script>
document.querySelectorAll(".faqs-link").forEach(function(link) {
  link.addEventListener("click", function(e) {
    e.preventDefault();
    var target = link.getAttribute('data-target');
    var targetElement = document.getElementById(target);
    if (targetElement) {
      var offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset - 120;
      window.scrollTo({
        top: offsetTop,
        behavior: "smooth"
      });
    }
  });
});

document.getElementById("question-84").addEventListener("click", function() {
  var targetElement = document.getElementById("opening-hours-faqs");
  if (targetElement) {
    var offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset;
    window.scrollTo({
      top: offsetTop,
      behavior: "smooth"
    });
  }
});

document.getElementById("faqid-84").addEventListener("click", function() {
  var targetElement = document.getElementById("opening-hours-faqs");
  if (targetElement) {
    var offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset;
    window.scrollTo({
      top: offsetTop,
      behavior: "smooth"
    });
  }
});

</script>
@endsection