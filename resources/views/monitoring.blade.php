@php
$page = 'Monitoring';
$pagetitle = 'Monitoring Form | We Are Vertigo';
$metadescription = 'Monitoring Form We are Vertigo';
$pagetype = 'white';
$pagename = 'contact';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-pt-3">
    <div class="col-12 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">CAREERS</p>
      <h1 class="mob-mt-0">Monitoring Questionnaire</h1>
      <p>Please fill in the form below:</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container pt-4">
      <div class="row">
      	<div class="col-12">
          <monitoring-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" style="min-height: 450px;"></monitoring-form>
    		</div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function() {
    var hashcode = window.location.hash;
    console.log(hashcode);
    $('html,body').animate({scrollTop: $(hashcode).offset().top + 100});
  });
</script>
@endsection