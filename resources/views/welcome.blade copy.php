@php
$page = 'Homepage';
$pagetitle = "We Are Vertigo | NI's leading Inflatable Park & Activity Centre.";
$metadescription = "We Are Vertigo is Northern Ireland’s leading Indoor activities destination for all ages. The World's Largest Inflatable Park can be found in our Newtownbreda Centre along with an incredible Alpine Themed Adventure Climbing & Soft Play village. Our Titanic Quarter Centre houses the second of our huge Inflatable Parks, an action packed Ninja Master Course and Ireland’s Only Indoor Skydiving Tunnel.";
$pagetype = 'light';
$pagename = 'home';
$ogimage = 'https://wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<style>
.home-full-height{
  min-height: 750px;
}
</style>
{{-- <link rel="preload" as="image" href="/img/offers/birthdays/skydive.webp"> --}}
<link rel="preload" as="image" href="/img/home/easter.webp">
@endsection
@section('header')
<div id="activities-banner" class="container-fluid bg-primary position-relative z-2">
  <div class="row">
    <div class="col-12 text-center">
      <a href="/choose-your-activities">
        <p class="mb-0 text-dark text-one text-uppercase py-1 position-relative d-inline-block"><b>Let us help you choose your activities</b> <i class="custom-icon chevron-double-right ml-2 text-one"></i></p>
      </a>
    </div>
  </div>
</div>
<div  class="home-full-height mob-fixed home-easter bg z-1" style="margin-top: -34px; padding-top: 34px;">
  <div class="trans"></div>
  <home-header></home-header>
</div>
@endsection
@section('content')
<div id="scrollToActivities" class="container-fluid position-relative mob-pt-0">
  <img src="/img/graphics/chevrons-right-double.svg" class="home-chevrons-right-double" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container container-wide py-5 mob-py-0 pl-0 mob-px-3">
      <div class="row justify-content-center py-5">
        <div class="col-lg-5 pl-0">

          <p class="box-title-top text-primary text-center text-uppercase">EXHILARATING</p>
          <p class="mimic-h1 text-center mb-2 page-title">Activities</p>
        </div>
        <div class="col-12">
            <activities-slider></activities-slider>
        </div>
        <div class="col-12 text-center mt-5 mob-mt-3">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline ">Check out all our activities</span> <a href="{{route('activities.index')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Check them out <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative mob-mt-5">
  <div class="row">
    <div class="container container-wide pb-5">
      <div class="row pt-5 mob-py-0">
        <home-boxes></home-boxes>
        <div class="col-12 text-center mt-5 mob-mt-0">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0">Have a question? </span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<titanic-opening-hours></titanic-opening-hours>
<div class="container">
  <div class="row">
    <div class="col-12">
      <button class="btn btn-primary btn-icon" type="button" data-toggle="collapse" data-target="#titanicCollapse" aria-expanded="false" aria-controls="titanicCollapse">Easter opening hours <i class="custom-icon chevron-double-down"></i></button>
      <div class="collapse" id="titanicCollapse">
        <p class="mt-4"><b>Easter opening hours - Titanic Quarter</b></p>
        <p>Mon 3rd 11am-5pm<br/>
          Tue 4th 11am-5pm<br/>
          Wed 5th 11am-5pm<br/>
          Thur 6th 11am-5pm<br/>
          Fri 7th 11am-5pm<br/>
          Sat 8th 11am-5pm<br/>
          Sun 9th 11am-5pm<br/>
          Mon 10th 11am-5pm<br/>
          Tue 11th 11am-5pm<br/>
          Wed 12th 11am-5pm<br/>
          Thur 13th 11am-5pm<br/>
          Fri 14th 11am-5pm<br/>
          Sat 15th 11am-5pm<br/>
          Sun 16th 11am-5pm<br/>
          Mon 17th 11am-5pm<br/>
          Tue 18th 11am-5pm</p>
      </div>
    </div>
  </div>
</div>
<newtownbreda-opening-hours></newtownbreda-opening-hours>
<div class="container">
  <div class="row">
    <div class="col-12">
      <button class="btn btn-primary btn-icon mt-4" type="button" data-toggle="collapse" data-target="#newtCollapse" aria-expanded="false" aria-controls="newtCollapse">Easter opening hours <i class="custom-icon chevron-double-down"></i></button>
      <div class="collapse" id="newtCollapse">
        <p class="mt-4"><b>Easter opening hours - Newtownbreda</b></p>
        <p>Mon 3rd 10am-5pm<span class="text-primary">*</span><br/>
          Tue 4th 10am-5pm<span class="text-primary">*</span><br/>
          Wed 5th 10am-5pm<span class="text-primary">*</span><br/>
          Thur 6th 10am-5pm<span class="text-primary">*</span><br/>
          Fri 7th 10am-5pm<span class="text-primary">*</span><br/>
          Sat 8th 10am-5pm<span class="text-primary">*</span><br/>
          Sun 9th 10am-5pm<span class="text-primary">*</span><br/>
          Mon 10th 10am-5pm<span class="text-primary">*</span><br/>
          Tue 11th 10am-5pm<span class="text-primary">*</span><br/>
          Wed 12th 10am-5pm<span class="text-primary">*</span><br/>
          Thur 13th 10am-5pm<span class="text-primary">*</span><br/>
          Fri 14th 10am-5pm<span class="text-primary">*</span><br/>
          Sat 15th 10am-5pm<span class="text-primary">*</span><br/>
          Sun 16th 10am-5pm<span class="text-primary">*</span><br/>
          Mon 17th 10am-5pm<span class="text-primary">*</span><br/>
          Tue 18th 10am-5pm<span class="text-primary">*</span><br/><br/>
          <span class="text-primary">*</span>10am tots session ages 1-5 only! </p>
      </div>
    </div>
  </div>
</div>
<div id="home-section-1" class="container-fluid position-relative">
  <div class="row pb-5 mob-py-0">
    <div class="container pt-5">
      <div class="row pt-5 mob-pt-0">
        <div class="col-lg-10 text-center text-lg-left">
          <h1 class="mb-3">Northern Ireland's<br/>premier entertainment complex</h1>
          <p>We Are Vertigo is Northern Ireland’s leading Indoor activities destination for all ages. We believe you’re never too old to have fun, so we encourage adventure and exploring with everything we do.</p>
          <p>The World's Largest Inflatable Park can be found in our Newtownbreda Centre along with an incredible Alpine Themed Adventure Climbing & Soft Play village.</p>
          <p>The Titanic Quarter Centre houses the second of our huge Inflatable Parks, an action packed Ninja Master Course and Ireland’s Only Indoor Skydiving Tunnel.</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
});
</script>
<style>
  #home-activities .home-activity{position:relative;width:356px;height:500px;background-repeat:no-repeat;background-position:center center;background-size:cover;border-radius:0;text-align:left;vertical-align:top;overflow:hidden;cursor:pointer;padding-top:2rem;padding-left:2rem}#home-activities .home-activity img{z-index:1;position:relative;width:calc(100% - 1rem);margin-top:-3rem;display:block;margin-left:-1rem;-webkit-box-shadow:0 0 .5rem rgba(0,0,0,.4)!important;-moz-box-shadow:0 0 .5rem rgba(0,0,0,.4)!important;box-shadow:0 0 .5rem rgba(0,0,0,.4)!important}#home-activities .home-activity .activity-info{position:relative;padding-top:1rem;background-color:#f6cf3c}#home-activities .home-activity .activity-info .get3for2{position:absolute;top:1.5rem;right:2.5rem;width:90px;height:90px;z-index:2;box-shadow:none!important;-webkit-box-shadow:none!important;-moz-box-shadow:none!important}#home-activities .home-activity .activity-info p{margin-left:1.5rem}#home-activities .home-activity .activity-info p.activity-title{font-weight:700;font-size:2rem;padding-bottom:1rem;padding-right:1rem}#home-activities .home-activity .activity-info p.activity-title i{position:absolute;right:15px;bottom:17px;font-size:1.5rem;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}#home-activities .home-activity .activity-info p.activity-title i.custom-icon{width:1.3rem;height:1.3rem;bottom:22px;background-size:contain;background-repeat:no-repeat}#home-activities .home-activity .activity-info p.activity-title i.custom-icon.chevron-double-right{background-image:url("/img/icons/chevron-double-right.svg")}#home-activities .home-activity .activity-info p:hover.activity-title i{right:10px}#home-activities .owl-carousel .owl-stage-outer{overflow:visible!important}#home-activities .owl-stage{margin:auto}#home-activities .owl-theme .owl-nav{margin-top:1rem;margin-bottom:-30px}#home-activities .owl-theme .owl-nav .owl-prev{margin-right:240px;font-size:0;position:relative;color:#1d252d;padding:0;width:25px;height:33px;background-color:transparent}#home-activities .owl-theme .owl-nav .owl-prev:after{position:absolute;content:"";right:0;top:0;background-image:url("/img/icons/chevron-left.svg");background-size:contain;width:25px;height:33px;background-repeat:no-repeat}#home-activities .owl-theme .owl-nav .owl-next{font-size:0;position:relative;color:#1d252d;padding:0;width:25px;height:33px;background-color:transparent}#home-activities .owl-theme .owl-nav .owl-next:after{position:absolute;content:"";left:0;top:0;background-image:url("/img/icons/chevron-right.svg");background-size:contain;width:25px;height:33px;background-repeat:no-repeat}#home-activities .owl-theme .owl-dots{width:200px;margin:auto}#home-activities .owl-theme .owl-dots .owl-dot span{width:30px;height:8px;background:#fff;border-radius:0}#home-activities .owl-theme .owl-dots .owl-dot.active span{background:#f6cf3c}@media only screen and (min-device-width:768px) and (max-device-width:1024px) and (orientation:portrait){#home-activities .owl-theme .owl-dots{width:220px}#home-activities .owl-theme .owl-nav .owl-prev{margin-right:270px}}@media only screen and (max-width:767px){#home-activities .home-activity{padding-left:1rem;padding-right:1rem}#home-activities .owl-theme .owl-nav{margin:1rem -33px -30px auto}#home-activities .owl-theme .owl-dots{margin:auto;width:calc(100vw - 1rem)}#home-activities .owl-theme .owl-dots .owl-dot span{width:24px;height:6px;margin:3px}}.page-home .menu_btn{top:calc(1rem + 14px) !important;}
</style>
@endsection
@section('modals')

@endsection