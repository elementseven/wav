@php
$page = 'Gift Vouchers';
$pagetitle = 'Christmas Skydive Vouchers | We Are Vertigo';
$metadescription = "Take advantage of all of our awesome activities with our gift vouchers!";
$pagetype = 'white';
$pagename = 'gift-vouchers snow-bg bg-repeat';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@section('styles')
<link rel="preload" as="script" href="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"/>
<style>
#roller-widget{
  display: block;
  position: static;
  visibility: visible;
  top: 0px;
  width: 100%;
  border: none;
  overflow: hidden;
  min-height: 756px;
}
</style>
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
@endsection
@section('content')
<div class="container-fluid position-relative z-2 mt-5 pt-5">
  <div class="row mt-5 mob-mt-0">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="">
            <div id="bookonline" class="bg-white d-block position-relative" style="min-height:756px;margin-top:0;padding-top:0;">
              <iframe id="roller-widget" src="https://roller.app/wearevertigo/products/christmascashvouchers" style="margin-top:0;"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative mt-5 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection