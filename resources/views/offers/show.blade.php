@php
$page = 'Offer';
$pagetitle = $post->title . ' | We Are Vertigo';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'light';
$pagename = 'offers';
$ogimage = 'https://wearevertigo.com' . $post->getFirstMediaUrl('double');
@endphp
@section('styles')
<style>
[data-f-id]{
  display:none !important;
}
</style>
@if($post->id == 17)
{{-- 3for2 Birthdays --}}
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home"></script>
<link rel="preload" as="image" href="/img/parties/birthdays/3for2/1.webp">
@elseif($post->id == 7)
{{-- Toddler Mornings --}}
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/toddlercheckout/en/home"></script>
@elseif($post->id == 18)
{{-- 3for2 Ninja --}}
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/allactivitiescheckout/en/products"></script>
@elseif($post->id == 19)
{{-- 3for2 Skydive --}}
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/skydivecheckout/en/products"></script>
@endif
@endsection
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
@if($post->slug == '3-for-2-on-birthday-parties')
<div class="full-height mob-fixed birthdays-3for2-1 bg z-1" style="margin-top: -34px; padding-top: 34px;">
  <div class="trans"></div>
  <birthdays-3-for-2-slider></birthdays-3-for-2-slider>
</div>
@else
<header class="container position-relative pt-5 mob-pt-0">
  <div class="row pt-5 mob-mb-5 text-center text-lg-left">
    <div class="col-xl-12 pt-5 mob-px-4 mb-5 mob-mb-0">
      <p class="pt-5 mob-pt-0 mb-1 text-title box-title-top text-uppercase"><a href="{{route('offers-and-discounts')}}"><i class="fa fa-angle-double-left"></i>&nbsp; Browse offers</a></p>
      <h1 class="blog-title mb-3">{{$post->title}}</h1>
      <p class="text-larger mt-3 mb-4">{{$post->excerpt}}</p> 
      <p class="mb-1 mimic-h3"><b>Share this offer:</b></p>
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
    </div>
  </div>
</header>
@endif
@endsection
@section('content')
<div class="container mob-px-4 position-relative z-3">
  <div class="row pb-5 mob-py-0 text-center text-lg-left">
    @if($post->slug != '3-for-2-on-birthday-parties')
    <div class="col-12 mob-mt-0 mb-5">
      <div class="backdrop overflow-hidden pb-3">
        <picture>
          <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('offers','double-webp')}}" type="image/webp"/> 
          <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('offers','double')}}" type="{{$post->getFirstMedia('offers')->mime_type}}"/> 
          <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('offers','normal-webp')}}" type="image/webp"/> 
          <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('offers','normal')}}" type="{{$post->getFirstMedia('offers')->mime_type}}"/> 
          <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('offers','featured-webp')}}" type="image/webp"/> 
          <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('offers','featured')}}" type="{{$post->getFirstMedia('offers')->mime_type}}"/> 
          <img src="{{$post->getFirstMediaUrl('offers','featured')}} 1w,  {{$post->getFirstMediaUrl('offers','normal')}} 768w, {{$post->getFirstMediaUrl('offers','double')}}1200w" type="{{$post->getFirstMedia('offers')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
        </picture>
        <div class="backdrop-back" data-aos="fade-down-right"></div>
      </div>
    </div>
    @else
    <div class="col-12 mt-5 pt-5 mob-mt-0"></div>
    @endif
    <div class="col-xl-10 mob-mt-0 blog-body">
      {!!$post->content!!}
      @if($post->id == 7 || $post->id == 17 || $post->id == 19)
      <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.show()">Book Now <i class="custom-icon chevron-double-right"></i></button>
      @elseif($post->id == 18)
      <button type="button" class="btn btn-primary btn-icon mt-4" onclick="RollerCheckout.showProduct({productId: '685943'})">Book Now <i class="custom-icon chevron-double-right"></i></button>
      @endif
    </div>
  </div>
</div>
@if($post->id != 7 && $post->id != 17 && $post->id != 18 && $post->id != 19 && $post->id != 20)
<div class="container-fluid position-relative z-2 my-5">
  <img src="/img/graphics/chevrons-right.svg" class="cargo-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container text-center text-lg-left">
      <div class="row">
        <div class="col-12">
          <p class="mb-1 text-uppercase text-small text-primary"><b>Ready to go?</b></p>
          <p class="mimic-h1 mb-2">BOOK NOW</p>
          <p>We have two amazing locations to choose from, which would you like to book?</p>
        </div>
        <div class="col-lg-4">
          <a href="/locations/titanic-quarter">
            <button type="button" class="btn btn-primary btn-icon w-100 mb-3">Titanic Quarter - Book now <i class="custom-icon chevron-double-right"></i></button>
          </a>
        </div>
        <div class="col-lg-4">
          <a href="/locations/newtownbreda">
            <button type="button" class="btn btn-primary btn-icon w-100">Newtownbreda - Book now <i class="custom-icon chevron-double-right"></i></button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@if($post->id == 20)
<div class="container-fluid position-relative z-2 mb-5">
  <img src="/img/graphics/chevrons-right.svg" class="cargo-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container text-center text-lg-left">
      <div class="row">
        <div class="col-12">
          <a href="/locations/newtownbreda">
            <button type="button" class="btn btn-primary btn-icon">Book Now <i class="custom-icon chevron-double-right"></i></button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('scripts')
<script>
  $(window).load(function(){
    $('.ql-video').each(function(i, e){
      var width = $(e).width();
      $(e).css({
        "width": width,
        "height": width*(9/16)
      });
    });
  });
</script>
@endsection