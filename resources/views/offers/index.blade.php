@php
$page = 'Offers & Combos';
$pagetitle = 'Offers & Combos | We Are Vertigo';
$metadescription = "Check out our amazing discounted offers below and take advantage of our massive savings. Book today as they won’t last forever! Want to keep up to date with our offers and discounts? Subscribe to our weekly newsletter at the bottom of this page. ";
$pagetype = 'white';
$pagename = 'offers';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
	<img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right"/>
  <div class="row pt-5 mt-lg-5 mob-pt-3">
    <div class="col-lg-12 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Awesome</p>
      <h1 class="mob-mt-0 page-title">Offers</h1>
      <p class="mb-0">Check out our amazing discounted offers below and take advantage of our massive savings. Book today as they won’t last forever! Want to keep up to date with our offers and discounts? Subscribe to our weekly newsletter at the bottom of this page. </p>
    </div>
  </div>
</header>
@endsection
@section('content')
<offers-index></offers-index>
@endsection