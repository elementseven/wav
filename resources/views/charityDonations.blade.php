@php
$page = 'Charity Donations';
$pagetitle = 'Charity Donations | We Are Vertigo';
$metadescription = "As part of our strong commitment to the community, We are Vertigo strives to help local organizations raise much needed funds for their programs.";
$pagetype = 'white';
$pagename = 'charity-donations';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 mt-5 pt-lg-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Generous</p>
      <h1 class="mob-mt-0 page-title">Charity<br/>Donations</h1>
      <p>As part of our strong commitment to the community, We are Vertigo strives to help local organizations raise much needed funds for their programs.</p>
      <button type="button" class="btn btn-primary btn-icon booknowbtn">Donation Request <i class="custom-icon chevron-double-down"></i></button>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container position-relative ">
  <img src="/img/graphics/chevrons-left.svg" class="home-chevrons-left-1" alt="We are Vertigo chevrons left"/>
  <div class="row py-5 mob-py-0">
    <div class="container py-5">
        <charity-slider></charity-slider>
    </div>
  </div>
</div>
<div class="position-relative z-1">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right"/>
  <div class="">
    <div class="container pt-4">
      <div class="row">
        <div class="col-12">
          <p>Each year, we give generously to local charities in the form of Vouchers for their fundraising events.</p>

          <p>We try to respond positively to every request we receive but sometimes it is not possible to do so because of the large amount of requests we receive.
          (Please review the guidelines below to see how your organization might benefit from what we can offer.)</p>

          <p>We are Vertigo Donation Request Policy is as follows:</p>
          <p>Requests receive preference if submitted through our online system. Requests submitted via email, fax, mail or phone may take longer to respond to.</p>
           
          <p>All donation requests should be submitted at least six weeks prior to the date of the event.</p>
          <ul>
            <li><p>Please fill out the online form below completely to be considered.</p></li>
            <li><p>Your organization must be a nonprofit located in the Greater Belfast Area.</p></li>
            <li><p>Please do not submit the form more than once for a single event.</p></li>
            <li><p>Each organization is eligible, but not guaranteed, to receive one donation in a 12-month period.</p></li>
            <li><p>Donation items are typically sent within the week prior to the event date.</p></li>
          </ul>

          <p>Again, due to the high volume of requests received, we regret that we may not be able to fulfill every request.  If you have any additional questions or want to check the status of your request, you may email us at <a href="mailto:info@wearevertigo.com">info@wearevertigo.com</a>. You may not receive notification as to whether or not you have been approved prior to your event.</p>

          <p class="mb-0"><b><span class="text-primary">* PLEASE NOTE</span> - As we are currently operating with a heavily reduced admin team we will be not able to process any Charity Requests until further notice. Once our team is back to full strength we will be able to accept them again.</b></p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 py-5 mob-py-0">
          <hr class="line-primary my-5"/>
        </div>
        <div class="col-12" id="bookonline">
          <p class="mimic-h2">Donation Requests</p>
          <p>If you want to submit a donation request, please do so through this short form.</p>
          <charity-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></charity-form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="position-relative pt-5 mob-pt-0 z-2">
  <div class="">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  document.addEventListener("DOMContentLoaded", function() {
  document.querySelectorAll(".booknowbtn").forEach(function(button) {
    button.addEventListener("click", function() {
      const targetElement = document.getElementById("bookonline");
      if (targetElement) {
        const offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset - 100;

        window.scrollTo({
          top: offsetTop,
          behavior: "smooth"
        });
      }
    });
  });
});

</script>
@endsection