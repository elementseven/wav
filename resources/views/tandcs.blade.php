@php
$page = 'Terms & Conditions';
$pagetitle = 'Terms & Conditions | We Are Vertigo';
$metadescription = "We are Vertigo is Northern Ireland's leading entertainment complex for all ages. Trampoline, Adventure, Ski & Spa. Discounts available for online bookings.";
$pagetype = 'white';
$pagename = 'tandcs';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5 mob-pt-3 mt-lg-5">
    <div class="col-lg-12 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">For your information</p>
      <h1 class="mob-mt-0 page-title">Terms <span class="text-primary">&</span><br/>Conditions</h1>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative z-1">
  <div class="">
    <div class="container pt-4">
        <h2 class="mb-4">On Site & Our Services:</h2>
        <ol>
          <li><p>We are Vertigo operates a strict ‘NO PICNIC’ policy. Only food and drink purchased on the premises may be consumed on site & Birthday Cakes are permitted for booked party rooms ONLY and not for parties using our General Admission facilities. There is a strict NO alcohol policy when using any of our equipment here at We are Vertigo.</p></li>

          <li><p>Children under 16 years of age must be supervised by an accompanying adult at all times in the We are Vertigo centre.</p></li>

          <li><p>We would ask customers to report any accidents to reception before leaving the centre. Accidents reported after leaving our site cannot be dealt with appropriately.</p></li>

          <li><p>In our Adventure Centre, NO shoes are permitted in the Soft Play Centre and Socks must be worn at all times for H&S purposes. Shoes MUST be worn on the high and low ropes courses and climbing tower. Open toed or heeled footwear is NOT permitted on this apparatus for safety reasons.</p></li>

          <li><p>We have a strict NO REFUND policy on our services including vouchers (this, however, does not include those times where the centres are closed by the management and alternative arrangements cannot be agreed with the customer). Bookings can be rescheduled and transferred to other parts of our centre(s) however we do require a minimum of 24 hours’ notice and we will make every effort to accommodate you.</p></li>

          <li><p>The Management reserve the right to refuse admission. (Please see Section. 20 for additional information.)</p></li>

          <li><p>YNO foul language or abuse of our staff will be tolerated at any time.</p></li>

          {{-- <li><p>Please arrive 15 minutes prior to your birthday party in order to meet your party host for Adventure, Ski or Platinum Parties and 30 minutes prior to the start of your Trampoline Party.</p></li> --}}

          <li><p>Your booking reference is your proof of purchase. It is COMPULSORY that you present this booking confirmation when you arrive for your party, lesson or for general admission. Inability or refusal to do so will result in you NOT being admitted to the centre. Proof provided via smart phone or other smart devices will be accepted (Please ensure devices are fully charged to ensure we can see your booking if that option is to be used).</p></li>

          <li><p>Organisations using Purchase Orders for bookings MUST bring the Purchase Order (which has been approved and booked with We are Vertigo) with them as a proof of purchase in order to gain access to the centre together with the We are Vertigo booking form provided. All Purchase Orders must be paid in full 7 days prior to your event date. PLEASE BE AWARE THAT IF PAYMENT IS NOT PROVIDED IN ACCORDANCE WITH THIS TIMELINE WE ARE VERTIGO RESERVES THE RIGHT TO CANCEL YOUR BOOKING IN ORDER TO MAKE THE TIME SLOT AVAILABLE FOR OTHER CUSTOMERS.</p></li>
          
          <li><p>NO adult football tops or baseball caps are to be worn in centre at any time.</p></li>

          <li><p>When booking a birthday or corporate party please be aware that a maximum allotted time for one of the We are Vertigo function rooms is 30 minutes.</p></li>

          <li><p>We would ask all customers to create completed orders & waivers for every member of their visiting party for their Online General Admission orders. This is for Health & Safety purposes and to ensure that the centre is operating at maximum efficiency allowing us to give customers the best possible experience.</p></li>

          <li><p>In our Inflatable Park, We Are Vertigo Slippy Grippy Socks must be used at all times (when available). Adults are not permitted in and around the Inflatable Area unless they have paid full admission and are wearing the appropriate footwear. A viewing gallery has been provided to allow for the monitoring of children if you feel it is required.</p></li>

          <li><p>The Inflatable Park and the associated activies within, not limited to boucning,  are physical activities which all participants (or parent/guardian/ supervising adult for participants under the age of 18) must ensure that they are fit and able to participate in unaided. Participants must be able to remain in control and not risk causing injury to themselves or others.</p></li>

          <li><p>Users are not permitted to have any hard or sharp objects in their possession when using participating in the Inflatable Park. This includes plaster casts, all prostheses, large items of jewellery, belt buckles and mobile telephones (please note that this is not an exhaustive list).</p></li>

          <li><p>The Management reserve the right to ask any customer to remove themselves from the premises for any of the following reasons, aggressive behaviour towards staff, swearing (generally or at staff), inappropriate use of the facilities, inappropriate use of equipment (e.g. Trampoline misuse includes: double bouncing, running on trampolines, back flips, bringing items or material onto the courts which may cause harm to yourself and other bouncers. You will be warned. 3 offences and you will be removed from the park.)</p></li>
          
          <li><p>During Public, Bank and School Holiday periods We Are Vertigo reverts to their Peak Pricing Policy.</p></li>
          
          <li><p>All accidents onsite MUST be reported to a staff member at the time of the incident.</p></li>

          <li><p>You acknowledge that by entering the Company’s premises and participating in the activities that you grant We are Vertigo on your behalf and on the behalf of the child/ren or ward/s, the permission to photograph and/or record themselves, child/ren or ward/s in connection with the Company and to use the photograph and/or recording for all purposes, including advertising and promotional purposes, in any manner chosen. The participant also waives the right to inspect or approve the use of the photograph and/or recording, and acknowledges and agrees to the rights granted to this release are without compensation of any kind.</p></li>

          <li><p>All offers and promotions run independently and cannot be used in conjunction with each other.</p></li>

          <li><p>Promotional codes have no monetary value and cannot be transferred between activities.</p></li>

          <li><p>Offers and promotions cannot be used in addition to a voucher.</p></li>

          {{-- <li><p>Please note our vouchers are valid for 6 months from date of purchase, you are able to book for any future date using the voucher but once the voucher has expired you will be unable to use towards your booking.</p></li> --}}
          
        </ol>

        <p class="mt-5"><b>Failure to comply with the above, will mean that you will be asked to leave our premises.</b></p>


        <h2 class="mt-5 mb-4">Online Retail:</h2>

        <h4>What are cookies?</h4>

        <p>Cookies are plain text files containing small amounts of anonymous information that are created and stored on your computer, mobile or other device when you visit a website. They can be used for lots of different purposes, like saving shopping baskets, enabling smooth login, pre populating forms and remembering preferences.
        Cookies are not harmful and cannot scan or retrieve your personal information.</p>

        <h4 class="mt-5">Why do we use Cookies?</h4>

        <p>We use cookies to improve your site experience by making the website easier to use as well as provide us with information about how the website is being used so that we can make sure it is up to date and error free.</p>

        <h4 class="mt-5">Managing Your Cookies</h4>

        <p>You can manage your cookies through adjusting your browser settings on your computer or mobile device.</p>
        <p>For more information about managing your cookies and cookies in general, you can visit <a href="https://www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a>.</p>
        <p>Please be aware if you choose to restrict or block cookies it may impact the functionality or performance of the website, or prevent you from using certain services.</p>

        <h4 class="mt-5">Changes</h4>

        <p>If we decide to change our Terms and Conditions, we will post the changes on this page so that you’re always aware of what information we collect, how we use it and under what circumstances we disclose it.</p>

        <h4 class="mt-5">Ordering</h4>

        <p>When placing an order you agree that all the information given is accurate and complete. You will receive an acknowledgement email confirming receipt of your order and an Order ID: this email will only be an acknowledgement and will not constitute acceptance of your order.</p>
        <p>By placing an order, you make an offer to us to purchase the services you have selected. We may or may not accept your offer at our discretion.</p>

        <h4 class="mt-5">Site Usage</h4>

        <p>We are Vertigo disclaims damages of any kind, compensatory, direct, indirect or consequential damages, loss of data, income or profit, loss of or damage to property and claims of third parties implied or otherwise relating to use of this site.</p>

        <h4 class="mt-5">Ownership of Information</h4>

        <p>We are Vertigo has permission to use, owns or manages the majority of pictures and descriptions for the website. This information is copyright of We are Vertigo and may not be used by any other party without express permission of We are Vertigo. The We are Vertigo website is provided for use only for viewing by an end user. Taking of any content or prices by automated processes, such as spidering, will be deemed as a breach of copyright and is expressly forbidden except where specifically agreed with We are Vertigo or where We are Vertigo has expressly requested the process.</p>

        <h4 class="mt-5">Accuracy of Information</h4>

        <p>We put a lot of time and effort into describing and photographing the services we provide using our own photography and often our own descriptions of the services.</p>
        <p>Although we aim that every picture and description is 100% accurate, mistakes do occur so let us know if you see or read something that isn’t correct.</p>

        <h4 class="mt-5">Pricing Liability</h4>

        <p>All prices listed on www.wearevertigo.com are correct at the time of entering. Your credit card or debit card will be charged at the point of purchase.</p>
        <p>If a genuine error is discovered in the price of the services that you have ordered, we will inform you as soon as possible. We shall be under no obligation to fulfil an order for a product that was advertised at an incorrect price.</p>
        <p>The participant acknowledges that their participation in any and all of the activities provided by the Company entails known and unknown risks that could result in physical or emotional injury, paralysis, death or damage to themselves to property or to third parties. The participant further acknowledges that the activities provided by the Company require a reasonable level of fitness and ability and that the company has provided accurate information outlining these risks during the onsite induction procedure to include video and physical briefings as well as well-appointed signage to that effect which you as the customer have/will watch during entrance to the park for each session visited.</p>

{{--         <h4 class="mt-5">Special Offers</h4>
        <p>Please note that all free Indoor Skydiving vouchers received with a birthday party, are only valid for 2 months! You can book for any future date but it must be booked within the 2 month time frame.</p> --}}

    </div>
  </div>
</div>
<div class="container-fluid position-relative pt-5">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection