@php
$page = 'Gift Vouchers';
$pagetitle = 'Gift Vouchers | We Are Vertigo';
$metadescription = "Take advantage of all of our awesome activities at reduced prices with our memberhsip packages! Ranging from off-peak & single activity memberships to any time, all activity memberships, theres something for everyone!";
$pagetype = 'white';
$pagename = 'gift-vouchers';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5  z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Awesome</p>
      <h1 class="mob-mt-0 page-title">Gift Vouchers</h1>
      <p>With Vouchers you can; bounce to your heart's content in the Inflatable Parks, take to the skies at Indoor Skydiving, pump up your adrenaline on the Ninja Master Course and test your agility on the Adventure Climbing & Soft Play Area. The perfect gift for all ages, from 1-101! All Gift Vouchers valid for a full 12 months.</p>
      <button id="activity-top-menu-btn" type="button" class="btn btn-primary btn-icon booknowbtn">Buy Now <i class="custom-icon chevron-double-down"></i></button>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1 py-5 mb-5 mob-pt-0 mob-mt-0">
  <div class="row">
    <div class="container pt-5">
      <vouchers-slider></vouchers-slider>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-2 pt-5">
  <div class="row py-5 mob-pt-0">
    <div class="container py-5 mob-py-0">
      <div class="row">
        <div class="col-lg-10 text-center text-lg-left">
          <h1 class="mb-3"><span class="text-primary">Vertigo</span><br/> Cash Vouchers</h1>
          <p class="mb-4">Choose a gift card for any cash amount to be used for any of our thrill inducing activities - the perfect gift for big kids and small! Charge up this voucher with your chosen amount and give someone the gift of adventure! Let them choose from Indoor Skydiving, Inflatable Parks, Ninja Master Course and Adventure Climbing & Soft Play.</p>
          <button type="button" class="btn btn-primary btn-icon booknowbtn">Buy Now <i class="custom-icon chevron-double-down"></i></button>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="bookonline" class="container-fluid position-relative z-2 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bg-primary px-5 pt-5 text-center mob-px-3 mob-pt-2">
            <h2 class="text-uppercase text-dark mb-4">Buy Online</h2>
            <iframe id="roller-widget" src="https://roller.app/wearevertigo/products/giftvouchers"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative mt-5 mt-5">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection