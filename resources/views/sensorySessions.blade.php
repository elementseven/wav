@php
$page = 'Sesnsory Sessions';
$pagetitle = 'Sesnsory Sessions | We Are Vertigo';
$metadescription = "These sessions are designed for people with special needs, autism or sensory needs. At these sessions lights are dimmed, music is turned down, and our capacity is halved as to reduce any unneeded stress.";
$pagetype = 'white';
$pagename = 'Sesnsory Sessions';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <img src="/img/graphics/chevrons-right.svg" class="top-chevrons-right" alt="We are Vertigo chevrons right" data-aos="fade-down-left" data-aos-delay="300"/>
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-left">
      <p class="box-title-top text-primary text-uppercase">Every Thursday</p>
      <h1 class="mob-mt-0 page-title">Sesnsory Sessions</h1>
      <p>Join us for our sensory sessions every Thursday at our Newtownbreda site 5-6pm!</p>
      
      <button type="button" class="btn btn-primary btn-icon booknowbtn">Book Now <i class="custom-icon chevron-double-down"></i></button>
   </div>
  </div>
</header>
@endsection
@section('content')
<div class="container py-5">
  <sensory-slider></sensory-slider>
</div>
<div id="bookonline" class="container-fluid position-relative z-2 mob-mt-5">
  <img src="/img/graphics/chevrons-right.svg" class="cargo-chevrons-right-1 z-0" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 py-5 mt-5 mob-mt-0 text-center text-lg-left">
          <p>These sessions are designed for people with special needs, autism or sensory needs. At these sessions lights are dimmed, music is turned down, and our capacity is halved as to reduce any unneeded stress. </p>
          <p>Your bounce session ticket includes 1 hour on our inflatable park + carers pass. (Must be 18+) </p>
        </div>
        <div class="col-12 px-0">
          <div class="bg-primary px-5 py-5 text-center mob-px-3 mob-pt-4 position-relative z-2">
            <h2 class="text-uppercase text-dark mb-0 mob-mb-4">Book Online</h2>
            <iframe id="roller-widget" src="https://roller.app/wearevertigo/products/newtownbredatickets" style="display: block; position: static; visibility: visible; top: 0px; width: 100%; border: none; overflow: hidden; height: 690px; min-height: 350px;"> </iframe>
          </div>
        </div>
      </div>
    </div>
     <div class="container pt-5">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script>
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
@endsection
@section('modals')

@endsection