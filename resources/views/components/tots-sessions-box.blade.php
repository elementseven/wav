<div class="container-fluid">
  <div class="row" >
    <div class="col-12 px-0">
      <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 ipad-mb-0 odd">
        <div class="activity-image">
          <picture><source media="(min-width: 1500px)" srcset="/img/activities/tots.webp" type="image/webp"> 
            <source media="(min-width: 1500px)" srcset="/img/activities/tots.jpg" type="image/jpeg"> 
            <source media="(min-width: 768px)" srcset="/img/activities/tots-mid.webp" type="image/webp"> 
            <source media="(min-width: 768px)" srcset="/img/activities/tots-mid.jpg" type="image/jpeg"> 
            <source media="(min-width: 1px)" srcset="/img/activities/tots-mob.webp" type="image/webp"> 
            <source media="(min-width: 1px)" srcset="/img/activities/tots-mob.jpg" type="image/jpeg"> 
            <img src="/img/activities/tots-mob.jpg" type="image/jpeg" alt="Inflatable Park" width="390" height="273" class="backdrop-content lazy h-auto"></picture>
        </div>
        <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-right">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <!-- <div class="bubble-box">
                <p>Open every<br/>day this<br/>half-term!</p>
              </div> -->
              <p class="mimic-h1">TODDLER BOUNCE</p>
              <p class="mb-4 activity-summary">1 hour full rein on our huge inflatable park + 1 hour in our adventure centre equipped with low ropes course & 3 story soft play village! A massive hit with our little bouncers ages 1-5!</p>
              <p class="mb-0">
                <a href="/activities/toddler-bounce">
                  <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                </a>
              </p>
              <p class="button-after button-after-dark mt-1 pl-2 mb-0"><a href="/activities/toddler-bounce"><i class="return-arrow"></i> Book tots session here</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>