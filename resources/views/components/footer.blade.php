<footer class="text-center text-lg-start position-relative z-3 ">
  <div class="footer-bg"></div>
  <div class="position-relative pt-5 mob-mt-0 mob-pt-3 ipadp-pt-0 ipadp-mt-0">
    <div class="container pt-5">
      <div class="row mt-5 pt-5 ipadp-pt-0  mob-pt-0 mob-mt-0">
        <div class="col-lg-4 pr-5 d-none d-lg-block">
          <img data-src="/img/logos/logo.svg" class="menu_logo lazy mt-2" width="125" height="52" alt="wearevertigo Logo"/>
          <p class="mimic-h3 mt-4 mb-2">Get in touch</p>
          <p class="mb-1"><b>Email: <a href="mailto:info@wearevertigo.com" class="text-primary">info@wearevertigo.com</a></b></p>
          <p class="mb-1"><b>Phone: <a href="tel:08700661522" class="text-primary">0870 0661 522*</a></b></p>
          <p class="text-smallest">*Calls to our 0870 number cost 2 pence per minute, plus your phone company’s access charge.</p>
          <p>
            <a href="http://www.facebook.com/VertigoBelfast" aria-label="Facebook" rel="noreferrer" target="_blank"><span class="me-3"><font-awesome-icon :icon="['fab', 'square-facebook']"/></span></a>
            <a href="http://www.instagram.com/wearevertigobelfast/" aria-label="Instagram" rel="noreferrer" target="_blank"><span class="me-3"><font-awesome-icon :icon="['fab', 'instagram']"/></span></a>
            <a href="https://www.youtube.com/@Wearevertigo" target="_blank" aria-label="Subscribe to us on YouTube"><span class="me-3"><font-awesome-icon :icon="['fab', 'youtube']"/></span></a>
            <a href="http://twitter.com/weare_vertigo" aria-label="Twitter" rel="noreferrer" target="_blank"><span class="me-3"><font-awesome-icon :icon="['fab', 'x-twitter']"/></span></a>
            <a href="https://www.tiktok.com/@we_are_vertigo" target="_blank" aria-label="Follow us on TikTok"><span class="me-3"><font-awesome-icon :icon="['fab', 'tiktok']"/></span></a>
          </p>
        </div>
        <div class="col-lg-6 mob-mb-5 ipadp-mb-5">
          <mailing-list></mailing-list>
        </div>
        <div class="col-12 d-lg-none mob-px-4">
          <img data-src="/img/logos/logo.svg" class="lazy menu_logo mt-2" width="125" height="52" alt="wearevertigo Logo"/>
          <p class="mimic-h3 mt-4 mb-2">Get in touch</p>
          <p class="mb-1"><b>Email: <a href="mailto:info@wearevertigo.com" class="text-primary">info@wearevertigo.com</a></b></p>
          <p class="mb-1"><b>Phone: <a href="tel:08700661522" class="text-primary">0870 0661 522*</a></b></p>
          <p class="text-smallest">*Calls to our 0870 number cost 2 pence per minute, plus your phone company’s access charge.</p>
          <p>
            <a href="http://www.facebook.com/VertigoBelfast" aria-label="Facebook" rel="noreferrer"><span class="me-3"><font-awesome-icon :icon="['fab', 'square-facebook']"/></span></a>
            <a href="http://www.instagram.com/wearevertigobelfast/" aria-label="Instagram" rel="noreferrer"><span class="me-3"><font-awesome-icon :icon="['fab', 'instagram']"/></span></a>
            <a href="https://www.youtube.com/@Wearevertigo" target="_blank" aria-label="Subscribe to us on YouTube"><span class="me-3"><font-awesome-icon :icon="['fab', 'youtube']"/></span></a>
            <a href="http://twitter.com/weare_vertigo" aria-label="Twitter" rel="noreferrer"><span class="me-3"><font-awesome-icon :icon="['fab', 'x-twitter']"/></span></a>
            <a href="https://www.tiktok.com/@we_are_vertigo" target="_blank" aria-label="Follow us on TikTok"><span class="me-3"><font-awesome-icon :icon="['fab', 'tiktok']"/></span></a>
          </p>
        </div>
      </div>
      <div class="row mob-px-4">
        <div class="col-lg-4 mt-4 pr-5 mob-px-3 ipadp-px-3">
          <p class="mimic-h3 mb-3">Find Us</p>
          <p>Unit 1 Cedarhurst Road<br/>Newtownbreda Factory Estate<br/>Belfast, BT8 7RH</p>
          <p><a href="https://www.google.com/maps/dir//We+Are+Vertigo+-+Newtownbreda+We+Are+Vertigo+-+Newtownbreda,+Newtownbreda+Industrial+Estate,+1+Cedarhurst+Rd,+Belfast+BT8+7RH/@54.5686495,-5.9072029,14z/data=!4m8!4m7!1m0!1m5!1m1!1s0x48610f2c72dcd001:0x559dbce868e15715!2m2!1d-5.9167121!2d54.5486098" target="_blank" aria-label="Google Maps Newtownbreda" rel="noreferrer" class="text-primary"><b>Get Directions</b> <font-awesome-icon :icon="['fas', 'fa-angle-double-right']"/></a></p>
        </div>
        <div class="col-lg-4 mt-4">
          <p class="text-title read-more-news text-uppercase mb-0 text-primary"><span class="me-1"><font-awesome-icon :icon="['far', 'clock']"/></span> Opening Hours</p>
          <p class="mimic-h3 mb-2">Inflatable Park</p>
          <p class="mb-2"><b>Mon</b> -  Closed<br/>
          <b>Tue</b>  - Closed<br/>
          <b>Wed</b>  - Closed<br/>
          <b>Thu</b> -  Closed<br/>
          <b>Fri</b>  - 4pm - 9pm<br/>
          <b>Sat</b>  - 10am - 5pm<span class="text-primary">*</span><br/>
          <b>Sun</b>  - 10am - 5pm<span class="text-primary">*</span></p>
          <p class="text-smaller"><span class="text-primary">*</span> 10am tots sessions. Normal sessions start at 11am</p>
        </div>
        <div class="col-lg-4 mt-4 mob-mb-5">
          <p class="text-title read-more-news text-uppercase mb-0 text-primary"><span class="me-1"><font-awesome-icon :icon="['far', 'clock']"/></span> Opening Hours</p>
          <p class="mimic-h3 mb-2">Adventure Centre</p>
          <p class="mb-2"><b>Mon</b> -  Closed<br/>
          <b>Tue</b>  -  Closed<br/>
          <b>Wed</b>  -  Closed<br/>
          <b>Thu</b> -   Closed<br/>
          <b>Fri</b>  -  4pm - 9pm<br/>
          <b>Sat</b>  - 12pm - 5pm<br/>
          <b>Sun</b>  - 12pm - 5pm</p>
        </div>
      </div>
      <div class="row d-none d-lg-flex my-4">
        <div class="col-12">
          <p class="text-smallest mob-line-height"><a href="/activities">Activities</a> | <a href="/parties-and-groups">Parties & Groups</a> <span class="d-none d-md-inline"> | </span><br class="d-md-none"/><a href="/help">Help</a> | <a href="/offers-and-discounts">Offers</a><span class="d-none d-md-inline"> | </span><br class="d-md-none"/><a href="/contact">Contact</a> | <a href="/charity-donations">Charity Donations</a> | <a href="/work-for-us">Work for us</a> </p>
        </div>
      </div>
    </div>
  </div>
  <div class="text-center bg-darker position-relative z-2">
    <div class="container py-2">
      <div class="row">
        <div class="col-lg-4 text-center text-lg-start">
          <p class="text-smallest mb-0 mob-mb-2 mob-mt-2"><a href="/terms-and-conditions">Terms & Conditions</a> | <a href="/privacy-policy">Privacy Policy</a></p>
        </div>
        <div class="col-lg-8 text-center text-lg-end">
          <p class="text-smallest mb-0">&copy;2024 We are Vertigo, All Rights Reserved<span class="d-none d-md-inline"> | </span><br class="d-md-none"/><a href="https://elementseven.co" target="_blank" aria-label="Element Seven Web Design Belfast" rel="noreferrer">Website by Element Seven</a></p>
        </div>
      </div>
    </div>
  </div>
</footer>