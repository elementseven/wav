<div class="container-fluid">
  <div class="row" >
    <div class="col-12 px-0">
      <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 ipad-mb-0">
        <div class="activity-image">
          <picture><source media="(min-width: 1500px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-double-webp.webp" type="image/webp"> <source media="(min-width: 1500px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-double.jpg" type="image/jpeg"> <source media="(min-width: 768px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-normal-webp.webp" type="image/webp"> <source media="(min-width: 768px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-normal.jpg" type="image/jpeg"> <source media="(min-width: 1px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-featured-webp.webp" type="image/webp"> <source media="(min-width: 1px)" srcset="/storage/44/conversions/home-inflatable-newtownbreda-featured.jpg" type="image/jpeg"> <img src="/storage/44/conversions/home-inflatable-newtownbreda-featured.jpg" type="image/jpeg" alt="Inflatable Park" width="390" height="273" class="backdrop-content lazy h-auto"></picture>
        </div>
        <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-left">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <!-- <div class="bubble-box">
                <p>Open every<br/>day this<br/>half-term!</p>
              </div> -->
              <p class="mimic-h1">Inflatable Park</p>
              <p class="mb-4 activity-summary">The World’s Largest Indoor Inflatable Activity Course! Inflatable Park is a whopping 30,000 square ft inflatable playground where childhood fantasy is reality. Ages 4-16 recommended, with toddler section available for ages 1-3!</p>
              <p class="mb-0">
                <a href="/activities/newtownbreda/inflatable-park">
                  <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                </a>
              </p>
              <p class="button-after button-after-dark mt-1 pl-2 mb-0"><a href="/activities/toddler-bounce" ><i class="return-arrow"></i> Book tots session here</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>