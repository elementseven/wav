<div class="container-fluid">
  <div class="row" >
    <div class="col-12 px-0">
      <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 ipad-mb-0">
        <div class="activity-image">
          <picture>
            <source media="(min-width: 1500px)" srcset="/storage/50/conversions/home-adventure-double-webp.webp" type="image/webp"> 
            <source media="(min-width: 1500px)" srcset="/storage/50/conversions/home-adventure-double.jpg" type="image/jpeg"> 
            <source media="(min-width: 768px)" srcset="/storage/50/conversions/home-adventure-normal-webp.webp" type="image/webp"> 
            <source media="(min-width: 768px)" srcset="/storage/50/conversions/home-adventure-normal.jpg" type="image/jpeg"> 
            <source media="(min-width: 1px)" srcset="/storage/50/conversions/home-adventure-featured-webp.webp" type="image/webp"> 
            <source media="(min-width: 1px)" srcset="/storage/50/conversions/home-adventure-featured.jpg" type="image/jpeg"> 
            <img src="/storage/50/conversions/home-adventure-featured.jpg" type="image/jpeg" width="390" height="273" alt="Adventure Climbing" class="backdrop-content lazy h-auto">
          </picture>
        </div>
        <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-start" data-aos="fade-left">
          <div class="d-table w-100 h-100">
            <div class="d-table-cell align-middle w-100 h-100">
              <!-- <div class="bubble-box">
                <p>Open every<br/>day this<br/>half-term!</p>
              </div> -->
              <p class="mimic-h1">Adventure Centre</p>
              <p class="mb-4 activity-summary">The Adventure Climbing & Soft Play area packs all the obstacles you need under one roof to challenge the true adventurer in you. With High and Low ropes courses, 30ft Climbing Wall and 3 Tier Soft Play Village, it is perfect for ages 1-16!</p>
              <p class="mb-0">
                <a href="/activities/newtownbreda/adventure-climbing">
                  <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                </a>
              </p>
              <p class="button-after button-after-dark mt-1 pl-2 mb-0"><a href="/activities/newtownbreda/adventure-climbing" ><i class="return-arrow"></i> More About Adventure Centre</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>