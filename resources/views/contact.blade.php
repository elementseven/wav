@php
$page = 'Contact';
$pagetitle = 'Contact | We Are Vertigo';
$metadescription = 'Contact We are Vertigo, get in touch, telephone we are vertigo, directions we are vertigo, call we are vertigo';
$pagetype = 'white';
$pagename = 'contact';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5">
  <div class="row pt-5 mob-pt-3 mt-lg-5">
    <div class="col-lg-8 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Get in touch</p>
      <h1 class="mob-mt-0 page-title">Contact Us</h1>
      <p>If you have a question you might find the answer on our <a href="/faqs"><b>FAQs Page</b></a>, otherwise use the contact form below to send us a message!</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="position-relative">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right"/>
  <div class="position-relative z-2">
    <div class="container pt-4">
      <div class="row">
      	<div class="col-lg-8">
          <contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'" style="min-height: 450px;"></contact-page-form>
    		</div>
        <div class="col-lg-4 mb-5 ps-5 mob-px-3 mob-mt-5 text-center text-lg-start">
          <p class="mimic-h3 mb-3 text-uppercase">Contact Details</p>
          <p class="mb-0 text-one"><a href="mailto:info@wearevertigo.com"><b><span class="text-white">Email:</span> info@wearevertigo.com</b></a></p>
          <p class="mb-0 text-one"><a href="tel:08700661522"><b><span class="text-white">Phone:</span> 0870 0661 522*</b></a></p>
          <p class="text-smallest pr-4 mob-px-0">*Calls to our 0870 number cost 2 pence per minute, plus your phone company’s access charge.</p>
          <p class="mb-0 text-large mt-3">
            <a href="http://www.facebook.com/VertigoBelfast" target="_blank" class="text-white"><i class="fa fa-facebook"></i></a>
            <a href="http://www.instagram.com/wearevertigobelfast/" target="_blank" class="text-white"><i class="fa fa-instagram ms-3"></i></a>
            <a href="http://twitter.com/weare_vertigo" target="_blank" class="text-white"><i class="fa fa-twitter ms-3"></i></a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="mb-5"></div>
{{-- <contact-page-map></contact-page-map> --}}
@endsection
@section('scripts')
<script>
document.addEventListener("DOMContentLoaded", function() {
  var hashcode = window.location.hash;
  console.log(hashcode);

  if (hashcode) {
    var targetElement = document.querySelector(hashcode);
    if (targetElement) {
      var offsetTop = targetElement.getBoundingClientRect().top + window.pageYOffset - 100;

      window.scrollTo({
        top: offsetTop,
        behavior: "smooth"
      });
    }
  }
});

</script>
@endsection