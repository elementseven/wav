@php
$page = 'Choose Your Activities';
$pagetitle = 'Choose Your Activities | We Are Vertigo';
$metadescription = "Discover what Vertigo Newtownbreda have to offer! With a huge Inflatable Park & Adventure Centre you’ll create memories that last a life-time with fun!";
$pagetype = 'white';
$pagename = 'choose-your-activities';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5 mt-lg-5 mob-pt-3">
    <div class="col-lg-10 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Let us help you</p>
      <h1 class="mob-mt-0 page-title">Choose <span class="text-primary">Your</span> Activities</h1>
      <p>Discover what Vertigo Newtownbreda has to offer! With a huge Inflatable Parks & Adventure Centre you’ll create memories that last a life-time with fun!</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container pt-4">
        
    </div>
  </div>
</div>
<div class="container pt-3 position-relative z-2">
  <div class="row pb-5 mob-py-0">
    <div class="col-12">
      <h2>What activity suits us best?</h2>
      <p class="text-primary"><i>Our Activities based by age:</i></p>
    </div>
    <div class="col-lg-5">
      <div class="backdrop">
        <div class="backdrop-content">
          <picture>
            <source srcset="/img/choose/1.webp" type="image/webp"/> 
            <source srcset="/img/choose/1.jpg" type="image/jpeg"/> 
            <img src="/img/choose/1.jpg" type="image/jpeg" alt="We are Vertigo" class="lazy w-100" />
          </picture>
        </div>
        <div class="backdrop-back" data-aos="fade-down-right"></div>
      </div>
    </div>
    <div class="col-lg-7">
      <h4 class="mob-mt-5">Ages 1 - 5</h4>
      <ul>
        <li><p>We have our fantastic toddler mornings in our Newtownbreda Site. This caters for kids aged 1-5 ONLY. This gives kids one hour in our <a href="/activities/inflatable-park"><u>Inflatable Park</u></a> main area, followed by one hour in our fantastic <a href="/activities/adventure-centre"><u>Adventure Centre</u></a>. Each ticket also includes ONE adult for a supervisory role. (We only allow one adult per child to ensure that there are no more adults than toddlers in the main park for any part of the session. Extra availability for an additional adult may be available on the day - please ask staff at check-in for availability).</p></li>
        <li><p>Our <a href="/activities/adventure-centre"><u>Adventure Centre</u></a> is a fantastic place if your kids love to climb on our low ropes course or visit a smaller soft play area! All kids must be aged 1+ to participate.</p></li>
      </ul>

      <p>All adults who wish to participate, no matter which activity, need to book. If you just wish to spectate, you do not need to book. We have spectator areas where you can see all areas and grab a tea, coffee or some snacks.</p>
    </div>
  </div>
  <div class="row my-5">
    <div class="col-lg-5">
      <div class="backdrop">
        <div class="backdrop-content">
          <picture>
            <source srcset="/img/choose/4.webp" type="image/webp"/> 
            <source srcset="/img/choose/4.jpg" type="image/jpeg"/> 
            <img src="/img/choose/4.jpg" type="image/jpeg" alt="We are Vertigo" class="lazy w-100" />
          </picture>
        </div>
        <div class="backdrop-back" data-aos="fade-down-right"></div>
      </div>
    </div>
    <div class="col-lg-7">
      <h4 class="mob-mt-5">Ages 4+</h4>
      <ul>
        <li><p>Our fantastic <a href="/activities/inflatable-park"><u>Inflatable Park</u></a> - the main area of our Inflatable Parks is for anyone (even adults!) over the age of 4 (children under 4 years can make sure of the toddler area!)</p></li>
        <li><p>Our <a href="/activities/adventure-centre"><u>Adventure Centre</u></a> is a fantastic place if your kids love to climb or visit a HUGE 3 level soft play area! </p></li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <h3 class="mt-5">What to book for a group of teenagers? </h3>
      <p>That is totally up to your teenagers. Each group is different. Ee have our fantastic <a href="/activities/inflatable-park"><u>Inflatable Park</u></a>. All our information about activities are listed below. And we have video tours for each activity, so you or your group can choose what activity or activities best suit them. </p>
    </div>
    <!-- <div class="col-lg-6">
      <h3 class="mt-5">What to book for a group of adults? </h3>
      <p>Try our fantastic <a href="/activities/inflatable-park"><u>Inflatable Park</u></a>. All our information about activities are listed below. And we have video tours for each activity, so you or your group can choose what activity or activities best suit them. </p>
    </div> -->
  </div>
  <div class="row">
    <div class="col-12">
      <h2 class="mt-5">Our Activities:</h2>
      <ul>
        <li><p>Inflatable Park - The World's Largest Indoor Inflatable Activity Course! Inflatable Park is a whopping 30,000 square ft inflatable playground where childhood fantasy is reality. To find out more about the Inflatable Park, <a href="/activities/inflatable-park"><b>click here!</b></a> </p></li>
        <li><p>Adventure Centre - The Adventure Climbing & Soft Play area packs all the obstacles you need under one roof to challenge the true adventurer in you. With High and Low ropes courses, 30ft Climbing Wall and 3 Tier Soft Play Village To find out more about the Adventure Centre, <a href="/activities/adventure-centre"><b>click here!</b></a></p></li>
        <li><p>Toddler Morning - The Inflatable Park and Adventure & Soft Play Centre are filled with sensory activities and safe fun for them to explore. Tots not only have the toddler area but the whole park to themselves... the best bit? Adults can participate alongside them in the main area. Top off a morning of fun at the Adventure Climbing & Soft Play Area! What’s included? 1 hour of Inflatable Park AND 1 hour in the Adventure Climbing & Soft Play Area.<br/>To book the tots morning, <a href="/locations/newtownbreda#bookonline"><b>click here!</b></a></p></li>
      </ul>

    </div>
  </div>
</div>
<div id="scrollToActivities" class="container-fluid position-relative">
  <img src="/img/graphics/chevrons-left.svg" class="home-chevrons-left-1" alt="We are Vertigo chevrons left" data-aos="fade-up-right"/>
  <div class="row">
    <div class="container container-wide py-5 mob-py-0 pl-0 mob-px-3">
      <div class="row py-5">
        <div class="col-12 pl-0">
          <activities-slider></activities-slider>
        </div>
        <div class="col-12 text-center mt-5 mob-mt-3">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline ">Check out all our activities</span> <a href="{{route('activities.index')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Check them out <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<style>
  #home-activities .home-activity{position:relative;width:356px;height:500px;background-repeat:no-repeat;background-position:center center;background-size:cover;border-radius:0;text-align:left;vertical-align:top;overflow:hidden;cursor:pointer;padding-top:2rem;padding-left:2rem}#home-activities .home-activity img{z-index:1;position:relative;width:calc(100% - 1rem);margin-top:-3rem;display:block;margin-left:-1rem;-webkit-box-shadow:0 0 .5rem rgba(0,0,0,.4)!important;-moz-box-shadow:0 0 .5rem rgba(0,0,0,.4)!important;box-shadow:0 0 .5rem rgba(0,0,0,.4)!important}#home-activities .home-activity .activity-info{position:relative;padding-top:1rem;background-color:#f6cf3c}#home-activities .home-activity .activity-info .get3for2{position:absolute;top:1.5rem;right:2.5rem;width:90px;height:90px;z-index:2;box-shadow:none!important;-webkit-box-shadow:none!important;-moz-box-shadow:none!important}#home-activities .home-activity .activity-info p{margin-left:1.5rem}#home-activities .home-activity .activity-info p.activity-title{font-weight:700;font-size:2rem;padding-bottom:1rem;padding-right:1rem}#home-activities .home-activity .activity-info p.activity-title i{position:absolute;right:15px;bottom:17px;font-size:1.5rem;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}#home-activities .home-activity .activity-info p.activity-title i.custom-icon{width:1.3rem;height:1.3rem;bottom:22px;background-size:contain;background-repeat:no-repeat}#home-activities .home-activity .activity-info p.activity-title i.custom-icon.chevron-double-right{background-image:url("/img/icons/chevron-double-right.svg")}#home-activities .home-activity .activity-info p:hover.activity-title i{right:10px}#home-activities .owl-carousel .owl-stage-outer{overflow:visible!important}#home-activities .owl-stage{margin:auto}#home-activities .owl-theme .owl-nav{margin-top:1rem;margin-bottom:-30px}#home-activities .owl-theme .owl-nav .owl-prev{margin-right:240px;font-size:0;position:relative;color:#1d252d;padding:0;width:25px;height:33px;background-color:transparent}#home-activities .owl-theme .owl-nav .owl-prev:after{position:absolute;content:"";right:0;top:0;background-image:url("/img/icons/chevron-left.svg");background-size:contain;width:25px;height:33px;background-repeat:no-repeat}#home-activities .owl-theme .owl-nav .owl-next{font-size:0;position:relative;color:#1d252d;padding:0;width:25px;height:33px;background-color:transparent}#home-activities .owl-theme .owl-nav .owl-next:after{position:absolute;content:"";left:0;top:0;background-image:url("/img/icons/chevron-right.svg");background-size:contain;width:25px;height:33px;background-repeat:no-repeat}#home-activities .owl-theme .owl-dots{width:200px;margin:auto}#home-activities .owl-theme .owl-dots .owl-dot span{width:30px;height:8px;background:#fff;border-radius:0}#home-activities .owl-theme .owl-dots .owl-dot.active span{background:#f6cf3c}@media only screen and (min-device-width:768px) and (max-device-width:1024px) and (orientation:portrait){#home-activities .owl-theme .owl-dots{width:220px}#home-activities .owl-theme .owl-nav .owl-prev{margin-right:270px}}@media only screen and (max-width:767px){#home-activities .home-activity{padding-left:1rem;padding-right:1rem}#home-activities .owl-theme .owl-nav{margin:1rem -33px -30px auto}#home-activities .owl-theme .owl-dots{margin:auto;width:calc(100vw - 1rem)}#home-activities .owl-theme .owl-dots .owl-dot span{width:24px;height:6px;margin:3px}}.page-home .menu_btn{top:calc(1rem + 14px) !important;}
</style>
@endsection