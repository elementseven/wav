@php
$page = 'Birthdays';
$pagetitle = 'Birthday Parties | We Are Vertigo';
$metadescription = "We Are Vertigo is THE place to celebrate your birthday! Soar high as an Indoor Skydiver or explore our Adventure Centre, test your agility on the Ninja Master Course or bounce till you drop at our massive Inflatable Parks!";
$pagetype = 'white';
$pagename = 'offers';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<link rel="preload" as="image" href="/img/bg/home-inflatable-titanic.webp">
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/birthdayscheckout/en/home"></script>
<style>
#roller-widget{
  display: block;
  position: static;
  visibility: visible;
  top: 0px;
  width: 100%;
  border: none;
  overflow: hidden;
  min-height: 350px;
}
</style>
@endsection
@section('header')
<header class="full-height mob-fixed birthdays-3for2-5 bg z-1" style="margin-top: -34px; padding-top: 34px;">
	<div class="trans"></div>
	<birthdays-3-for-2-slider></birthdays-3-for-2-slider>
</header>
@endsection
@section('content')
<div class="container">
   <div class="row py-5 mt-5 text-start">
    <div class="col-12">
      <p class="mimic-h2"><span class="text-primary">NEWTOWNBREDA</span> <br class="d-lg-none" />BIRTHDAY PARTY!</p>
      <p>Our Newtownbreda site is situated in Newtownbreda and is home to the largest indoor inflatable park in the world! Perfect for those adventure seekers! Join us for an inflatable park brithday full of fun challenges or try our adnventure centre eqipped with high & low ropes courses, rock climbing & a huge soft play village! Can't decide...? Why not double up on the fun with a combo birthday! That is 1 hour on our huge infltable park + 2 hours in our adventure centre!</p>
      <p class="text-large"><b>Here's what will happen on your birthday party!!</b></p>
      <ul>
        <li><p>Please arrive at the front desk 30 mins before your birthday is scheduled!</p></li>
        <li><p>Here you will meet your party host who will talk you through your special day!</p></li>
        <li><p>If you are an inflatable park birthday you will recieve your grippy socks!</p></li>
        <li><p>Your group will be shown to your starting destination!</p></li>
        <li><p>Enjoy your big day with all your friends! You'll get a 1 hour session or if it is a combo it'll be 3 hours!</p></li>
        <li><p>Finish your day on a high! You'll then be shown to your party room for pizza, icecream & unlimited juice! (*Please note we have a strict no picnic policy however you are more than welcome to bring decorations, a birthday cake and party bags!)</p></li>
        <li><p><b>FREE RETURN PASS! For parties over 10 guests, the birthday child will receive a FREE return pass voucher which will be sent after the party via email!</b></p></li>
      </ul>
      <button class="btn btn-primary btn-icon" onclick="RollerCheckout.show()" type="button">BOOK NOW <i class="custom-icon chevron-double-right"></i></button>
    </div>
  </div>
</div>
<div class="container mb-5 mob-mb-0">
  <div class="row">
    <div class="col-12 text-center py-5">
      <p class="mimic-h2">GET YOUR PARTY INVITES HERE!</p>
      <a href="/docs/vertigo-birthday-invitation.pdf">
        <button class="btn btn-primary">Download</button>
      </a>
    </div>
  </div>
</div>
<div class="container position-relative z-1 pb-5 mb-5 mob-mt-0 pl-0 mob-px-3">
  <div class="row justify-content-center">
    <div class="col-lg-10 pl-0 mob-px-3">
      <birthdays-slider></birthdays-slider>
    </div>
  </div>
</div>
<div class="container-fluid position-relative z-1">
  <div class="row">
     <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
@endsection