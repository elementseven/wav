@php
$page = 'Gift Vouchers';
$pagetitle = 'Gift Vouchers | We Are Vertigo';
$metadescription = "Take advantage of all of our awesome activities at reduced prices with our memberhsip packages! Ranging from off-peak & single activity memberships to any time, all activity memberships, theres something for everyone!";
$pagetype = 'white';
$pagename = 'gift-vouchers';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/activitygiftvouchers/en/products"></script>
@endsection
@section('header')
<header class="container container-wide position-relative pt-5 z-2">
  <div class="row pt-5 mob-mt-3">
    <div class="col-lg-12 mt-5 pt-5 mob-pt-0 mob-mt-3 text-center text-lg-start">
      <h1 class="mob-mt-0 page-title">Get Your <span class="text-primary">Vouchers</span></h1>
      <p class="text-large">Gift them a thrilling experience with one of our cash vouchers!</p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <div class="row">
    <div class="container pt-4">
        
    </div>
  </div>
</div>
<div class="container container-wide pt-3 position-relative z-2">
{{-- 
	<div class="row py-5 mob-pt-0">
		<div class="col-lg-10 text-center text-lg-start">
  		<p class="mimic-h1 mb-2"><span class="text-primary">Combo</span> Vouchers</p>
  		<p class=" mb-4">Check out our multi-activity combo passes! Perfect for the ultimate day out and experience all of our fantastic activities!</p>
  	</div>
  	<div class="col-12 px-4 d-lg-none">
  		<carousel :items="1" :margin="20" :center="false"  :autoHeight="false" :autoWidth="false" :nav="false" :dots="true" :loop="true" :autoplay="true">
  			<div style="padding-bottom: 50px;">
  				<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/combo-ultimate.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Ultimate')"><span class="text-primary">Ultimate</span> Combo Pass</p>
			      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Ultimate')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '687063'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>6+</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
  			</div>
  			<div style="padding-bottom: 50px;">
  				<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/combo-ti.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Titanic')">Combo Pass <span class="text-primary">Titanic</span></p>
			      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Titanic')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685951'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>6+</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
  			</div>
  			<div style="padding-bottom: 50px;">
  				<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/combo-nb.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Newtownbreda')">Combo Pass <span class="text-primary">Newtownbreda</span></p>
			      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Newtownbreda')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685957'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>1+</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
  			</div>
  			<div style="padding-bottom: 50px;">
  				<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/tots.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/tots.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/tots.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Tots')">x5 <span class="text-primary">Tots</span> Bounce Passes </p>
			      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Tots')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '687073'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>1 - 5</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
  			</div>
  		</carousel>
  	</div>x
  	<div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/combo-ultimate.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Ultimate')"><span class="text-primary">Ultimate</span> Combo Pass</p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Ultimate')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '687063'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>6+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/combo-ti.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Titanic')">Combo Pass <span class="text-primary">Titanic</span></p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Titanic')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685951'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>6+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/combo-nb.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Newtownbreda')">Combo Pass <span class="text-primary">Newtownbreda</span></p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Newtownbreda')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685957'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>1+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/tots.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/tots.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/tots.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Tots')">x5 <span class="text-primary">Tots</span> Bounce Passes </p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Tots')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '687073'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>1 - 5</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div> --}}
  {{-- <div class="row pb-5 mob-py-0">
  	<div class="col-lg-10 text-center text-lg-start">
  		<p class="mimic-h1 mb-2">Activity <span class="text-primary">Vouchers</span></p>
  		<p class="mb-4">Scale our epic Ninja Master Course, soar with an Indoor Skydiving Experience or bounce in one of our massive Inflatable Parks! </p>
  	</div>
  	<div class="col-12 px-4 d-lg-none">
  		<carousel :items="1" :margin="20" :center="false"  :autoHeight="false" :autoWidth="false" :nav="false" :dots="true" :loop="true" :autoplay="true">
  			<div style="padding-bottom: 50px;">
			  	<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/skydive-silver.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white pl-2" onclick="showChristmasModal('Silver')">Get Skydiving</p>
			      	<p class="subtext text-uppercase text-primary letter-spacing pl-2" onclick="showChristmasModal('Silver')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685959'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>4+</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
		    </div>
		    <div style="padding-bottom: 50px;">
	  			<div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/inflatable.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white shadow cursor-pointer pl-2" onclick="showChristmasModal('Inflatable')">Get Inflatable Park</p>
			      	<p class="subtext text-uppercase text-primary letter-spacing cursor-pointer pl-2" onclick="showChristmasModal('Inflatable')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685941'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      	<div class="price">Ages<br/>4+</div>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
			  </div>
			  <div style="padding-bottom: 50px;">
			    <div class="backdrop">
			      <picture>
			        <source srcset="/img/offers/christmas/2022/ninja.webp" type="image/webp"/> 
			        <source srcset="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg"/> 
			        <img src="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
			      </picture>
			      <div class="christmas-offer-content">
			      	<p class="title title-up-a-bit text-white pl-2" onclick="showChristmasModal('Ninja')">Get Ninja</p>
			      	<div class="price">Ages<br/>6+</div>
			      	<p class="subtext text-uppercase text-primary letter-spacing pl-2" onclick="showChristmasModal('Ninja')">Tell me more <i class="fa fa-angle-double-right"></i></p>
			      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685943'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
			      </div>
			      <div class="backdrop-back"></div>
			    </div>
			   </div>
  		</carousel>
  	</div>
  	<div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/skydive-silver.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Silver')">Get Skydiving</p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Silver')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685959'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>4+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/inflatable.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white shadow cursor-pointer" onclick="showChristmasModal('Inflatable')">Get Inflatable Park</p>
	      	<p class="subtext text-uppercase text-primary letter-spacing cursor-pointer" onclick="showChristmasModal('Inflatable')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685941'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      	<div class="price">Ages<br/>4+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5 d-none d-lg-block">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/ninja.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Ninja')">Get Ninja</p>
	      	<div class="price">Ages<br/>6+</div>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Ninja')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '685943'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div> --}}
  <div class="row mt-5">
		<div class="col-lg-10 text-center text-lg-start">
  		<p class="mimic-h1 mb-2">Cash <span class="text-primary">Vouchers</span></p>
  		<p class="mb-4">Can't decide? Let them choose with our Cash Vouchers! Valid for any activity! </p>
  	</div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2022/cash-2.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2022/cash-2.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2022/cash-2.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white" onclick="showChristmasModal('Cash')"><span class="text-primary">Vertigo</span> Cash Vouchers</p>
	      	<p class="subtext text-uppercase text-primary letter-spacing" onclick="showChristmasModal('Cash')">Tell me more <i class="fa fa-angle-double-right"></i></p>
	      	<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '610712'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div>
</div>
{{-- <div class="container-fluid position-relative mt-5 mt-5">
  <div class="row">
    <div class="container container-wide">
    	<div class="row">
    		<div class="col-lg-8">
    			<p class="mimic-h2 mb-0">FAQ<span class="text-primary">'</span>s</p>
    			<p class="text-small mb-4 text-primary">* Click on a question to see the answer</p>
		      <p class="text-large mb-0"><b><a id="question-1" class="text-white d-block pb-3" data-toggle="collapse" href="#faqid-1" role="button" aria-expanded="false" aria-controls="faqid-1">When is my voucher valid to?</a></b></p>
		      <div class="collapse pb-4" id="faqid-1" >
		      	<p class="mb-0">Vouchers are valid for 12 months, Christmas vouchers 6 months, Birthday vouchers 6 months!</p>
		      </div>
		      <hr class="line-primary mt-0"/>   			
    			<p class="text-large mb-0"><b><a id="question-2" class="text-white d-block pb-3" data-toggle="collapse" href="#faqid-2" role="button" aria-expanded="false" aria-controls="faqid-2">How does 3 for 2 work on my voucher?</a></b></p>
		      <div class="collapse pb-4" id="faqid-2" >
		      </div>
		      <hr class="line-primary mt-0"/>
					<p class="text-large mb-0"><b><a id="question-3" class="text-white d-block pb-3" data-toggle="collapse" href="#faqid-3" role="button" aria-expanded="false" aria-controls="faqid-3">How do I receive my voucher?</a></b></p>
		      <div class="collapse pb-4" id="faqid-3" >
		      	<p class="mb-0">You'll receive your voucher via email!</p>
		      </div>
		      <hr class="line-primary mt-0"/>
    		</div>
    	</div>
      <div class="row">
        <div class="col-12 text-center text-lg-left mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div> --}}
@endsection
@section('modals')
<!-- Modal -->
<div class="modal fade" id="christmasModal" tabindex="-1" role="dialog" aria-labelledby="christmasModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body pt-5">
        
      </div>
      <p class="text-white">
        <span data-dismiss="modal" aria-label="Close" class="cursor-pointer" aria-hidden="true">&times; Close</span>
      </p>
    </div>
  </div>
</div>
@endsection
@section('scripts')
{{-- <script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script> --}}
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
  function showChristmasModal(voucher){
  	$('#christmasModal .modal-body').html('');
  	var title = '';
  	var desc = '';
  	var picturehtml = '';
  	var btn = '';
  	switch(voucher) {
  		
		  case 'Inflatable':

		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">Inflatable Park</p>';
	  		desc = '<p class="px-3">Treat your loved ones this Christmas to a day full of fun activities in one of the biggest inflatable parks in the world! Perfect for all the family!</p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/inflatable.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/inflatable.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'685941\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>4+</div>';

		    break;

		  case 'Ninja':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">Ninja</p>';
	  		desc = '<p class="px-3">The perfect present for all the karate kids out there! Race, climb, balance & swing through our epic ninja master courses!</p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/ninja.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/ninja.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'685943\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>6+</div>';
		    
		    break;

		 	case 'Silver':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">Skydiving</p>';
	  		desc = '<p class="px-3">Give the gift of flight this Christmas! The perfect present for those adrenaline junkies & superhero wannabes! We&#39;ve got packages perfect for those beginners right up to professionals! Our expert instructors will guide you through each step to ensure their experience is unforgettable!</p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/skydive-silver.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/skydive-silver.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'685959\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>4+</div>';
		    
		    break;

		  case 'Titanic':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">Combo Pass <span class="text-primary">Titanic</span></p>';
	  		desc = '<p class="px-3">Treat them this Christmas to 2 hours filled with fun activities! Our combo passes gives you 1 hour in our huge inflatable park taking on its challenges + 1 hour racing through our epic ninja master courses! The perfect present for those adventure seekers!</p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/combo-ti.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/combo-ti.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'685951\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>6+</div>';
		    
		    break;

		  case 'Newtownbreda':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">Combo Pass <span class="text-primary">Newtownbreda</span></p>';
	  		desc = '<p class="px-3">The perfect gift for the explorers of the family. Bounce, climb & slide your way through the biggest indoor inflatable park in the world + 2 hours in our Adventure centre equipped with 30ft climbing wall, high & low ropes courses and soft play village! </p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/combo-nb.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/combo-nb.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'685957\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>1+</div>';

		    break;

		  case 'Ultimate':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;"><span class="text-primary">Ultimate</span> Combo Pass</p>';
	  		desc = '<p class="px-3">The perfect present for those adrenaline junkies! Treat them to an unforgettable day with an inflatable park session, ninja master course session & indoor skydiving experience all in one day! </p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/combo-ultimate.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/combo-ultimate.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'687063\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>6+</div>';

		    break;

		  case 'Tots':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;">x5 <span class="text-primary">Tots</span> Bounce Passes</p>';
	  		desc = '<p class="px-3">The perfect present for those cheeky monkeys! Our tots&#39; sessions allow your little ones full access to our Newtownbreda inflatable park & adventure centre. The perfect gift for those first-time parents! </p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/tots.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/tots.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/tots.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'687073\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = '<div class="price">Ages<br/>1 - 5</div>';
		    
		    break;

		  case 'Cash':
		    
		  	title = '<p class="mimic-h3 mb-5 text-white shadow position-relative z-2" style="margin-top: -50px;"><span class="text-primary">Vertigo</span> Cash Vouchers</p>';
	  		desc = '<p class="px-3">Can&#39;t decide? Don&#39;t worry you can play it safe with our cash vouchers! Choose any amount which can be redeemed on any of our epic activities!</p>';
	  		picturehtml = '<div class="backdrop"><picture><source srcset="/img/offers/christmas/2022/cash-2.webp" type="image/webp"/><source srcset="/img/offers/christmas/2022/cash-2.jpg" type="image/jpeg"/><img src="/img/offers/christmas/2022/cash-2.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" /></picture><div class="backdrop-back"></div></div>';
		    btn = '<button type="button" class="btn btn-primary btn-icon shadow" onclick="RollerCheckout.showProduct({productId: \'610712\'})">Buy Now <i class="custom-icon chevron-double-right"></i></button>';
		    ages = null;

		    break;

		  default:
		    // code block
		}
		$('#christmasModal .modal-body').append(picturehtml);
    $('#christmasModal .modal-body').append(ages);
    $('#christmasModal .modal-body').append(title);
    $('#christmasModal .modal-body').append(desc);
    $('#christmasModal .modal-body').append(btn);
  	$('#christmasModal').modal('show');
  }
</script>
<style>
	#christmasModal .price{
		position: absolute;
		z-index: 2;
		top: 4rem;
		right: 3rem;
		width: 70px;
		height: 70px;
		border-radius: 100%;
		background-color: #F6CF3C;
    color: #000;
   	font-weight: 300;
    font-family: Tungsten, sans-serif;
    text-transform: uppercase;
    padding: 9px 10px;
    text-align: center;
    font-size: 30px;
    line-height: 28px;
	}
	.christmas-offer-content .title.title-up-a-bit{
		    bottom: 3.5rem;
	}
	.owl-stage-outer{
		overflow: visible !important;
	}
	@media only screen and (max-width : 767px){
		.christmas-offer-content button{
			left: calc(50% - 110px);
		}
		.christmas-offer-content .title.title-up-a-bit{
			bottom: 3rem;
		}
		.christmas-offer-content .subtext{
			bottom:  1.5rem;
		}
		#christmasModal .mimic-h3{
			margin-top:  -10vw !important;
		}
	}
</style>
@endsection