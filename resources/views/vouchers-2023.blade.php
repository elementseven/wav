@php
$page = 'Gift Vouchers';
$pagetitle = 'Gift Vouchers | We Are Vertigo';
$metadescription = "Take advantage of all of our awesome activities at reduced prices with our memberhsip packages! Ranging from off-peak & single activity memberships to any time, all activity memberships, theres something for everyone!";
$pagetype = 'white';
$pagename = 'gift-vouchers snow-bg bg-repeat';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('styles')
<script id="roller-checkout" src="https://cdn.rollerdigital.com/scripts/widget/checkout_iframe.js" data-checkout="https://ecom.roller.app/wearevertigo/christmascashvouchers/en/products"></script>
@endsection
@section('header')
<header class="container container-wide position-relative pt-5 z-2">
  <div class="row pt-5 mob-mt-3">
    <div class="col-lg-10 mt-5 mob-mt-3 text-center text-lg-left">
      <h1 class="mob-mt-0 page-title pt-5 mob-pt-0">Treat Them This <span class="text-red">Christmas</span></h1>
      <p class="text-large">Give the gift of memories this Christmas, with an unforgettable experience shared with friends and family! With our thrilling Christmas vouchers, you gift the idea and they choose the memory with Indoor Skydiving, The World's Largest Inflatable Park and Epic Adventure Climbing and Soft Play Centre!</p>
      <p class="text-large"><b>All Christmas Cash Vouchers are valid for redemption from 26th December 2023 to 30th June 2024.</b></p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container container-wide pt-3 position-relative z-2">
	<div class="row py-5 mob-pt-0 justify-content-center">
  	<div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2023/65.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2023/65.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2023/65.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white"><span class="text-primary">£65</span>  CASH VOUCHER<br/>FOR ONLY £50</p>
	      	<button type="button" class="btn btn-red btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '794279'})">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
	      	<div class="price">USE CODE<br/>XMAS50</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2023/100.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2023/100.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2023/100.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white"><span class="text-primary">£100</span>  CASH VOUCHER<br/>FOR ONLY £75</p>
	      	<button type="button" class="btn btn-red btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '794282'})">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
	      	<div class="price">USE CODE<br/>XMAS75</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2023/150.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2023/150.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2023/150.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white"><span class="text-primary">£150</span>  CASH VOUCHER<br/>FOR ONLY £100</p>
	      	<button type="button" class="btn btn-red btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '794284'})">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
	      	<div class="price">USE CODE<br/>XMAS100</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2023/skydive.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2023/skydive.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2023/skydive.jpg" type="image/jpeg" alt="skydive vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white"><span class="text-primary">BIG SAVINGS on</span>  <br/>INDOOR SKYDIVING VOUCHERS</p>
	      	<button type="button" class="btn btn-red btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '795408'})">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
	      	<div class="price">SAVE<br/>20%+</div>
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
    <div class="col-lg-4 mob-px-3 mb-4 pb-5">
    	<div class="backdrop">
	      <picture>
	        <source srcset="/img/offers/christmas/2023/cash.webp" type="image/webp"/> 
	        <source srcset="/img/offers/christmas/2023/cash.jpg" type="image/jpeg"/> 
	        <img src="/img/offers/christmas/2023/cash.jpg" type="image/jpeg" alt="vouchers this Christmas at We are Vertigo Belfast" class="lazy w-100 backdrop-content shadow" />
	      </picture>
	      <div class="christmas-offer-content">
	      	<p class="title title-up-a-bit text-white"><span class="text-primary">Custom Value</span><br/>CASH VOUCHER</p>
	      	<button type="button" class="btn btn-red btn-icon shadow" onclick="RollerCheckout.showProduct({productId: '795431'})">Buy Now <i class="custom-icon chevron-double-right-white"></i></button>
	      	{{-- <div class="price">USE CODE<br/>XMAS100</div> --}}
	      </div>
	      <div class="backdrop-back"></div>
	    </div>
    </div>
  </div>
</div>
@endsection
@section('modals')
<!-- Modal -->

@endsection
@section('scripts')
{{-- <script src="//cdn.rollerdigital.com/scripts/widget/responsive_iframe.js"></script> --}}
<script>
  $(document).ready(function (){
    $(".booknowbtn").click(function (){
      $('html, body').animate({
        scrollTop: $("#bookonline").offset().top -100
      }, 500);
    });
  });
</script>
<style>
	.backdrop .price{
		position: absolute;
		z-index: 2;
		top: 1rem;
		left: 1rem;
		width: 100px;
		height: 100px;
		border-radius: 100%;
		background-color: #F6CF3C;
    color: #000;
   	font-weight: 300;
    font-family: Tungsten, sans-serif;
    text-transform: uppercase;
    padding: 1.5rem 10px;
    text-align: center;
    font-size: 30px;
    line-height: 28px;
	}
	.owl-stage-outer{
		overflow: visible !important;
	}
	.christmas-offer-content .title{
		font-size: 46px;
	}
	@media only screen and (max-width : 767px){
		.christmas-offer-content button{
			left: calc(50% - 110px);
		}
		.christmas-offer-content .title.title-up-a-bit{
			bottom: 1.5rem;
		}
		.christmas-offer-content .subtext{
			bottom:  1.5rem;
		}
		#christmasModal .mimic-h3{
			margin-top:  -10vw !important;
		}
	}
</style>
@endsection