@php
$page = 'Accommodation';
$pagetitle = 'Accommodation | We Are Vertigo';
$metadescription = 'We have partnered up with a range of local hotels to offer our customers exclusive discounts on "stay-cations". Book using the links below to make the most of these amazing discounts!';
$pagetype = 'white';
$pagename = 'accommodation';
$ogimage = 'https://www.wearevertigo.com/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container position-relative pt-5 z-2">
  <div class="row pt-5 mob-pt-3">
    <div class="col-lg-12 mt-5 text-center text-lg-start">
      <p class="box-title-top text-primary text-uppercase">Awesome</p>
      <h1 class="mob-mt-0 page-title ipadp-smaller-h1">Accommodation</h1>
      <p>We have partnered up with a range of local hotels to offer our customers exclusive discounts on "stay-cations". Book using the links below to make the most of these amazing discounts! </p>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container-fluid position-relative z-1">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container pt-4">
        
    </div>
  </div>
</div>
<div class="container-fluid pt-3">
  <div class="row pb-5 mob-py-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/accommodation/crowneplaza.webp" type="image/webp"/> 
                <source srcset="/img/accommodation/crowneplaza.jpg" type="image/jpeg"/> 
                <img src="/img/accommodation/crowneplaza.jpg" type="image/jpeg" alt="The Crowne Plaza hotel near We are Vertigo Titanic Quarter" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="mimic-h1">The Crowne Plaza</p>
                  <p class="text-small">Crowne Plaza Belfast is a 4-star hotel situated in the surrounding Lagan Valley Regional Park, only a 7-minute drive to We are Vertigo. The hotel offers free car parking and complimentary Wi-Fi throughout. Guests have access to our newly refurbished onsite health club, including an impressive gym, 20 metre swimming pool, steam room, sauna and Jacuzzi. All of our luxurious bedrooms include a mini fridge, flat screen TV, safe and ironing board for your convenience.</p>
                  <p class="text-small">The hotel boasts a variety of dining options including The Green Room Restaurant with locally sourced produce, the Spice Club offering classic Indian dishes and The River Bar and Lounge for family-friendly dining. </p>
                  <a href="https://bit.ly/2Imi9aO" target="_blank">
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 px-0">
          <div class="activity-preview py-5 mob-mb-0 mob-pt-0 odd">
            <div class="activity-image">
              <picture>
                <source srcset="/img/accommodation/holidayinn.webp" type="image/webp"/> 
                <source srcset="/img/accommodation/holidayinn.jpg" type="image/jpeg"/> 
                <img src="/img/accommodation/holidayinn.jpg" type="image/jpeg" alt="The Holiday Inn newar We are Vertigo Belfast" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-right">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="mimic-h1 w-100">Holiday Inn</p>
                   <p class="text-small">The 4-star Holiday Inn has the perfect location for a stay in Belfast City Centre, includes free Wi-Fi throughout the hotel and secure discounted parking nearby. Family holidays are made even more affordable as kids under 17 stay free and kids 13 and under eat for free in our onsite restaurant.  The hotel features an all-day dining menu, Starbucks coffee, a wide range of drinks and take-away snacks available 24 hours a day.</p>
                   <p class="text-small">There is also an onsite fitness centre where guests can make use of cardio equipment and free weights. Our newly refurbished open lobby provides the perfect setting to enjoy our board games or watch a live sports match.  </p>
                  <a href="https://bit.ly/2vXdy9B" target="_blank">
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5 mob-mt-0">
        <div class="col-12 px-0">
          <div class="activity-preview mb-5 py-5 mob-mb-0 mob-pt-0 even">
            <div class="activity-image">
              <picture>
                <source srcset="/img/accommodation/holidayinnexpress.webp" type="image/webp"/> 
                <source srcset="/img/accommodation/holidayinnexpress.jpg" type="image/jpeg"/> 
                <img src="/img/accommodation/holidayinnexpress.jpg" type="image/jpeg" alt="The Holiday Inn Express hotel near We are Vertigo Belfast" class="lazy" />
              </picture>
            </div>
            <div class="preview-box card bg-primary p-5 mob-px-3 mob-pb-4 text-dark text-center text-lg-left" data-aos="fade-left">
              <div class="d-table w-100 h-100">
                <div class="d-table-cell align-middle w-100 h-100">
                  <p class="mimic-h1">Holiday Inn Express</p>
                  <p class="text-small">Located in Belfast’s Queens Quarter this modern 3-star hotel is only a short stroll away from many cafés and restaurants. Guests can make use of our free onsite car park and free Wi-Fi throughout the hotel.</p>
                  <p class="text-small">Complimentary breakfast buffet Is included with every booking and has a wide variety of hot and continental options to suit everyone. Food is served daily from 12pm and families can relax and enjoy their meal in the knowledge that children under 12 eat free from the kids’ options, when accompanied by an adult. Our spacious rooms include flat screen TV, work area, safe, coffee/tea making facilities and hairdryers.</p>
                  <a href="https://bit.ly/2tz8OFE" target="_blank">
                    <button type="button" class="btn btn-black btn-icon d-inline-block">Book Now <i class="custom-icon chevron-double-right-white"></i></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid position-relative">
  <img src="/img/graphics/chevrons-right.svg" class="home-chevrons-right-1" alt="We are Vertigo chevrons right" data-aos="fade-down-left"/>
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center mt-5">
          <p class="mimic-h2"><span class="mr-4 mob-mx-0 d-inline-block d-md-inline">Have a question for us?</span> <a href="{{route('contact')}}"><button type="button" class="btn btn-primary btn-icon mob-mt-2 ipadp-mt-3">Get in touch <i class="custom-icon chevron-double-right"></i></button></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection