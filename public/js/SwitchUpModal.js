(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["SwitchUpModal"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************************/
/***/function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsModalsSwitchUpModalVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
mounted:function mounted(){
$('#switchupModal').modal('show');
$('.close-modal').click(function(){
$('#switchupModal').modal('hide');
});
}};


/***/},

/***/"./resources/js/components/Modals/SwitchUpModal.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/Modals/SwitchUpModal.vue ***!
  \**********************************************************/
/***/function resourcesJsComponentsModalsSwitchUpModalVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./SwitchUpModal.vue?vue&type=template&id=50ae1660& */"./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660&");
/* harmony import */var _SwitchUpModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./SwitchUpModal.vue?vue&type=script&lang=js& */"./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
_SwitchUpModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
_SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__.render,
_SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Modals/SwitchUpModal.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/***/function resourcesJsComponentsModalsSwitchUpModalVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwitchUpModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SwitchUpModal.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SwitchUpModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default;

/***/},

/***/"./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660& ***!
  \*****************************************************************************************/
/***/function resourcesJsComponentsModalsSwitchUpModalVueVueTypeTemplateId50ae1660(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SwitchUpModal_vue_vue_type_template_id_50ae1660___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SwitchUpModal.vue?vue&type=template&id=50ae1660& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660&":
/*!********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Modals/SwitchUpModal.vue?vue&type=template&id=50ae1660& ***!
  \********************************************************************************************************************************************************************************************************************************/
/***/function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsModalsSwitchUpModalVueVueTypeTemplateId50ae1660(__unused_webpack_module,__webpack_exports__,__webpack_require__){
__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c(
"div",
{
staticClass:"modal fade mx-auto",
attrs:{
id:"switchupModal",
tabindex:"-1",
role:"dialog",
"aria-labelledby":"switchupModalTitle",
"aria-hidden":"true"}},


[
_c(
"div",
{
staticClass:"modal-dialog modal-lg modal-dialog-centered",
attrs:{role:"document"}},

[
_c("div",{staticClass:"modal-content"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/modals/cargo-switch-up.webp?v2.0",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/modals/cargo-switch-up.jpg?v2.0",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy h-auto d-none d-lg-block w-100",
attrs:{
src:"/img/cargo/modals/cargo-switch-up.jpg?v2.0",
type:"image/png",
alt:"We're switching it up - Cargo by Vertigo Belfast",
width:"300"}})]),



_vm._v(" "),
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/cargo/modals/cargo-switch-up-mob.webp?v2.0",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/cargo/modals/cargo-switch-up-mob.jpg?v2.0",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy h-auto d-lg-none w-100",
attrs:{
src:"/img/cargo/modals/cargo-switch-up-mob.jpg?v2.0",
type:"image/png",
alt:"We're switching it up - Cargo by Vertigo Belfast",
width:"300"}})]),



_vm._v(" "),
_c(
"div",
{
staticClass:
"btn-text cursor-pointer text-center d-block w-100 mb-5 mt-4 position-relative z-200",
attrs:{"data-dismiss":"modal"}},

[_vm._v("[X] Close")]),

_vm._v(" "),
_c(
"div",
{
staticClass:
"btn-text cursor-pointer text-center z-200 close-modal",
staticStyle:{
position:"absolute",
top:"1rem",
left:"1rem",
"z-index":"200",
"font-size":"25px"},

attrs:{"data-dismiss":"modal"}},

[_c("i",{staticClass:"fa fa-times"})])])])]);






}];

_render._withStripped=true;



/***/}}]);

}());
