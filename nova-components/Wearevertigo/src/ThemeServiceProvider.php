<?php

namespace Elementseven\Wearevertigo;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Nova;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::booted(function () {
            Nova::theme(asset('/elementseven/wearevertigo/theme.css'));
        });

        $this->publishes([
            __DIR__.'/../resources/css' => public_path('elementseven/wearevertigo'),
        ], 'public');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
