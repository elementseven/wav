<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\Activity' => 'App\Policies\ActivityPolicy',
        'App\Models\ActivitySlide' => 'App\Policies\ActivitySlidePolicy',
        'App\Models\Application' => 'App\Policies\ApplicationPolicy',
        'App\Models\Category' => 'App\Policies\CategoryPolicy',
        'App\Models\Faq' => 'App\Policies\FaqPolicy',
        'App\Models\Location' => 'App\Policies\LocationPolicy',
        'App\Models\Offer' => 'App\Policies\OfferPolicy',
        'App\Models\Post' => 'App\Policies\PostPolicy',
        'App\Models\Role' => 'App\Policies\RolePolicy',
        'App\Models\User' => 'App\Policies\UserPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
