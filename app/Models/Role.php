<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','slug'
    ];

    protected static function boot()
		{
			parent::boot();
			static::saving(function ($role) {
				$role->slug = Str::slug($role->title, "-");
			});
		}

    public function users(){
        return $this->hasMany('App\Models\User');
    }
}
