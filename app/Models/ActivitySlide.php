<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class ActivitySlide extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'photo',
    'video',
    'order'
  ];


  public function registerMediaConversions(Media $media = null): void
	{
		$this->addMediaConversion('normal')->width(1170);
		$this->addMediaConversion('normal-webp')->width(1170)->format('webp');
		$this->addMediaConversion('double')->width(2340);
		$this->addMediaConversion('double-webp')->width(2340)->format('webp');
		$this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
		$this->addMediaConversion('featured')->width(400)->crop('crop-center', 400, 280);
		$this->addMediaConversion('featured-webp')->width(400)->crop('crop-center', 400, 280)->format('webp');
	}
	
	public function registerMediaCollections(): void
	{
		$this->addMediaCollection('activityslides')->singleFile();
	}

	public function activity(){
		return $this->belongsTo('App\Models\Activity');
	}

}
