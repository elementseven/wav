<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Event extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title',
      'subtitle',
      'date',
      'description',
      'resdiary',
      'glistrr',
      'status',
      'price',
      'image'
  ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($event) {
            $event->slug = Str::slug($event->title, "-");
        });
    }
    
    protected $casts = [
        'created_at' => 'datetime',
    ];
    
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumbnail')->crop('crop-center', 400, 400);
        $this->addMediaConversion('featured')->width(400)->crop('crop-center', 400, 400);
        $this->addMediaConversion('featured-webp')->width(400)->crop('crop-center', 400, 400)->format('webp');
    }
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('events')->singleFile();
    }

}