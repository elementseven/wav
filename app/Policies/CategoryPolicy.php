<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create an category.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Category  $category
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Category $upcategory
     * @return mixed
     */
    public function update(User $user, Category $upcategory)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Category $viewcategory
     * @return mixed
     */
    public function view(User $user, Category $viewcategory)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Category $delcategory
     * @return mixed
     */
    public function delete(User $user, Category $delcategory)
    {
        return $user->role->name == 'Super Admin';
    }
}
