<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Location;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create a location.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Location $location
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can update the location.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Location $location
     * @return mixed
     */
    public function update(User $user, Location $location)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the location.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Location $location
     * @return mixed
     */
    public function view(User $user, Location $location)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the location.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Location $location
     * @return mixed
     */
    public function delete(User $user, Location $location)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view locations.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Location  $location
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->name == 'Super Admin';
    }
}
