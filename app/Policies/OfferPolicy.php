<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Offer;
use Illuminate\Auth\Access\HandlesAuthorization;

class OfferPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create an offer.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Offer  $offer
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can update the offer.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Offer  $upoffer
     * @return mixed
     */
    public function update(User $user, Offer $upoffer)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the offer.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Offer  $viewoffer
     * @return mixed
     */
    public function view(User $user, Offer $viewoffer)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the offer.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Offer  $deloffer
     * @return mixed
     */
    public function delete(User $user, Offer $deloffer)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view offers.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Offer $offer
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->name == 'Super Admin';
    }
}
