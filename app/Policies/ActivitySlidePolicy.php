<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ActivitySlide;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActivitySlidePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create a post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return mixed
     */
    public function update(User $user, ActivitySlide $activityslide)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return mixed
     */
    public function view(User $user, ActivitySlide $activityslide)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Post  $post
     * @return mixed
     */
    public function delete(User $user, ActivitySlide $activityslide)
    {
        return $user->role->name == 'Super Admin';
    }

    /**
     * Determine whether the user can view activity slides.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\ActivitySlide  $activityslide
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role->name == 'Super Admin';
    }
}
