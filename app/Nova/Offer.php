<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Number;
use Nevadskiy\Quill\Quill;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Offer extends Resource
{

    public static function authorizable()
    {
        return true;
    }
    
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Offer::class;
    public static $with = ['media'];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    public static function label() {
        return 'Offers & Discounts';
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Date::make('Expires', 'expires')->nullable(),
            Date::make('Created', 'created_at')->onlyOnIndex(),
            Number::make('Display Order', 'display_order'),
            Text::make('Excerpt')->sortable()->onlyOnForms(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('offers');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('offers', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('offers', 'thumbnail');
            })->deletable(false),
            Select::make('Status')->options($statuses)->sortable(),
            Quill::make('Content')->withFiles('public')
            ->toolbar([
                [[ 'header' => 2 ],[ 'header' => 3 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->help('Main content'),
            Textarea::make('Meta Description','meta_description')->onlyOnForms(),
            Textarea::make('Keywords','keywords')->onlyOnForms(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
