<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\HasMany;
use Nevadskiy\Quill\Quill;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Activity extends Resource
{   
    public static function authorizable()
    {
        return true;
    }

    public function authorizedToDelete(Request $request)
    {
        return false;
    }
    
    public static $perPageViaRelationship = 6;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Activity::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        return [
            Text::make('Title')->sortable(),
            Select::make('Status')->options($statuses)->sortable(),
            Text::make('Summary')->hideFromIndex(),
            Image::make('Photo')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('photo')->toMediaCollection('activities');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('activities', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('activities', 'thumbnail');
            })->deletable(false)->hideFromIndex(),
            Text::make('Ages')->hideFromIndex(),
            Text::make('First Page Title','title_1')->hideFromIndex(),
            Quill::make('First Description', 'description_1')->withFiles('public')
            ->toolbar([
                [[ 'header' => 2 ],[ 'header' => 3 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->help('Main content'),
            Text::make('Second Page Title','title_2')->hideFromIndex(),
            Quill::make('Second Description', 'description_2')->withFiles('public')
            ->toolbar([
                [[ 'header' => 2 ],[ 'header' => 3 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->help('Main content 2'),
            Textarea::make('Roller')->sortable()->hideFromIndex(),
            Textarea::make('Meta Description','meta_description')->onlyOnForms()->hideFromIndex(),
            Textarea::make('Keywords','keywords')->onlyOnForms()->hideFromIndex(),
            BelongsTo::make('Location'),
            HasMany::make('ActivitySlides', 'activityslides'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
