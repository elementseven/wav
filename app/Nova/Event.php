<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Select;
use Nevadskiy\Quill\Quill;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Event extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Event::class;
    public static $with = ['media'];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $statuses = array('Published'=>'Published','Draft'=>'Draft');
        $bookingoptions = array(
            '/cargo/book-now/friday-live-music'=> 'Friday Live Music',
            '/cargo/book-now/saturday-bottomless-brunch'=> 'Saturday Bottomless Brunch',
            '/cargo/book-now/saturday-live-music'=>'Saturday Live Music',
            '/cargo/book-now/sunday-bottomless-brunch'=> 'Sunday Bottomless Brunch',
            'custom'=> 'One Off Event *Resdiary code needed below'
        );
        return [
            Text::make('Title')->sortable(),
            Text::make('Subtitle')->sortable(),
            Date::make('Sort Date', 'created_at')->sortable(),
            Text::make('Display Date & Time', 'date')->onlyOnForms(),
            Text::make('Price')->sortable(),
            Select::make('Booking Page', 'link')->options($bookingoptions)->onlyOnForms()->help(
                'Select the event name, if this is a one off event enter the resdiary code below & make sure to write a description for the landing page'
            ),
            Number::make('Resdiary')->sortable()->onlyOnForms()->help(
                'Enter the resdiary code, eg. 27298'
            ),
            Number::make('Glistrr')->sortable()->onlyOnForms()->help(
                'Enter the Glistrr ID , eg. 6'
            ),
            Quill::make('Description', 'description')->withFiles('public')
            ->toolbar([
                [[ 'header' => 2 ],[ 'header' => 3 ]],               
                ['bold', 'italic', 'underline'],
                [['list' => 'ordered'], ['list' => 'bullet']],
                ['blockquote'],
                ['link', 'image', 'video'],
                ['clean'],
            ])->help('Main content'),
            Image::make('Image')->store(function (Request $request, $model) {
                    $model->addMediaFromRequest('image')->toMediaCollection('events');
                })
                ->preview(function () {
                    return $this->getFirstMediaUrl('events', 'thumbnail');
                })
                ->thumbnail(function () {
                    return $this->getFirstMediaUrl('events', 'thumbnail');
            })->deletable(false),
            Select::make('Status')->options($statuses)->sortable(),
            
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
