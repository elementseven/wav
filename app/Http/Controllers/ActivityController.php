<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::has('activities')->orderBy('title','asc')->get();
        return view('activities.index')->with(['locations' => $locations]);
    }


    /**
     * Return a json of activities.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){
        if($request->input('location') == '*'){
          $activities = Activity::where('status','!=','draft')
          ->orderBy('created_at','asc')
          ->with('location')
          ->paginate($request->input('limit'));
        }else{
          $activities = Activity::whereHas('location', function($q) use($request){
              $q->where('id', $request->input('location'));
          })
          ->where('status','!=','draft')
          ->orderBy('created_at','asc')
          ->with('location')
          ->paginate($request->input('limit'));
        }
        foreach($activities as $p){
          $p->featured = $p->getFirstMediaUrl('activities', 'featured');
          $p->featuredwebp = $p->getFirstMediaUrl('activities', 'featured-webp');
          $p->double = $p->getFirstMediaUrl('activities', 'double');
          $p->doublewebp = $p->getFirstMediaUrl('activities', 'double-webp');
          $p->normal = $p->getFirstMediaUrl('activities', 'normal');
          $p->normalwebp = $p->getFirstMediaUrl('activities', 'normal-webp');
          $p->mimetype = $p->getFirstMedia('activities')->mime_type;
        }
        return $activities;
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show($location, $slug)
    {
        $activity = Activity::where([['slug',$slug],['status','!=','draft']])->whereHas('location', function($q) use($location){
            $q->where('slug', $location);
        })->with('location', 'activityslides')->first();

        $activity->normal = $activity->getFirstMediaUrl('activities', 'normal');
        $activity->normalwebp = $activity->getFirstMediaUrl('activities', 'normal-webp');
        $activity->double = $activity->getFirstMediaUrl('activities', 'double');
        $activity->doublewebp = $activity->getFirstMediaUrl('activities', 'double-webp');
        $activity->featured = $activity->getFirstMediaUrl('activities', 'featured');
        $activity->featuredwebp = $activity->getFirstMediaUrl('activities', 'featured-webp');
        $activity->mimetype = $activity->getFirstMedia('activities')->mime_type;
        
        foreach($activity->activityslides as $as){
            if(!$as->video){
                $as->double = $as->getFirstMediaUrl('activityslides', 'double');
                $as->doublewebp = $as->getFirstMediaUrl('activityslides', 'double-webp');
                $as->normal = $as->getFirstMediaUrl('activityslides', 'normal');
                $as->normalwebp = $as->getFirstMediaUrl('activityslides', 'normal-webp');
                $as->featured = $as->getFirstMediaUrl('activityslides', 'featured');
                $as->featuredwebp = $as->getFirstMediaUrl('activityslides', 'featured-webp');
                $as->mimetype = $as->getFirstMedia('activityslides')->mime_type;
            }
        }

        return view('activities.show')->with(['activity' => $activity]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }
}
