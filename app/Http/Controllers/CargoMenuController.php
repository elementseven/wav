<?php

namespace App\Http\Controllers;

use App\Models\CargoMenu;
use Illuminate\Http\Request;

class CargoMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CargoMenu  $cargoMenu
     * @return \Illuminate\Http\Response
     */
    public function show(CargoMenu $cargoMenu)
    {
        $menu = CargoMenu::orderBy('created_at','desc')->first();
        $url = $menu->getFirstMediaUrl('menus');
        return redirect()->to($url);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CargoMenu  $cargoMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(CargoMenu $cargoMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CargoMenu  $cargoMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CargoMenu $cargoMenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CargoMenu  $cargoMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(CargoMenu $cargoMenu)
    {
        //
    }
}
