<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Event;
use Carbon\Carbon;

class PageController extends Controller
{

  public function monitoring(){
    return view('monitoring');
  }
  public function christmasGiftVouchers(){
    return view('vouchers-2023');
  }
  public function welcome(){
    return view('welcome');
  }
  public function birthdaysSplit(){
    return view('birthdays');
  }
  public function sensorySessions(){
    return view('sensorySessions');
  }
  public function cargo(){
    return view('cargo.cargo');
  }
  public function customEvent(){
    $event = Event::where('id',32)->first();
    return view('cargo.hideout')->with(['event' => $event]);
  }
  public function cargoEvents(){
    return view('cargo.cargo-events');
  }
  public function cargoLandingPage(){
    return view('cargo.landingpage');
  }
  public function sundayBottomlessBrunch(){
    return view('cargo.sundayBottomlessBrunch');
  }
  public function saturdayBottomlessBrunch(){
    return view('cargo.saturdayBottomlessBrunch');
  }
  public function saturdayLiveMusic(){
    return view('cargo.saturdayLiveMusic');
  }
  public function fridayLiveMusic(){
    return view('cargo.fridayLiveMusic');
  }
  public function oneOffEvent(Event $event, $title){
    $event->featured = $event->getFirstMediaUrl('events', 'featured');
    $event->featuredwebp = $event->getFirstMediaUrl('events', 'featured-webp');
    $event->mimetype = $event->getFirstMedia('events')->mime_type;
    return view('cargo.oneOffEvent')->with(['event' => $event]);
  }
  public function oneOffEventGlistrr(Event $event, $title){
    $event->featured = $event->getFirstMediaUrl('events', 'featured');
    $event->featuredwebp = $event->getFirstMediaUrl('events', 'featured-webp');
    $event->mimetype = $event->getFirstMedia('events')->mime_type;
    return view('cargo.cargo-glistrr')->with(['event' => $event]);
  }
  public function getEvents(){
    $events = Event::where([['status','!=','draft'],['created_at', '>=', Carbon::today()]])
    ->orderBy('created_at','asc')
    ->limit(8)
    ->get();
    foreach($events as $p){
      $p->featured = $p->getFirstMediaUrl('events', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('events', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('events')->mime_type;
    }
    return $events;
  }
  public function getAllEvents($limit){
    $events = Event::where([['status','!=','draft'],['created_at', '>=', Carbon::today()]])
    ->orderBy('created_at','asc')
    ->paginate($limit);
    foreach($events as $p){
      $p->featured = $p->getFirstMediaUrl('events', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('events', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('events')->mime_type;
    }
    return $events;
  }
  public function contact(){
    return view('contact');
  }
  public function activities(){
    return view('activities');
  }
  public function inflatablePark(){
    $activity = Activity::where('id', 4)->first();
    $activity->normal = $activity->getFirstMediaUrl('activities', 'normal');
    $activity->normalwebp = $activity->getFirstMediaUrl('activities', 'normal-webp');
    $activity->double = $activity->getFirstMediaUrl('activities', 'double');
    $activity->doublewebp = $activity->getFirstMediaUrl('activities', 'double-webp');
    $activity->featured = $activity->getFirstMediaUrl('activities', 'featured');
    $activity->featuredwebp = $activity->getFirstMediaUrl('activities', 'featured-webp');
    $activity->mimetype = $activity->getFirstMedia('activities')->mime_type;
    
    foreach($activity->activityslides as $as){
        if(!$as->video){
            $as->double = $as->getFirstMediaUrl('activityslides', 'double');
            $as->doublewebp = $as->getFirstMediaUrl('activityslides', 'double-webp');
            $as->normal = $as->getFirstMediaUrl('activityslides', 'normal');
            $as->normalwebp = $as->getFirstMediaUrl('activityslides', 'normal-webp');
            $as->featured = $as->getFirstMediaUrl('activityslides', 'featured');
            $as->featuredwebp = $as->getFirstMediaUrl('activityslides', 'featured-webp');
            $as->mimetype = $as->getFirstMedia('activityslides')->mime_type;
        }
    }
    return view('activities.inflatablePark')->with(['activity' => $activity]);
  }
  public function adventureCentre(){
    $activity = Activity::where('id', 5)->first();
    $activity->normal = $activity->getFirstMediaUrl('activities', 'normal');
    $activity->normalwebp = $activity->getFirstMediaUrl('activities', 'normal-webp');
    $activity->double = $activity->getFirstMediaUrl('activities', 'double');
    $activity->doublewebp = $activity->getFirstMediaUrl('activities', 'double-webp');
    $activity->featured = $activity->getFirstMediaUrl('activities', 'featured');
    $activity->featuredwebp = $activity->getFirstMediaUrl('activities', 'featured-webp');
    $activity->mimetype = $activity->getFirstMedia('activities')->mime_type;
    
    foreach($activity->activityslides as $as){
        if(!$as->video){
            $as->double = $as->getFirstMediaUrl('activityslides', 'double');
            $as->doublewebp = $as->getFirstMediaUrl('activityslides', 'double-webp');
            $as->normal = $as->getFirstMediaUrl('activityslides', 'normal');
            $as->normalwebp = $as->getFirstMediaUrl('activityslides', 'normal-webp');
            $as->featured = $as->getFirstMediaUrl('activityslides', 'featured');
            $as->featuredwebp = $as->getFirstMediaUrl('activityslides', 'featured-webp');
            $as->mimetype = $as->getFirstMedia('activityslides')->mime_type;
        }
    }
    return view('activities.adventureCentre')->with(['activity' => $activity]);
  }

  public function toddlerBounce(){
    return view('activities.toddlerBounce');
  }

  public function chooseYourActivities(){
    return view('chooseYourActivities');
  }
  public function memberships(){
    return view('memberships');
  }
  public function giftVouchers(){
    return view('vouchers-2023-org');
  }
  public function foodDrink(){
    return view('foodDrink');
  }
  public function charityDonations(){
    return view('charityDonations');
  }
  public function tandcs(){
    return view('tandcs');
  }
  public function privacyPolicy(){
    return view('privacyPolicy');
  }
  public function accommodation(){
    return view('accommodation');
  }
  public function partiesGroups(){
    return view('parties.index');
  }
  public function birthdays(){
    return view('parties.birthdays');
  }
  public function corporate(){
    return view('parties.corporate');
  }
  public function schools(){
    return view('parties.schools');
  }
  public function stagHen(){
    return view('parties.stagHen');
  }
  public function partiesChristmas(){
    return view('parties.christmas');
  }
  public function partiesAQE(){
    return view('parties.aqe');
  }
  public function parties(){
    return view('parties.parties');
  }
  public function workForUs(){
    return view('workForUs');
  }
}
