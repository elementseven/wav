<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Carbon\Carbon;
use Newsletter;
use Mail;
use Auth;

class SendMail extends Controller
{

	public function superchargeRequest(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|digits_between:10,12',
			'message' => 'required|string',
		]); 

		$subject = "Highstreet Voucher";
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		Mail::send('emails.highstreetVoucher',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'phone' => $phone,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@wearevertigo.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}

	public function monitoringForm(Request $request){
		// Validate the form data
		$this->validate($request,[
			'nin' => 'required|string|min:6|max:20',
			'gender' => 'required',
			'religion' => 'required'
		]); 

		$nin = $request->input('nin');
		$gender = $request->input('gender');
		$religion = $request->input('religion');

		Mail::send('emails.monitoring',[
			'nin' => $nin,
			'gender' => $gender,
			'religion' => $religion
		], function ($message) use ($nin, $gender, $religion){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject('Monitoring Form - We Are Vertigo');
			$message->to('monitoring@wearevertigo.com');
			//$message->to('luke@elementseven.co');
		});
		return 'success';
	}

  public function enquiry(Request $request){

    // Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'message' => 'required|string',
			'date' => 'required|string'
		]); 

		$subject = "General Enquiry";
		$name = $request->input('name');
		$email = $request->input('email');
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$content = $request->input('message');
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}
		Mail::send('emails.enquiry',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'date' => $date,
			'phone' => $phone,
			'company' => $company,
			'content' => $content
		], function ($message) use ($subject, $email, $date, $name, $content, $phone, $company){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@wearevertigo.com');
		});
		return 'success';
	}

	public function party(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'phone' => 'required|string|min:11',
			'message' => 'required|string',
			'date' => 'required|string'
		]); 

		$subject = "Booking Request - " . $request->input('subject');
		$name = $request->input('name');
		$date = Carbon::parse($request->input('date'))->format('d/m/Y');
		$email = $request->input('email');
		$content = $request->input('message');
		$heard = null;
		if($request->input('heard') != ""){
			$heard = $request->input('heard');
		}
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		Mail::send('emails.party',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'date' => $date,
			'company' => $company,
			'phone' => $phone,
			'heard' => $heard,
			'content' => $content
		], function ($message) use ($subject, $email, $date, $name, $content, $phone, $company, $heard){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@wearevertigo.com');
		});
		return 'success';
	}

	public function christmasEnquiry(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'email' => 'required|email',
			'phone' => 'required|string|min:11',
			'message' => 'required|string',
		]); 

		$subject = "Booking Request - " . $request->input('subject');
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');
		$heard = null;
		if($request->input('heard') != ""){
			$heard = $request->input('heard');
		}
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		$company = null;
		if($request->input('company') != ""){
			$company = $request->input('company');
		}
		Mail::send('emails.party',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'phone' => $phone,
			'company' => $company,
			'heard' => $heard,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone, $company, $heard){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@wearevertigo.com');
		});
		return 'success';
	}

	public function charity(Request $request){

    	// Validate the form data
		$this->validate($request,[
			'name' => 'required|string|max:255',
			'charity_number' => 'required|string|max:255',
			'charity_name' => 'required|string|max:255',
			'email' => 'required|email',
			'phone' => 'required|string|min:11',
			'message' => 'required|string',
		]); 

		$subject = "Charity Donaition Request";
		$name = $request->input('name');
		$email = $request->input('email');
		$content = $request->input('message');
		$charity_name = $request->input('charity_name');
		$charity_number = $request->input('charity_number');
		$phone = null;
		if($request->input('phone') != ""){
			$phone = $request->input('phone');
		}
		Mail::send('emails.charity',[
			'name' => $name,
			'subject' => $subject,
			'email' => $email,
			'phone' => $phone,
			'charity_name' => $charity_name,
			'charity_number' => $charity_number,
			'content' => $content
		], function ($message) use ($subject, $email, $name, $content, $phone, $charity_name, $charity_number){
			$message->from('donotreply@wearevertigo.com', 'We Are Vertigo');
			$message->subject($subject);
			$message->replyTo($email);
			$message->to('info@wearevertigo.com');
		});
		return 'success';
	}

	public function mailingListSignup(Request $request){
	  	$this->validate($request,[
	      'email' => 'required|string|email|max:255'
	    ]);
	    Newsletter::subscribe($request->input('email'));
	    return "success";
	}
}