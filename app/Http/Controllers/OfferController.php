<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('offers.index');
    }

    /**
     * Display the christmas offers page.
     *
     * @return \Illuminate\Http\Response
     */
    public function christmas(){
        return view('offers.christmas');
    }

    /**
     * Return Json of offers and discounts.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request){

        $posts = Offer::where('status','!=','draft')
        ->orderBy('display_order','asc')
        ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','expires','created_at']);

        foreach($posts as $p){
          $p->featured = $p->getFirstMediaUrl('offers', 'featured');
          $p->featuredwebp = $p->getFirstMediaUrl('offers', 'featured-webp');
          $p->mimetype = $p->getFirstMedia('offers')->mime_type;
        }
        return $posts;
    }

    public function supercharge(){
        return view('offers.superchargeHighstreetVoucher');
    }

    public function superchargeVoucher(){
        return view('offers.superchargeVoucher');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function show($date, $slug){
        $post = Offer::where([['slug',$slug],['status','!=','draft']])->whereDate('created_at', $date)->first();
        return view('offers.show')->with(['post' => $post]);
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function edit(Offer $offer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $offer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
